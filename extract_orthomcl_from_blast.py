import sys,os
from Bio.Blast import NCBIXML
import math

"""
used to extract the seqid and best hit to orthomcl group
"""

evalue_limit = 20

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print "python extract_orthomcl_from_blast.py infile.xml outfile"
        sys.exit(0)
    
    infile = open(sys.argv[1],"r")
    outfile = open(sys.argv[2],"w")
    for i in NCBIXML.parse(infile):
        qu_full = str(i.query)
        finished = False
        for m in i.alignments:
            if finished == True:
                break
            evalue = float(m.hsps[0].expect)
            hit_def = str(str(m.hit_def).split("|")[2].strip())
            if hit_def == "no_group":
                continue
            print qu_full,hit_def
            try:
                if -math.log10(evalue) > evalue_limit:
                    outfile.write(qu_full+"\t"+hit_def+"\n")
            except:
                outfile.write(qu_full+"\t"+hit_def+"\n")
            finished = True
    outfile.close()
    infile.close()
