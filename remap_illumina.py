#!/usr/bin/env python	
import sys
import re
from collections import defaultdict
from Bio import SeqIO

Usage = """
Usage:

remap_illumina.py reference.fasta s_N_sorted.txt

Derives isogroup counts, normalized by length, from illumina mapping.

reference.fasta is the same reference that was squashed and mapped to. It is used to map the 
contigs to isogroups and get the sequence lengths.

The s_N_sorted.txt file from eland_extended gerald analyses in casava. From the casava 
manual:

s_N_sorted.txt - This output file is similar to s_N_export.txt, except it contains only entries for 
reads which pass purity filtering and have a unique alignment in the reference. These are sorted by 
order of their alignment position, which is meant to facilitate the extraction of ranges of reads 
for purposes of visualization or SNP calling.

The length is calculated as the sum of the lengths of all contigs mapped to an isogroup, there is 
no weighting of any sort.

Uniqueness of mapping is not evaluated in any way, it is assumed to have been evaluated already in 
the s_N_sorted.txt file. ie, reads with non-unique mapping are entirely exluded.

The output columns are:

g - the gene (isogroup, or contig if cap3)
tl - total length of the gene, summed across component contigs
tc - total count of (unique) reads mapped to the gene across component contigs
rpk - 1000.0 * tc / tl, the count normalized to kb.


"""

if len(sys.argv) < 3:
	print Usage
else:
	fasta_name = sys.argv[1]
	ali_name = sys.argv[2]

	lengths = {} #Dictionary with key contig and value contig length
	map = {} #Dictionary with key contig and value gene
	fasta_handle = open(fasta_name, "rU")
	totallength = 0
	for record in SeqIO.parse(fasta_handle, "fasta"):
		name = record.id
		gene = name
		#>contig00057_length_766_numreads_24_gene_isogroup00001_status_it_thresh
		#>cap3_Contig968
		m = re.match(r'(contig\d+).+?(isogroup\d+)', name)
		if m:
			name = m.group(1)
			gene = m.group(2)
		length = len(record.seq)
		#print length, name, gene
		lengths[name]=length
		totallength += 1
		map[name]=gene
	fasta_handle.close()
	
	gmap = defaultdict(set) # Dictionary with key gene and value set, where the members of the set are the contigs in that gene
	for c in map.keys():
		gmap[map[c]].add(c)
	
	counts = {} #Dictionary with key contig and value count of the number of times tags mapped to it
	ali_handle = open(ali_name, "rU")
	p = re.compile(r'(contig\d+)')
	totalcount = 0
	for line in ali_handle:
		line = line.strip()
		fields = line.split('\t')
		contig = fields[11]
		m = p.match(contig)
		if m:
			contig = m.group(1)
		#EAS1745 0004    1       30      15025   14938   0       1       TTCTGTCTTCTCGTTCGATACAAATGGTTTCGCCA     bbbTcccccffffffffffffffffffffffffff     Nanomia_ncontig_cap3_24Mar10.fna        cap3_Contig10000        -2      R       32NNN   135
		counts[contig] = counts.get(contig, 0) + 1
		totalcount += 1
	ali_handle.close()
	
	
	
	print "gene length count rpk"
	genes = gmap.keys()
	genes.sort()
	for g in genes:
		tc = 0
		tl = 0
		for c in gmap[g]:
			tl = tl + lengths[c]
			tc = tc + counts.get(c, 0)
		rpk = 1000.0 * tc / tl
		print g, tl, tc, rpk
	
"""
	
	map = {} #Dictionary with key contig and value gene
	counts = {} #The total counts for each gene in map_name, including 0 for genes with no count
	map_handle = open(map_name, "rU")
	for line in map_handle:
		line = line.strip()
		contig, gene = line.split()
		#print contig, gene
		map[contig] = gene
		counts[gene] = 0
	
	counts_handle = open(counts_name, "rU")
	n = 0
	
	# Load the data into the maps
	ttoc = defaultdict(dict) # Dictionary with key tag and value dict. The nested dict has key contig and value count for contig for tag
	ttog = defaultdict(set) # Dictionary with key tag and value set of genes, used to identify unique tags that hit no more than one gene
	for line in counts_handle:
		n = n + 1
		if n < 2:
			continue
		line = line.strip()
		fields = line.split()
		#Tag_Seq GI_num  GI_Pos  Read_ID Mismatch
		mis = int(fields[4])
		if mis > mismatch and mismatch > -1:
			continue
		tag = fields[0]
		contig = fields[1]
		
		ttoc[tag][contig] = ttoc[tag].get(contig, 0) + 1
		
		gene = map.get(contig, '')
		if len(gene) < 1:
			sys.stderr.write("Warning: Contig %s found in %s but not %s\n" % (c, counts_name, map_name))
		
		ttog[tag].add(gene)
		
	
	all = {}
	uniq = {}
	
	for t in ttoc.keys():
		genes = set([])
		for contig in ttoc[t].keys():
			gene = map.get(contig, '')
			if len(gene) < 1:
				sys.stderr.write("Warning: Contig %s found in %s but not %s\n" % (c, counts_name, map_name))
			if not gene in genes:
				all[gene] = all.get(gene,0) + ttoc[t][contig]
				if len(ttog[t]) < 2:
					uniq[gene] = uniq.get(gene,0) + ttoc[t][contig]
			genes.add(gene)

	# Add up the counts from the maps
	print 'gene\tall\tuniq'
	for g in all.keys():
		print g +'\t'+ str(all[g]) +'\t'+ str(uniq.get(g,0))
		
		
"""
