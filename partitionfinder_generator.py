#!/usr/bin/env python

import os
import sys

Usage = """
Generates PartitionFinder configuration file using genes as partitions and
all protein models available in RAxML (44 in total).  Model selection uses
BIC.  For other parameters, please refer to PartitonFinder Manual.

Usage:
  partitionfinder_generator.py superematrix.phy supermatrix.partition.txt

"""

if len(sys.argv) < 2:
        print Usage
else:
        align_file = sys.argv[1]
        partition_file = sys.argv[2]

        partitions = open(partition_file, 'r')

        data = []
        for line in partitions:
                line = line.strip('\n')
                elements = line.split(' ')
                out = elements[1:4]
                out = ' '.join(out)
                data.append(out)

        outfile = open("partition_finder.cfg", 'w')

        align = "alignment = %s;" %align_file
        default_param1 = "\nbranchlengths = linked;\nmodels = all_protein;\nmodel_selection = BIC;\n[data_blocks]\n"
        default_param2 = "\n[schemes]\nsearch = rcluster;\n"

        outfile.write(align)
        outfile.write(default_param1)
        for i in data:
                outfile.write(i + ";\n")
        outfile.write(default_param2)
        outfile.close()

