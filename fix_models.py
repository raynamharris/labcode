import sys,os

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "usage: python fix_models.py RAXMLMODEL"
		sys.exit(0)
	infile = open(sys.argv[1],"rU")
	allnums = {}
	lines = {}
	count = 0
	for i in infile:
		spls = i.strip().split("=")
		nums = spls[1].split("-")
		num1 = int(nums[0])
		num2 = int(nums[1])
		allnums[count] = [num1,num2]
		lines[count] = spls[0]
		count += 1
	infile.close()
	for i in allnums:
		if allnums[i][0] > allnums[i][1]:
			allnums[i][0] = allnums[i][0]-2
			allnums[i-1][1] = allnums[i-1][1]-2
			print lines[i]
#	for i in allnums:
#		print lines[i]+"="+str(allnums[i][0])+"-"+str(allnums[i][1])
