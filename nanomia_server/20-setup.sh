SSHDCONF=/etc/ssh/sshd_config
sed -e '/Port / c Port 1724' -e '/^PasswordAuthentication/ c PasswordAuthentication no' -e '/^X11Forwarding/ c X11Forwarding no' $SSHDCONF >$SSHDCONF.bak
diff $SSHDCONF $SSHDCONF.bak
mv $SSHDCONF.bak $SSHDCONF
service sshd restart
