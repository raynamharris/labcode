sed -e '/BOOTPROTO/d' -e '/IPADDR/d' -e '/NETMASK/d' -e '/BROADCAST/d' -e '/NETWORK/d' -e '/GATEWAY/d' /etc/sysconfig/network-scripts/ifcfg-em1 >ifcfg-em1-tmp
echo "BOOTPROTO=static" >>ifcfg-em1-tmp
echo "IPADDR=128.148.30.58" >>ifcfg-em1-tmp
echo "NETMASK=255.255.255.0" >>ifcfg-em1-tmp
echo "BROADCAST=128.148.30.255" >>ifcfg-em1-tmp
echo "NETWORK=128.148.30.0" >>ifcfg-em1-tmp
echo "GATEWAY=128.148.30.1" >>ifcfg-em1-tmp
mv /etc/sysconfig/network-scripts/ifcfg-em1 /etc/sysconfig/network-scripts/ifcfg-em1.bak
mv ifcfg-em1-tmp /etc/sysconfig/network-scripts/ifcfg-em1
echo "search brown.edu
nameserver 10.1.1.10" >/etc/resolv.conf
/etc/init.d/network restart
