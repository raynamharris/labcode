useradd -G wheel -p `mkpasswd -l 32` mhowison
SUDOERS=/etc/sudoers
sed -e '/^%wheel/ c %wheel	ALL=(ALL)	NOPASSWD: ALL' $SUDOERS >$SUDOERS.bak
diff $SUDOERS $SUDOERS.bak
mv $SUDOERS.bak $SUDOERS
chmod 440 $SUDOERS
mkdir -p ~mhowison/.ssh
cp id_dsa.pub ~mhowison/.ssh/authorized_keys
chmod 700 ~mhowison/.ssh
chmod 600 ~mhowison/.ssh/*
chown -R mhowison:mhowison ~mhowison/.ssh
