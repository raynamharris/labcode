import os,sys
import newick3,phylo3
import re

basename = "RAxML_bestTree."

taxa_base = "Scaphopods"

"""

"""

if __name__ == '__main__':
	if len(sys.argv) != 3:
		print "python prune_gene_tree_by_taxa.py DIR outdir"
		sys.exit(0)
	
	dir = sys.argv[1]
	
	outdir = sys.argv[2]
	
	count = 0
	for j in os.listdir(dir):
		if basename in j:
			count += 1
			filen = dir +"/" + j
			file = open(filen,"r")
			line = file.readline().strip()
			#find the name
			result = re.findall(r"(Scaphopods@\d+)",line)
			names = []
			nmsstr = ""
			for i in result:
				nmsstr += i +" "
			nmsstr = nmsstr[:-1]
			if len(result) > 0:
				cmd = "phyutility -pr -names "+nmsstr+ " -in "+filen+" -out "+outdir+"/"+j
#				print cmd
				os.system(cmd)
			else:
				os.system("cp "+filen+" "+outdir+"/"+j)
			file.close()
			if count % 100 == 0:
				print count