import sys

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "python get_degrees.py graphfile"
		sys.exit(0)
	infile = open(sys.argv[1],"r")
	graph_map = {}
	for i in infile:
		spls = i.strip().split("\t")
		if spls[0] not in graph_map:
			graph_map[spls[0]] = 0
		if spls[1] not in graph_map:
			graph_map[spls[1]] = 0
		graph_map[spls[0]] += 1
		graph_map[spls[1]] += 1
	for i in graph_map:
		print graph_map[i]
	infile.close()
