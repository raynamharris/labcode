import sys,os

if __name__ == "__main__":
	if len(sys.argv) != 5:
		print "python get_single_graph_connections.py graph_number graph_file mcl_input outfile"
		sys.exit(0)
	ingraph = int(sys.argv[1])
	infile = open(sys.argv[2],"r")
	inmcl = open(sys.argv[3],"r")
	outfile = open(sys.argv[4],"w")
	count = 0
	cutoff = 60
	for i in infile:
		if count == ingraph:
			graph = i.split("\t")
		count += 1
	for i in inmcl:
		spls = i.split("\t")
		if spls[0] in graph and spls[1] in graph and float(spls[2]) > cutoff:
			outfile.write(i)
	infile.close()
	inmcl.close()
	outfile.close()
