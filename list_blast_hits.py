import sys
from Bio.Blast import NCBIXML
from Bio import SeqIO

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "usage: python list_blast_hists.py infile.xml"
		sys.exit(0)

	inxml = open(sys.argv[1],"r")
	for k in NCBIXML.parse(inxml):
		qutitle = str(k.query)
		for m in k.alignments:
			print qutitle
			break
	inxml.close()
