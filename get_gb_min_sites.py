from Bio import SeqIO
import sys,os

"""
this is to get the list to concatenate based on a minimum number of sites

it can easily be used for the other files just by changing the file ending
"""

file_ending = ".aln-gb"

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "usage: python get_gb_min_stats.py DIR numofsites outfile"
		sys.exit(0)
	path = sys.argv[1]
	numofsites = int(sys.argv[2])
	outfile = sys.argv[3]
	out = open(outfile,"w")
	for i in os.listdir(path):
		print i
		if i[-len(file_ending):] == file_ending:
			handle = open(path+"/"+i,"rU")
			number = len(list(SeqIO.parse(handle,"fasta"))[0].seq)
			handle.close()
			if number >= numofsites:
				out.write(str(i)+" ")
	out.write("\n")
	out.close()
