import sys,os,sqlite3
from numpy import mean
from numpy import median

species_of_interest = ["Bargmannia","Frillagalma","Agalma_elegans","Nanomia","Hydra_magnipapillata","Nematostella_vectensis","Clytia_hemisphaerica","Hydractinia_echinata","Podocoryna_carnea"]


if __name__ == "__main__":
	if len(sys.argv) !=  3:
		print "python summarize_cluster_stats.py database mcl_in"
		sys.exit(0)
	
	database = sys.argv[1]
	
	if os.path.exists(database) == False:
		print "the database has not been created or you are not in the right place"
	con = sqlite3.connect(database)
	cur = con.cursor()
	
	mcl_in = open(sys.argv[2],"r")		
	clusters = {}
	count = 0
	for i in mcl_in:
		spls = i.strip().split("\t")
		clusters[count]=spls
		count += 1
	
	species_names = []
	species_names_id = {} #key id value species
	cur.execute("SELECT id,name FROM species_names;")
	for i in cur:
		if str(i[1]) in species_of_interest:
			species_names.append(str(i[1]))
			species_names_id[str(i[0])] = str(i[1])

	count = 0
	print "cluster_id\tnum_seqs\tnum_singlets\tnum_tax\tmean_seq_per_taxa\tmed_seq_per_taxa"
	for i in clusters:
		#print i,len(clusters[i])
		sp_names_list = []
		num_singlets = 0
		for j in clusters[i]:
			sp_name = None
			cur.execute("SELECT species_names_id,seq_id,file FROM translated_seqs WHERE id = ?",(j,))
			a = cur.fetchall()
			if len(a) > 0:
				sp_name = species_names_id[str(a[0][0])]
				sp_names_list.append(sp_name)
				file = str(a[0][2])
				if "singlets.fsa" in file:
					num_singlets += 1
		cnts = []
		for j in species_names:
			tcnt = sp_names_list.count(j)
			cnts.append(tcnt)
		print str(i)+"\t"+str(len(clusters[i]))+"\t"+str(num_singlets)+"\t"+str(len(set(sp_names_list)))+"\t"+str(mean(cnts))+"\t"+str(median(cnts))
		count += 1
	