from Bio import SeqIO
from numpy import mean
import sys

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "usage: python trim_illumina_fastq_based_on_quality_se.py se_in"
		sys.exit(0)
	count = 0
	outfile = open(sys.argv[1]+".thinned","a")
	infile = open(sys.argv[1],"r")
	for first in SeqIO.parse(infile,"fastq-illumina"):
		seq_quals1 = first.letter_annotations['phred_quality']
		if mean(seq_quals1) > 35:
			seqs = [first]
			SeqIO.write(seqs,outfile,"fastq-illumina")
		count += 1
		if count % 10000 == 0:
			print count	
	infile.close()
