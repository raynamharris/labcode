import sys

from Bio import AlignIO
from Bio.SeqUtils.ProtParam import ProteinAnalysis
from collections import defaultdict


def sp_aa_comp(align):
	'''
	Calculates fraction of AA per species
	'''
	d =  defaultdict(lambda: defaultdict(int))
	
	for rec in align:
		rec.seq = rec.seq.ungap("X")
		rec.seq = rec.seq.ungap("-")
		x = ProteinAnalysis(str(rec.seq))
		for k, v in x.count_amino_acids().iteritems():
			x.count_amino_acids()[k] = v / (len(rec.seq)*1.0)
		d[rec.id] = x.count_amino_acids()
	
	return d

if __name__=="__main__":
	if len(sys.argv) < 4:
		print "Usage: python compositional_heterogeneity.py alignment format outfile"
		sys.exit(0)

	msa = AlignIO.read(open(sys.argv[1]), sys.argv[2])
	
	result = sp_aa_comp(msa)
	with open(sys.argv[3], "w") as f:
		for v in result.itervalues():
			print >>f, "species", ",", ",".join([str(x) for x in v.keys()] )
			break
		for k, v in result.iteritems():
			print >>f, k, ",", ",".join([str(x) for x in v.values()] )

