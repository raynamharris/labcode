import sys,os,sqlite3

if __name__== "__main__":
    if len(sys.argv) != 4: 
        print "python extract_orthomcl_files_from_blast_out.py infile db outdir"
        sys.exit(0)
    seq_dict = {}
    groups = []
    infile = open(sys.argv[1],"r")
    count = 0
    for i in infile:
        spls = i.strip().split("\t")
        if spls[1] not in seq_dict:
            seq_dict[spls[1]] = []
        seq_dict[spls[1]].append(spls[0])
        if spls[1] not in groups:
            groups.append(spls[1])
        count += 1
    infile.close()
    database = sys.argv[2]
    if os.path.exists(database) == False:
        print "the database has not been created or you are not in the right place"
    con = sqlite3.connect(database)
    cur = con.cursor()
    species_names = []
    species_names_id = {} #key id value species
    cur.execute("SELECT id,name FROM species_names;")
    for i in cur:
        species_names.append(str(i[1]))
        species_names_id[str(i[0])] = str(i[1])

    outdir = sys.argv[3]
    for i in groups:
        if len(seq_dict[i]) < 3:
            continue
        outfile = open(outdir+"/"+i+".fasta","w")
        for j in seq_dict[i]:
            cur.execute("SELECT species_names_id,seq FROM translated_seqs WHERE id = ?",(j,))
            a = cur.fetchall()
            if len(a) > 0:
                outfile.write(">"+species_names_id[str(a[0][0])]+"@"+j+"\n"+str(a[0][1])+"\n")
        outfile.close()
