import sys,os,sqlite3
import os.path
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from colors import *

"""
This assumes the output from clusterx obtained from http://paccanarolab.org/software/scps/

this will take the output from the mcl analysis and write out the fasta
files for each cluster , this assumes that all labels are the sequence id labels

this will use the main mollusk.db for translations

USE THIS ONE!

this also requires identifying the outgroup taxa as only clusters with
at least 2 ingroup taxa will be included and at least 4 total taxa. 
"""

#database = "mollusk.db"

outgroups = ["Lingula_anatina","Schmidtea_mediterranea", "Chaetopterus_variopedatus","Terebratalia_transversa","Cerebratulus_lacteus", "Carinoma_mutabilis","Capitella","Helobdella_robusta","Macrostomum lignano","Paraplanocera oligoglena", "Drosophila melanogaster"]
#outgroups = ["Hydra_magnipapillata","Nematostella_vectensis","Clytia_hemisphaerica","Podocoryna_carnea","Hydractinia_echinata"]
#outgroups = ["Capitella","Gallus","Drosophila_melanogaster","Amphimedon_queenslandica","Trichoplax","Strongylocentrotus_purpuratus"]

SMALLESTCLUSTER = 4
SMALLESTINGROUP = 1

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "usage: python extract_clusters_from_clusterx_out.py database clusterx_out OUTDIR"
		sys.exit(0)
	
	database = sys.argv[1]
	clusterx_file = open(sys.argv[2],"rU")
	clusters = {}
	count = 0
	for i in clusterx_file:
		if "%" in i or len(i) < 2:
			continue
		spls = i.strip().split("\t")
		id = spls[0]
		cl = spls[1]
		if cl not in clusters:
			clusters[cl] = []
		clusters[cl].append(id)

	if os.path.exists(database) == False:
		print "the database has not been created or you are not in the right place"
	con = sqlite3.connect(database)
	cur = con.cursor()

	species_names = []
	species_names_id = {} #key id value species
	cur.execute("SELECT id,name FROM species_names;")
	for i in cur:
		species_names.append(str(i[1]))
		species_names_id[str(i[0])] = str(i[1])

	OUTDIR = sys.argv[3]
	count = 0
	error_file = open("cluster_errors","w")
	for i in clusters:
		if len(clusters[i]) >= SMALLESTCLUSTER:
			print i,len(clusters[i])
			ingroup_samp = []
			cluster_out_seqs = []
			for j in clusters[i]:
				seq = None
				cur.execute("SELECT species_names_id,seq FROM translated_seqs WHERE id = ?",(j,))
				a = cur.fetchall()
				if len(a) > 0:
					seq = SeqRecord(Seq(str(a[0][1])))
					sp_name = species_names_id[str(a[0][0])]
					seq.id = sp_name+"@"+str(j)
					seq.description = ""
					seq.name = ""
					cluster_out_seqs.append(seq)
					if sp_name not in outgroups and sp_name not in ingroup_samp:
						ingroup_samp.append(sp_name)
				else:
					seq = None
				if seq == None:
					print "ERROR:no break"
					error_file.write(j+"\n")
					print j
					#sys.exit(0)

			if len(ingroup_samp) >= SMALLESTINGROUP:
				cluster_out_seqfile = open(OUTDIR+"/"+str(count)+".fasta","w")
				SeqIO.write(cluster_out_seqs,cluster_out_seqfile,"fasta")
				cluster_out_seqfile.close()
			count += 1
	error_file.close()
	clusterx_file.close()
		
