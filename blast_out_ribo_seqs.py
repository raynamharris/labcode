import os,sys
from Bio import SeqIO
from Bio.Blast import NCBIXML

"""
this takes a seed ribo file and will remove the sequences that blast to the file

make sure that the line around 27 or so that defines the hitid and the line around
39 or so that gets the sequence ids are defined correct. these change depending on the
technology
"""

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python blast_out_ribo_seqs.py riboseqs.fa infile.fa outfile.fa"
		sys.exit(0)
	riboseqs = sys.argv[1]
	infilen = sys.argv[2]
	#run the blast real quick
	cmd = "makeblastdb -in "+infilen+" -dbtype nucl -out "+infilen+".db"
	os.system(cmd)
	cmd = "blastn -db "+infilen+".db -outfmt 5 -num_threads 8 -out "+infilen+".temp.blast.xml -query "+riboseqs
	print "running blast"
	os.system(cmd)
	takeout = []
	blin = open(infilen+".temp.blast.xml","r")
	for i in NCBIXML.parse(blin):
		for j in i.alignments:
			hitid = str(j.hit_def).split()[0]#this is for 454
			print hitid
			takeout.append(hitid)
	blin.close()
	print "removing temporary files"
	os.system("rm "+infilen+".db*")
	os.system("rm "+infilen+".temp.blast.xml")
	infile = open(infilen,"r")
	outseqs = []
	outribos = []
	for i in SeqIO.parse(infile,"fasta"):
		if i.id not in takeout:
			outseqs.append(i)
		else:
			outribos.append(i)
	infile.close()
	outfile = open(sys.argv[3],"w")
	SeqIO.write(outseqs,outfile,"fasta")
	outfile.close()
	outfile = open(sys.argv[2]+".RIBOSEQS","w")
	SeqIO.write(outribos,outfile,"fasta")
	outfile.close()
