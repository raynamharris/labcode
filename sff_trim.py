#! /usr/bin/env python
import sys
import re
from Bio import SeqIO

if __name__ == "__main__":
	if len(sys.argv)!=4:
		print "sff_trim.py in.sff out.sff SEQUENCE"
		sys.exit(0)
	
	in_name = sys.argv[1]
	out_name = sys.argv[2]
	adap = sys.argv[3]
	
	# The following is based on example code from http://www.biopython.org/DIST/docs/api/Bio.SeqIO.SffIO-module.html
	
	records = (record for record in 
		SeqIO.parse(in_name,"sff") 
		if record.seq[record.annotations["clip_qual_left"]:].startswith("AAAGA"))