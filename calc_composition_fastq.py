from Bio import SeqIO
import sys

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "python calc_gc.py inseq"
		sys.exit(0)
	infile = open(sys.argv[1],"r")
	content = []
	seqlen = 0
	nseqs = 0
	limit = 1000000
	first = True
	count = 0
	for i in SeqIO.parse(infile,"fastq-illumina"):
		if first ==True:
			first = False
			seqlen = len(i.seq)
			for j in range(seqlen):
				content.append({'A':0,'C':0,'G':0,'T':0,'N':0,'.':0})
		for j in range(seqlen):
			content[j][i.seq.tostring()[j].upper()] += 1
		nseqs += 1
		if nseqs == limit:
			break
	for j in range(seqlen):
		string =""
		for k in content[j]:
			content[j][k] /= float(nseqs)
			string += k +":"+str(content[j][k])+"  "
		print string
		
