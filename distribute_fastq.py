import sys
import os

Usage = """
Usage:

python distribute_fastq in.fastq n

Splits up in.fastq into n files of equal size

"""

filetype = "fastq"

if __name__ == "__main__":

	# Parse arguments
	if len(sys.argv) != 3:
		print Usage
		sys.exit(0)
	print "Called with: " + sys.argv[0]
	in_name = sys.argv[1]
	num_files = int(sys.argv[2])
	

	# Parse file names
	(filedir, filename) = os.path.split(in_name)
	L = filename.split('.')
	extension = L[-1]
	name_root = '.'.join(L[:-1])
	print "output root name is " + name_root
	print "output extension is " + extension
	
	
	# Open output files
	out_files = []
	for i in range(num_files):
		outname = name_root + '.' + str(i) + '.' + extension
		outfile = open(outname,"w")
		out_files.append(outfile)
	
	
	# Loop over sequences in the input file, distribute them among the output files
	print "Parsing sequences from " + in_name
	infile = open(in_name,"r")
	n = 0 # Number of lines processed
	
	freq = 1000000 # printing freuqency
	
	s = ""
	for line in infile:
		n = n + 1
		s = s + line
		if ((n % 4) == 0):
			if s[0] != '@':
				sys.exit("ERROR: The first character of the following sequence entry (occuring at or prior to line " + str(n) + ") is not @, poorly formed sequence:\n" + s)
			out_files[(n/4) % num_files].write(s)
			s = ""
		if ((n/float(4)) % freq) == 0:
			print " " + str(n/4) + " sequences processed"
		
	infile.close()
	for outfile in out_files:
		outfile.close()
		

	