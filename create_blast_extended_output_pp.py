import sys,os
import subprocess
from Bio import SeqIO
from Bio.Blast.Applications import NcbiblastpCommandline
from Bio.Blast import NCBIXML
import math
import pp

DEBUG = False
num_jobs = 20

def blast2(seq1,dbf):
	in1 = open(seq1.id,"w")
	in1.write(">"+seq1.id+"\n"+seq1.seq.tostring().upper()+"\n")
	in1.close()
	cline = Bio.Blast.Applications.NcbiblastnCommandline(query=seq1.id,db=dbf, evalue=0.0001, outfmt=5,out=seq1.id+".xml")
	return_code = subprocess.call(str(cline), shell=(sys.platform!="win32"))

def calc_blast_thread(index,seqs,dbfile):
	outfile = open("pp."+str(index)+".temp","w")
	for i in seqs:
		match = False
		blast2(i,dbfile)
		result_handle = open(i.id+".xml","rU")
		try:
			for k in Bio.Blast.NCBIXML.parse(result_handle):
				qutitle = str(k.query)
				for m in k.alignments:
					evalue = 0
					try:
						evalue = -math.log10(m.hsps[0].expect)
					except:
						evalue = 180 #largest -log10(evalue) seen
					hitid = str(m.hit_def)
					if hitid == qutitle: #don't want hits to self
						continue
					outfile.write(qutitle+"\t"+hitid+"\t"+str(evalue)+"\t"+str(k.query_letters)+"\t"+str(m.length))
					for j in m.hsps:
						outfile.write("\t"+str(j.identities)+"\t"+str(j.query_start)+","+str(j.query_end)+","+str(j.sbjct_start)+","+str(j.sbjct_end))
					outfile.write("\n")

			result_handle.close()
			os.remove(i.id)#remove file
			os.remove(i.id+".xml")#remove file
		except:#fix weird nonunicode characters
			result_handle.close()
			result_handle = open(i.id+".xml","rU")
			result_handle2 = open(i.id+".2.xml","w")
			for k in result_handle:
				result_handle2.write(str(unicode(k,errors="replace").replace(u'\ufffd',"-")))
			result_handle.close()
			result_handle2.close()
			result_handle2 = open(i.id+".2.xml","rU")
			try:
				for k in Bio.Blast.NCBIXML.parse(result_handle2):
					qutitle = str(k.query)
					print qutitle
					for m in k.alignments:
						evalue = 0
						try:
							evalue = -math.log10(m.hsps[0].expect)
						except:
							evalue = 180 #largest -log10(evalue) seen
						hitid = str(m.hit_def)
						if hitid == qutitle: #don't want hits to self
							continue
						outfile.write(qutitle+"\t"+hitid+"\t"+str(evalue)+"\t"+str(k.query_letters)+"\t"+str(m.length))
						for j in m.hsps:
							outfile.write("\t"+str(j.identities)+"\t"+str(j.query_start)+","+str(j.query_end)+","+str(j.sbjct_start)+","+str(j.sbjct_end))
						outfile.write("\n")
			except:#usually this only happens if there is an empty file
				error_file = open("blast_errors","a")
				error_file.write(str(i.id)+"\n")
				error_file.close()
			result_handle2.close()
			os.remove(i.id)#remove file
			os.remove(i.id+".xml")#remove file
			os.remove(i.id+".2.xml")#remove file
	outfile.close()
	print "done",index
	return "pp."+str(index)+".temp"



if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python create_blast_output_pp.py infile outfile"
		sys.exit(0)
	#create a blastDB for the infile
	print "making db"
	cmd = "makeblastdb -in "+sys.argv[1]+" -dbtype nucl -out "+ sys.argv[1]+".db"
	os.system(cmd)
	print "finished making db"
	#open infile
	infile = open(sys.argv[1],"rU")
	allseqs = []
	for i in SeqIO.parse(infile,"fasta"):
		allseqs.append(i)
	allseqs.reverse()
	ppservers = ()
	job_server = pp.Server(num_jobs,ppservers=ppservers)
	print "Starting pp with ",job_server.get_ncpus(),"workers"
	jobs = []
	start = 0
	for index in range(num_jobs):
		print "starting worker "+str(index)
		if index == (num_jobs-1):
			jobs.append(job_server.submit(calc_blast_thread,(index,allseqs[start:len(allseqs)+1],sys.argv[1]+".db"),(blast2,),("math","Bio.SeqIO","Bio.Blast.NCBIXML","Bio.Blast.Applications","subprocess")))
			print start,len(allseqs)+1
		else:
			jobs.append(job_server.submit(calc_blast_thread,(index,allseqs[start:start+(len(allseqs)/int(num_jobs))],sys.argv[1]+".db"),(blast2,),("math","Bio.SeqIO","Bio.Blast.NCBIXML","Bio.Blast.Applications","subprocess")))
			print start,start+(len(allseqs)/int(num_jobs))
			start += (len(allseqs)/int(num_jobs))
	outfile = open(sys.argv[2],"w")
	outfile.write("query\thitid\tevalue\tquerylen\talignmentlen\tidentities\tquerystart,queryend,sbjctstart,sbjctend\n")
	for job in jobs:
		namef = job()
		print "combining "+str(namef)
		toufile = open(namef,"r")
		for i in toufile:
			outfile.write(i)
		toufile.close()
		os.system("rm "+str(namef))
	outfile.close()
