import sys,os,sqlite3
from Bio.Blast import NCBIXML
from Bio import SeqIO
from colors import *
import math


"""
this should look at the structure within the analyses folder and examine
the BLAST.db files in the root of the species directories 

then it will open that file and take the species name for each blast result plus 
the query_edited_id as the guid for our sequences. then it will take the blast 
accession as the guid for the ref seq.

the mcl will do the clustering on the refs (nr database hits) and our
seqs that hit on those.

this will only cluster things that have at least one hit with BLAST 
(blastx)
"""

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python create_mcl_input_from_sqlite.py INDIR outfile"
		sys.exit(0)

	INDIR = sys.argv[1]
	out = open(sys.argv[2],"w")
	for i in os.listdir(INDIR):
		#work with BLAST
		species_name = i
		print "working with BLAST from",BOLD,species_name,RESET
		BLASTDB = INDIR+"/"+i+"/BLAST.db"
		conn = sqlite3.connect(BLASTDB)
		c = conn.cursor()
		c.execute("select query_edited_id,hit_accession,evalue from blast_results;")
		qeid = None
		ha = None
		evalue = None
		for n in c:
			qeid = str(n[0])
			ha = str(n[1])
			evalue = float(n[2])
			try:
				evalue = -math.log10(evalue) #get the first hit
			except:
				evalue = 180 #largest -log10(evalue) seen
			out.write(ha+"\t"+species_name+"_"+qeid+"\t"+str(evalue)+"\n")
		c.close()
	out.close()
		
