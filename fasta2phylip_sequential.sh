#!/bin/bash

# Bash script to change alignment from FASTA format to PHYLIP
# sequential format.  Taxon names can be of any length.
#
# USAGE: fasta2phylip_sequential.sh  alignment.fa
#

BASENAME=${1%.*}
NUMSEQS=$(grep '>' -c $1)
NUMBASES=$(grep -m 1 -v '>' $1 | expr `wc -m` - 1)
echo $NUMSEQS $NUMBASES | awk '{ print "\t"$1"\t"$2 }' > $BASENAME.phy
cat $1 | sed 's/>//g' |  xargs -n 2 | awk '{ print $1" "$2" " $3 }' >> $BASENAME.phy

