import sys,os,newick3,phylo3

def mean(li):
	lenli = float(len(li))
	retmn = 0
	for i in li:
		retmn += i/lenli
	return retmn

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python get_branch_length_distributions.py mean/all infile.tre"
		sys.exit(0)
	if sys.argv[1] not in ["all","mean"]:
		print "second arg must be all or mean"
		sys.exit(0)
	infile = open(sys.argv[2],"r")
	tree = newick3.parse(infile.readline())
	infile.close()
	bl = []
	for i in tree.iternodes():
		bl.append(i.length)
		if sys.argv[1] == "all":
			print i.length
	if sys.argv[1] == "mean":
		print "mean: " + str(mean(bl))
