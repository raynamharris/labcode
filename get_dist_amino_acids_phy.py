import sys,os

"""
for a phylip formatted file, this will get the distribution of amino acids 

"""
aa = ['A','R','N','D','C','E','Q','G','H','I','L','K','M','F','P','S','T','W','Y','V']

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python get_dist_amino_acids_phy.py infile.phy outfile"
		sys.exit(0)
	infile = open(sys.argv[1],"r")
	outfile = open(sys.argv[2],"w")
	first = True
	seqs_dict = {}
	for i in infile:
		if first == True or len(i) < 2:
			first = False
			continue
		spls = i.split("\t")
		seqs_dict[spls[0]] = {}
		for j in aa:
			seqs_dict[spls[0]][j] = 0
		for j in spls[1]:
			if j.upper() in aa:
				seqs_dict[spls[0]][j.upper()] += 1
		outfile.write(spls[0])
		for j in aa:
			outfile.write("\t"+str(seqs_dict[spls[0]][j]))
		outfile.write("\n")
	infile.close()
	outfile.close()
	
