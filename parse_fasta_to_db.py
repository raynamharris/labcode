#! /usr/bin/env python


import re
import MySQLdb
import sys
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord


# Set the input file name
InFileName = sys.argv[1]

global_gene = ""

if (len(sys.argv) > 2):
	global_gene = sys.argv[2]
	
ncbi = False
#ncbi = True

# Open the input file
InFile = open(InFileName, 'r')


MyConnection = MySQLdb.connect( host = "localhost", user = "root", \
     passwd = "", db = "siphseqs")
MyCursor = MyConnection.cursor()

# Loop over each line in the file
for seq_record in SeqIO.parse(InFile, "fasta") :
	seq = str(seq_record.seq)
	gene = ""
	desc = seq_record.description
	#print seq_record.name
	#print seq_record.name
	print desc
	note = ""
	taxon = ""
	gi = "NULL"
	accession = ""
	sample = ""
	
	if (len(gene)>0):
		fields = desc.split(' ')
		taxon = fields[0]
		gene = global_gene
		
	elif ncbi:
		# Assume it is a genbank download
		#print "\tNCBI"
		fields = desc.split('|')
		gi = fields[1]
		accession = fields[3]
		human = fields[4]
		human = human.strip()
		#print "\t" + human
		words = human.split(' ')
		taxon = words[0] + "_" + words[1]
		if (re.search("18S ribosomal RNA gene and internal transcribed spacer 1", human)):
			gene = "18S_ITS"
		elif (re.search("18S rRNA, internal transcribed spacer 1 and 5.8S rRNA", human)):
			gene = "18S_ITS"
		elif (re.search("18S", human)):
			gene = "18S"
		elif (re.search("small subunit ribosomal RNA", human)):
			gene = "18S"
		elif (re.search("28S", human)):
			gene = "28S"
		elif (re.search("16S", human)):
			gene = "16S"
		elif (re.search("elongation factor 1 alpha", human)):
			gene = "ef1a"
		elif (re.search("COI", human)):
			gene = "co1"
		elif (re.search("COX1", human)):
			gene = "co1"
		else:
			print "WARNING: gene not known"
	else:
		# It is my own header format
		# >Genus_species|gene|sample
		
		fields = desc.split('|')
		taxon = fields[0]
		gene = fields[1]
		sample = fields[2]
		
		
	#print "\t" + taxon
	#print "\t" + gene
		
		
	sql = """	INSERT INTO sequences SET
	taxon = '%s',
	description = '%s',
	gene = '%s',
	filename = '%s',
	seq = '%s',
	notes = '%s',
	gi = %s,
	accession = '%s',
	sample = '%s'
	;""" % (taxon, desc, gene, InFileName, seq, note, gi, accession, sample)
	print sql
	MyCursor.execute(sql)
InFile.close()
MyCursor.close()
MyConnection.close()