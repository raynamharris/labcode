import os,sys
from Bio import SeqIO

"""
this assumes that the cluster files are the fasta files in something
like cluster_out_2_1 not in the aligned folder

this will ouput for the clusters the number of seqs, number of taxa, and the mean lkength of the sequences
"""


if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "usage: python get_num_of_seqs_per_taxa_fasta.py file"
		sys.exit()	
	file = open(sys.argv[1],"r")
	counts = {} #key is the taxon, value is the count
	for i in SeqIO.parse(file,"fasta"):
		if i.id.split("@")[0] not in counts:
			counts[i.id.split("@")[0]] = 0
		counts[i.id.split("@")[0]] += 1
	file.close()
	
	skeys = counts.keys()
	skeys.sort()
	
	for i in skeys:
		print str(i)+"\t"+str(counts[i])
