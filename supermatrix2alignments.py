#!/usr/bin/env python

"""
Extracts individual genes from a supermatrix in PHYLIP format. Only extracts
cells from the matrix that are not all gaps. Exports the AA sequences 
to individual alignments in FASTA format, with species and 'gene number' (in the 
order implied by the supermatrix) as the header.

NOTE: THIS SCRIPT IS MODIFIED FROM THE UTILITY 'matrix2genes' in AGALMA 

"""

import sys
import os

from Bio import AlignIO

if len(sys.argv) < 3:
	print >>sys.stderr, "\nusage: %s <supermatrix.phy> <alignment format> <partitions.txt>\n%s" % (
		os.path.basename(sys.argv[0]), __doc__)
	sys.exit(0)

filename = os.path.basename(sys.argv[1])
align_format = sys.argv[2]
alignment = AlignIO.read(open(sys.argv[1]), align_format)

partition = []
for line in open(sys.argv[3]):
	# Parse the range specifier in the RAxML partition format:
	#  WAG,genename=i-j
	# and decrement i by one to get 0-based, exclusive ranges.
	i, j = map(int, line.rstrip().partition('=')[2].split('-'))
	partition.append((i-1, j))

for i, v in enumerate(partition):
	outfile = os.path.join(os.getcwd(), filename + str(i+1)+ ".fa")
	with open(outfile, "w") as f:
		for record in alignment:
			seq = str(record.seq[v[0]:v[1]]).replace('-', 'X')
			if seq != 'X'*len(seq):				
				print >>f, '>%s@%d' % (record.id.replace(' ', '_'), (i+1))
				print >>f, seq

