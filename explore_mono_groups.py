import os
import argparse
import sqlite3

def query_out(db, seq_ids):
	'''connects to database, performs query and
	prints result to fasta files'''

	conn = sqlite3.connect(db)
	c = conn.cursor()

	outfile_1 = os.path.join(os.getcwd(), "proteins.fa")
	outfile_2 = os.path.join(os.getcwd(), "nucleotides.fa")
	with open(outfile_1, "w") as fprot, open(outfile_2, "w") as fnuc:
		for i in seq_ids:
			c.execute('SELECT id, catalog_id, gene, aa_sequence, nt_sequence, confidence \
				FROM agalma_sequences \
				WHERE id = {id}'.format(id=i))
			all_rows=c.fetchall()
			for rec in all_rows:
				print >>fprot, '>{0}|{1}|{2}|{5}\n{3}'.format(*rec)
				print >>fnuc, '>{0}|{1}|{2}|{5}\n{4}'.format(*rec)

	conn.close()		

if __name__=="__main__":
	parser = argparse.ArgumentParser(description="Takes a sqlite \
		database and a list of sequence_ids and outputs 2 \
		FASTA files (proteins and nucleotides) with the sequences. \
		The headers are: sequence_id|gene|isoform|""")
	parser.add_argument('database', help='''(absolute or relative) path \
		to sqlite database.''')
	parser.add_argument('ids', nargs="*", type=int, 
	help='''list of sequence IDs separated by space.''')
	args = parser.parse_args()
	print "Processing database: ", args.database
	print "Querying sequence_ids: ", args.ids
	query_out(args.database, args.ids)
	
