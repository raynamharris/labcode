import sys,os
from Bio import SeqIO

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python check_for_failed_n_by_n_blasts.py fastafile mcl_in_file outfile"
		sys.exit(0)
	
	#print "reading seq ids"
	#fastafile = open(sys.argv[1],"r")
	#seqids = []
	#for i in SeqIO.parse(fastafile,"fasta"):
	#	seqids.append(i.id)
	#fastafile.close()
	#print "finished reading seq ids"
	
	print "reading mcl in"
	mcl_in = open(sys.argv[2],"r")
	seqidsmcl = set()
	count =0 
	for i in mcl_in:
		spls = i.strip().split("\t")
		quid = spls[1]
		seqidsmcl.add(quid)
		count += 1
		if count%10000==0:
			print count,len(seqidsmcl)
	mcl_in.close()
	print "finished reading mcl in"
	
	seqs = []
	fastafile = open(sys.argv[1],"r")
	for i in SeqIO.parse(fastafile,"fasta"):
		if i.id not in seqidsmcl:
			seqs.append(i)
	fastafile.close()
	
	outfile = open(sys.argv[3],"w")
	SeqIO.write(seqs,outfile,"fasta")
	outfile.close()