#!/usr/bin/env python
import sys
flowcell = sys.argv[1]
nseqs = 0
npassed = 0
for line in open(sys.argv[2]):
    nseqs += 1
    fields = line.strip().split("\t")
    assert len(fields) == 11
    if fields[10] == '1':
        npassed += 1
        print "@%s:%s:%s:%s:%s:%s:%s %s:N:0:%s\n%s\n+\n%s" % (
            fields[0], fields[1], flowcell, fields[2],
            fields[3], fields[4], fields[5],
            fields[7], fields[6],
            fields[8].replace('.', 'N'),
            ''.join(chr(ord(x)-31) for x in fields[9]))
print >>sys.stderr, npassed, "/", nseqs, "sequences passed"
