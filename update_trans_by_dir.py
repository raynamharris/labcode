import sys,sqlite3,os
from colors import *
from Bio import SeqIO

#database = "mollusk.db"

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python update_names_into_sqlite.py database DIR"
		sys.exit(0)
	database = sys.argv[1]
	if os.path.exists(database) == False:
		print "the database has not been created or you are not in the right place"
	con = sqlite3.connect(database)
	species = []
	cur = con.cursor()
	cur.execute("SELECT * FROM species_names;")
	a = cur.fetchall()
	for i in a:
		species.append(str(i[1]))
	DIR = sys.argv[2]
	for i in species:
		print GREEN+i+RESET
		sDIR = sys.argv[2]+"/"+i+"/translations"
		try:
			for j in os.listdir(sDIR):
				print "\t"+BLUE+j+RESET
				os.system("python ~/Dropbox/programming/labcode/update_trans_seqs_into_sqlite.py "+database+" "+i+" "+sDIR+"/"+j)
				#print "python ~/Dropbox/programming/phylogenomics/update_trans_seqs_into_sqlite.py "+i+" "+sDIR+"/"+j
		except:
			continue
	con.close()
