#!/usr/bin/env python	
import sys
import re
from collections import defaultdict
from Bio import SeqIO

Usage = """
Usage:

remap_solid.py gene.map results.tab mismatch

Converts contig counts from results.tab to gene counts. gene.map is used to map contigs to genes.

results.tab is the file produced by the solid mapper of tag mapping to the reference

mismatch is the maximum number of colorspace mismatches allowed, -1 applies no limit

The following output fields are generated for each gene:
gene- the gene name
all- the count of tags that map to each gene, regardless of if that tag maps to multiple genes
uniq- the count of gene-unique tags that map to each gene, where gene-unique means that the tag 
maps to one and only one gene (other tags are omitted from this count)

There are different ways to consider uniqueness, and the different types of uniqueness are handled 
differently. If a tag maps to multiple places within a contig, each mapping is counted. This is not 
desirable, but is relatively rare. This behavior is for computational efficiency- it allows the 
counting to be tag specific rather than read specific.

The other type of uniqueness is across contigs within genes. The genes set in the last loop keeps 
track of which genes a tag has been mapped to, and only adds the count for a given tag to a gene 
once. If a tag maps to multiple contigs in a gene, it will not be counted multiple times.  This 
accommodates situations where the some contigs are subsets of others, which would otherwise lead 
to some tags being overcounted towards a gene.

Finally, uniqueness across genes is reported in the output file, where uniq contains counts derived 
only from tags that map to no more than one gene.

"""

if len(sys.argv) < 4:
	print Usage
else:
	map_name = sys.argv[1]
	counts_name = sys.argv[2]
	mismatch = int(sys.argv[3])
	
	map = {} #Dictionary with key contig and value gene
	counts = {} #The total counts for each gene in map_name, including 0 for genes with no count
	map_handle = open(map_name, "rU")
	for line in map_handle:
		line = line.strip()
		contig, gene = line.split()
		#print contig, gene
		map[contig] = gene
		counts[gene] = 0
	
	counts_handle = open(counts_name, "rU")
	n = 0
	
	# Load the data into the maps
	ttoc = defaultdict(dict) # Dictionary with key tag and value dict. The nested dict has key contig and value count for contig for tag
	ttog = defaultdict(set) # Dictionary with key tag and value set of genes, used to identify unique tags that hit no more than one gene
	for line in counts_handle:
		n = n + 1
		if n < 2:
			continue
		line = line.strip()
		fields = line.split()
		#Tag_Seq GI_num  GI_Pos  Read_ID Mismatch
		mis = int(fields[4])
		if mis > mismatch and mismatch > -1:
			continue
		tag = fields[0]
		contig = fields[1]
		
		ttoc[tag][contig] = ttoc[tag].get(contig, 0) + 1
		
		gene = map.get(contig, '')
		if len(gene) < 1:
			sys.stderr.write("Warning: Contig %s found in %s but not %s\n" % (c, counts_name, map_name))
		
		ttog[tag].add(gene)
		
	
	all = {}
	uniq = {}
	
	for t in ttoc.keys():
		genes = set([])
		for contig in ttoc[t].keys():
			gene = map.get(contig, '')
			if len(gene) < 1:
				sys.stderr.write("Warning: Contig %s found in %s but not %s\n" % (c, counts_name, map_name))
			if not gene in genes:
				all[gene] = all.get(gene,0) + ttoc[t][contig]
				if len(ttog[t]) < 2:
					uniq[gene] = uniq.get(gene,0) + ttoc[t][contig]
			genes.add(gene)

	# Add up the counts from the maps
	print 'gene\tall\tuniq'
	for g in all.keys():
		print g +'\t'+ str(all[g]) +'\t'+ str(uniq.get(g,0))
