import os,sys

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print "usage: python alignment_split_folder.py INDIR numtosplit"
        sys.exit(0)
    DIR = sys.argv[1]
    files = []
    for i in os.listdir(DIR):
        if ".fasta" in i:
            files.append(i)

    numtosplit = int(sys.argv[2])
    start = 0
    total = len(files)
    for i in range(numtosplit):
        OUTDIR = sys.argv[1]+"."+str(i)
        os.mkdir(OUTDIR)
        os.mkdir(OUTDIR+"/aligned")
        print OUTDIR
        if i+1 == numtosplit:
            tfiles = files[start:]
            for j in tfiles:
                os.system("cp "+DIR+"/"+j+" "+OUTDIR+"/"+j)
        else:
            tfiles = files[start:start+int(total/numtosplit)]
            for j in tfiles:
                os.system("cp "+DIR+"/"+j+" "+OUTDIR+"/"+j)
            start = start+int(total/numtosplit)

