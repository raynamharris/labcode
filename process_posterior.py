#!/usr/bin/env python

"""Process posterior probabilities. Remove
N1 trees (burn in) and sample posterior every 
N2 trees. It requires both N1 and N2 numbers 
(N2 must be > 0)


USAGE: python process_posterior.py posterior_file N1 N2

"""

import os
import sys
import itertools

input_file = sys.argv[1]
skip = int(sys.argv[2])
every = int(sys.argv[3])

f1 = open(input_file, "r")
outfile = "trimmed_infile"

with open(outfile, "w") as f2:
        lines = itertools.islice(f1, skip, None, every)
        for line in lines:
                line = line.strip('\n')
                print>>f2, line
f1.close()
