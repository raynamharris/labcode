import sys,os
import subprocess
from Bio import SeqIO
from Bio.Blast.Applications import NcbiblastpCommandline
from Bio.Blast import NCBIXML
import math

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python create_blast_output_from_xml.py infile outfile"
		sys.exit(0)
	#open infile
	result_handle = open(sys.argv[1],"rU")
	outfile = open(sys.argv[2],"w")
	outfile.write("query\thitid\tevalue\tquerylen\talignmentlen\tidentities\tquerystart,queryend,sbjctstart,sbjctend\n")
	for k in NCBIXML.parse(result_handle):
		qutitle = str(k.query)
		for m in k.alignments:
			evalue = 0
			try:
				evalue = -math.log10(m.hsps[0].expect)
			except:
				evalue = 180 #largest -log10(evalue) seen
			hitid = str(m.hit_def)
			if hitid == qutitle: #don't want hits to self
				continue
			outfile.write(qutitle+"\t"+hitid+"\t"+str(evalue)+"\t"+str(k.query_letters)+"\t"+str(m.length))
			for j in m.hsps:
				outfile.write("\t"+str(j.identities)+"\t"+str(j.query_start)+","+str(j.query_end)+","+str(j.sbjct_start)+","+str(j.sbjct_end))
			outfile.write("\n")
	outfile.close()
