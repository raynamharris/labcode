import sys,os
from Bio import SeqIO

"""
makes individual seq and qlt files for the set of seqs in infile.fna
"""

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "usage: python make_individual_seq_qlt_files.py infile.fna infile.qual outdir"
		sys.exit(0)
	infas = open(sys.argv[1],"rU")
	inqlt = open(sys.argv[2],"rU")
	outdir = sys.argv[3]
	if os.path.isdir(outdir) == False:
		os.mkdir(outdir)
	allseqs = []
	for i in SeqIO.parse(infas,"fasta"):
		outf = open(outdir+"/"+str(i.id)+".seq","w")
		SeqIO.write([i], outf, "fasta")
		allseqs.append(i.id)
		outf.close()
	for i in SeqIO.parse(inqlt,"qual"):
		if i.id in allseqs:
			outf = open(outdir+"/"+str(i.id)+".qlt","w")
			SeqIO.write([i], outf, "qual")
			outf.close()
			allseqs.remove(i.id)
	print "missing:",allseqs
	infas.close()
	inqlt.close()
