
import sys,os
import sqlite3
from Bio import SeqIO
import mkdbdir

"""
this is going to go ahead and run the mkdbdir and individual scripts 
for automation of the dbest

the only input is the ncbi_id
"""

def get_parent_info(_id,cur):
	cur.execute("SELECT id from attributes where name = ?",("parent_id",))
	paid = str(cur.fetchall()[0][0])
	cur.execute("SELECT value from directories_triple where directories_id = "+str(_id)+" and attribute_id = "+str(paid),())
	parentid = str(cur.fetchall()[0][0])
	#print "parent:", parentid
	cur.execute("SELECT * from directories where id = ?",(parentid,))
	a = cur.fetchall()[0]
	padir = str(a[1])
	patype = str(a[2])
	return [parentid,padir,patype]


def run_partigene(ncbi_id,con,cur):
	parti_id = mkdbdir.partigene_create(ncbi_id,con,cur)
	cur.execute("SELECT path from directories where id = ?",(parti_id,))
	partigenedir = str(cur.fetchall()[0][0])
	curdir = os.getcwd()
	print "changing directory to "+partigenedir
	os.chdir(partigenedir)
	os.system("mkdir partigene")
	os.chdir(os.getcwd()+"/partigene")
	print "running partigene"
	cmd = "perl /home/smitty/Dropbox/projects/DUNN/scripts/PartiGene_predb_noninteractive.pl "+str(ncbi_id)+" ABC"
	os.system(cmd)
	return parti_id

def run_postcluster():
	os.chdir(os.getcwd()+"/../")
	os.system("mkdir postcluster")
	os.system("mkdir postcluster/protein")
	os.chdir(os.getcwd()+"/postcluster")
	os.system("perl /home/smitty/Dropbox/projects/DUNN/scripts/postcluster2 ../partigene/protein > analysis.log")
	os.system("python /home/smitty/Dropbox/programming/phylogenomics/fixpostcluster2_seqs.py processed.fasta processed.nc.fasta")
	
def run_blastx(parti_id,con,cur):
	blastx_id = mkdbdir.blastx_create(parti_id,con,cur)
	cur.execute("SELECT path from directories where id = ?",(blastx_id,))
	blastdir = str(cur.fetchall()[0][0])
	cmd = "blastx -db "+nrdb+" -query processed.nc.fasta -evalue 0.0001 -outfmt 5 -out "+blastdir+"/processed.blast.xml -num_threads=8"
	os.system(cmd)
	return blastx_id
	
def run_prot4est(blastx_id,parti_id,con,cur):
	prot4est_id = mkdbdir.prot4est_create([blastx_id,parti_id],con,cur)
	cur.execute("SELECT path from directories where id = ?",(prot4est_id,))
	prot4est_dir = str(cur.fetchall()[0][0])
	#change directory
	cur.execute("SELECT path from directories where id = ?",(parti_id,))
	partigenedir = str(cur.fetchall()[0][0])
	cur.execute("SELECT path from directories where id = ?",(blastx_id,))
	blastdir = str(cur.fetchall()[0][0])
	os.chdir(prot4est_dir)
	os.system("python /home/smitty/Dropbox/programming/phylogenomics/prot4EST_config_maker_for_db.py "+partigenedir+"/postcluster/processed.nc.fasta processed_out some_species "+blastdir+"/processed.blast.xml > config")
	os.system("python /home/smitty/Dropbox/programming/phylogenomics/build_cut.py "+partigenedir+"/postcluster/processed.nc.fasta "+blastdir+"/processed.blast.xml human_high.cod")
	os.system("cp -r "+partigenedir+"/postcluster/protein TEMP")
	os.system("perl  /home/smitty/Dropbox/projects/DUNN/scripts/prot4EST_noninteractive_SAS.pl")
	return [prot4est_id,prot4est_dir]
	
def run_blast2go(blastx_id,con,cur):
	blast2go_id = mkdbdir.blast2go_create(blastx_id,con,cur)
	cur.execute("SELECT path from directories where id  = ?", (blast2go_id,))
	blast2go_dir = str(cur.fetchall()[0][0])
	#change directory
	os.chdir(blast2go_dir)
	os.system("python /home/smitty/Dropbox/programming/parseblast.py")
	os.system("java -Xms1024m -Xmx2048m -jar ~/apps/b2g4pipe/blast2go.jar -in "+parseblastfile+" -v -a -out "+parseblastfile+".annot -prop ~/blast2go/blast2go.properties")

def enter_sequences_into_db_from_prot4est(fid,fpath,con,cur):
	print "entering sequences into prot4est"
	parentid,padir,patype = get_parent_info(fid,cur)
	if patype == "blastx": #need to get the partigene parent
		nparentid,npadir,npatype = get_parent_info(parentid,cur)
		assert npatype == "partigene"
		nuc_file = open(npadir+"/postcluster/processed.nc.fasta","r")
		seqs_nuc = {}
		seqs_qual = {}
		for i in SeqIO.parse(nuc_file,"fasta"):
			qual_file = open(fpath+"/TEMP/"+i.id+".qlt","r")
			b = list(SeqIO.parse(qual_file,"qual"))[0].letter_annotations['phred_quality']
			sb = ""
			for j in b:
				sb += str(j)+" "
			seqs_nuc[i.id[3:]] = i.seq.tostring()
			seqs_qual[i.id[3:]] = sb
		nuc_file.close()
		seqs_prot = {}
		seqs_meth = {}
		prot_file = open(fpath+"/processed_out/translations_xtn.fsa","r")
		for i in SeqIO.parse(prot_file,"fasta"):
			seqs_prot[i.id[3:]] = i.seq.tostring()
			seqs_meth[i.id[3:]] = i.description.split("Method: ")[1]
		prot_file.close()
		cur.execute("SELECT directories_triple.value from directories_triple where directories_triple.directories_id = ? and directories_triple.attribute_id = 1;",(fid,))
		taxonid = str(cur.fetchall()[0][0])
		#initial sequence entry with taxa , nuc, prot, qual,
		for i in seqs_nuc:
			try:
				id_ = cur.execute("INSERT into sequences (sequence_name,parent_id,taxa_id,nucleotide_seq,quality_seq,translated_seq,translation_method) values (?,?,?,?,?,?,?);" ,
					(i,fid,taxonid,seqs_nuc[i],seqs_qual[i],seqs_prot[i],seqs_meth[i]))
			except:
				if i not in seqs_prot:
					id_ = cur.execute("INSERT into sequences (sequence_name,parent_id,taxa_id,nucleotide_seq,quality_seq) values (?,?,?,?,?);" ,
						(i,fid,taxonid,seqs_nuc[i],seqs_qual[i]))
					
		con.commit()

nrdb = "/home/smitty/lib/blast_data/nr"

if __name__ == "__main__":
	if len(sys.argv) <= 1:
		print "usage: dbest_wrapper ncbi_id"
		print "usage: dbest_wrapper resume path"
		sys.exit(0)
	con = sqlite3.connect(mkdbdir.database)
	cur = con.cursor()
	ncbi_id = ""
	if sys.argv[1] == "resume":
		path = sys.argv[2]
		cur.execute("SELECT * from directories where path like '%"+path.split("/")[-1]+"%'",())
		a = cur.fetchall()[0]
		fpath = str(a[1])
		fid = str(a[0])
		typ = str(a[2])
		print fpath,fid,typ
		if typ == "blastx":
			parentid,padir,patype = get_parent_info(fid,cur)
			assert patype == "partigene"
			print "continuing from blastx to prot4EST"
			run_prot4est(fid,parentid,con,cur)
		"""
		this will enter these sequences into the database, both the nucleotide and the protein
		"""
		if typ == "prot4est":
			enter_sequences_into_db_from_prot4est(fid,fpath,con,cur)
	else:
		ncbi_id = sys.argv[1]
		parti_id = run_partigene(ncbi_id,con,cur)
		#need to add a check if things didn't work out (very likely if the files are large in which case manual partigene is required)
		#now running postcluster things
		run_postcluster()
	
		#need some error checking to see if anything failed to this point (unlikely)
		#would be nice to move some of these over to the cluster queue but that 
		#requires that we don't move to the next steps until that is complete
		#might be able to implement that but making a while loop that has a pause in it
		#where it checks to see if the blastfile is there

		#now run the blast
		blastx_id = run_blastx(parti_id,con,cur)

		#now run prot4est
		p4estid,p4estdir = run_prot4est(blastx_id,parti_id,con,cur)
		#enter the nucleotide and amino acid sequences into the database
		enter_sequences_into_db_from_prot4est(p4estid,p4estdir,con,cur)

		#now run blast2go
		#run_blast2go(blastx_id,con,cur)
