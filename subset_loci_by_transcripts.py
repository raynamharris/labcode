import sys
from Bio import SeqIO

"""
this will reduce the transcripts.fa file from oases
to only those loci that have fewer than or equal to the number
of transcripts set
"""

"""
this also throws out short things if you turn this on
"""
CUT_SHORT = True
CUT_LENGTH = 150

MAX_TRAN = 20

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python subset_loci_by_transcripts.py infile.fa outfile.fa"
		sys.exit(0)
	
	keep = []
	
	infile  = open(sys.argv[1],"r")
	locus = {} #key is locus label, value is number of transcripts
	count = 0
	for i in SeqIO.parse(infile,"fasta"):
		spls = i.id.split("_Transcript_")
		spls2 = spls[1].split("_Confidence_")
		num = int(spls2[0].split("/")[1])
		if num < MAX_TRAN:# and num > 5:#and for blasting taking out the ones that only have one
			if CUT_SHORT == True:
				if len(i.seq) >= CUT_LENGTH:
					keep.append(i)	
			else:
				keep.append(i)
	infile.close()
	
	outfile = open(sys.argv[2],"w")
	SeqIO.write(keep,outfile,"fasta")
	outfile.close()