import sys, dendropy
from collections import defaultdict

"""

"""

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "python trinity_tree_taxa_tree.py trees.tre"
		sys.exit(0)
	file_name = sys.argv[1]
	treelist = dendropy.TreeList()
	treelist.read_from_path(file_name, schema="newick")

	taxa_tree = defaultdict(set)
	taxa_count = defaultdict(int)

	i = 0

	for tree in treelist:
		#leaves = []
		for leaf in tree.leaf_nodes():
			label = leaf.taxon.label
			label = ' '.join( label.split( ' ' )[:-1] )
			#leaves.append(label)
			taxa_tree[label].add(i)

		i = i + 1

	i = 0
	for taxon in taxa_tree.keys():
		n = len(taxa_tree[ taxon ])
		if n > 1:
			print taxon + '\t' + str( n )

		if i < 0:
			print taxa_tree[ taxon ]

		i = i + 1
