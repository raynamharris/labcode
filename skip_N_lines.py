#!/usr/bin/env python

"""Skip a given number of lines in a text file.
Written for processing tree files resulting from 
Bayesian searches to remove burnin.

USAGE: python skip_N_lines.py N

"""

import os
import sys

f1 = open(sys.argv[1], "r")
outfile = "trimmed_file"
lines = f1.readlines()[int(sys.argv[2]):]

with open(outfile, "w") as f2:
	for line in lines:
		line = line.strip('\n')
		print>>f2, line
f1.close()
