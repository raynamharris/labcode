from Bio import SeqIO
import sys

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "usage: python get_all_seq_lengths.py fastafile"
		sys.exit(0)
	handle = open(sys.argv[1],"rU")
	for i in SeqIO.parse(handle,"fasta"):
		print len(i.seq)
	handle.close()
