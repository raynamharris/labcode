import sys,os,sqlite3
from Bio.Blast import NCBIXML
from Bio import SeqIO
from colors import *
import math

DB = "/home/smitty/Desktop/pln.178.db"

#Trematoda Taxonomy ID:6178
#Microsporidia Taxonomy ID: 6029
PARASITES = [6178,6029]

"""
this should look at the structure within the analyses folder and examine
the files in the BLAST directory first 

then it will take the name of the directory (name of the species) plus 
the isotig as the guid for our sequences. then it will take the blast 
accession as the guid for the ref seq.

if the first hit is to a parasite, the sequence will be skipped

the mcl will do the clustering on the refs (nr database hits) and our
seqs that hit on those.

this will only cluster things that have at least one hit with BLAST 
(tblastx)
"""

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python create_mcl_input_from_folder_structure.py INDIR outfile"
		sys.exit(0)

	#this is for reading in the parasite information
	conn = sqlite3.connect(DB)
	
	#each position corresponds to a taxon
	lefts = []
	rights = []
	
	for i in PARASITES:
		c = conn.cursor()
		c.execute("select left_value,right_value from taxonomy where ncbi_id=?", (i,))
		c.close()
		fl = None
		fr = None
		for j in c:
			fl = int(j[0])
			fr = int(j[1])
		lefts.append(fl)
		rights.append(fr)


	INDIR = sys.argv[1]
	out = open(sys.argv[2],"w")
	for i in os.listdir(INDIR):
		#work with BLAST
		species_name = i
		print "working with BLAST from",BOLD,species_name,RESET
		BLASTDIR = INDIR+"/"+i+"/BLAST/"
		for j in os.listdir(BLASTDIR):
			if "blast.xml" in j:
				print BLASTDIR+BLUE+j+NORMAL
				bhandle = open(BLASTDIR+j,"rU")
				for k in NCBIXML.parse(bhandle):
					#with the species_name, this makes the guid
					qutitle = k.query.split(" ")[0]
					bad = False
					for m in k.alignments:
						evalue = 0
						try:
							evalue = -math.log10(m.hsps[0].expect) #get the first hit
						except:
							evalue = 180 #largest -log10(evalue) seen
						hitid = ""
						try:
							name = m.hit_def.split("[")[1].split("]")[0] 
							c = conn.cursor()
							c.execute("select left_value,right_value,ncbi_id from taxonomy where name=?", (str(name),))
							c.close()
							fl = None
							fr = None
							fncbi_id = None
							for n in c:
								fl = int(n[0])
								fr = int(n[1])
								fncbi_id = int(n[2])
							for n in range(len(lefts)):
								if fl > lefts[n] and fr < rights[n]:
									bad = True
							hitid = m.hit_id.split("|")[1]
						except: #just no name hit for parasite
							hitid = m.hit_id.split("|")[1] #verify this is the accession
						if bad == False:
							out.write(hitid+"\t"+species_name+"_"+qutitle+"\t"+str(evalue)+"\n")
				bhandle.close()
	out.close()
		
