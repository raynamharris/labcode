import sqlite3,sys
from Bio import SeqIO
from Bio.Blast import NCBIXML


DB = "/home/smitty/Desktop/pln.180.db"

MAXBLAST = 1 #number of blast hits to look at

"""
by using an sqlite database, get the left right for the parasites and see if the blast hits
are within those left rights
"""

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python get_taxa_distribution_blast_xml.py blast.xml outfile"
		sys.exit(0)
	#open infile
	
	conn = sqlite3.connect(DB)
	
	inblast = open(sys.argv[1],"rU")
	for j in NCBIXML.parse(inblast):
		qutitle = j.query.split(" ")[0]
		bad = False
		if len(j.alignments) > 0:
			count = 0
			for k in j.alignments:
				if count >= MAXBLAST:
					break
				try:
					count += 1
					name = k.hit_def.split("[")[1].split("]")[0]
					going = True
					name_string = ""
					ncbiid = None
					while going == True:
						c = conn.cursor()
						if ncbiid == None:
							c.execute("select name,parent_ncbi_id from taxonomy where name=?", (str(name),))
						else:
							c.execute("select name,parent_ncbi_id from taxonomy where ncbi_id=?", (str(ncbiid),))
						c.close()
						nm = None
						fncbi_id = None
						for m in c:
							nm = str(m[0])
							fncbi_id = int(m[1])
						name_string += nm+";"
						ncbiid = fncbi_id
						if fncbi_id == 1:
							going = False
							break
					print qutitle+"="+name_string
				except:
					#print k.hit_def
					count += 1
					continue
				
	inblast.close()
