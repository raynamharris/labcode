import os,sys

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python rename_contig_pdfs_to_isogroups.py keyfile dir"
		sys.exit(0) # stop reading after showing usage.
	keyfile = open(sys.argv[1],"rU") #  sys.argv:[1] is the first readable argument. sys argv[0] is the program itself
	#key: contig value: isogroup
	keydict = {}
	for i in keyfile:
		if len(i) > 2:
			spls = i.strip().split("\t")
			keydict[spls[0]] = spls[1] # first elemnt in line, and second element value
	keyfile.close()

	direc = sys.argv[2]
	for i in keydict:
		try:
			os.rename(direc+"/"+i+".pdf",direc+"/"+keydict[i]+"_"+i+".pdf")
		except:
			continue
