import sys,os
import subprocess
from Bio import SeqIO
from Bio.Blast.Applications import NcbiblastpCommandline
from Bio.Blast import NCBIXML
import math

DEBUG = False

def blast2(seq1,dbf):
	in1 = open(seq1.id,"w")
	in1.write(">"+seq1.id+"\n"+seq1.seq.tostring().upper()+"\n")
	in1.close()
	cline = NcbiblastpCommandline(query=seq1.id, db=dbf, evalue=0.0001, outfmt=5,out=seq1.id+".xml")
	print cline
	return_code = subprocess.call(str(cline), shell=(sys.platform!="win32"))
	#sys.exit()

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python create_mcl_input_masked_seqs_self_blast.py infile outfile"
		sys.exit(0)
	#create a blastDB for the infile
	print "making db"
	cmd = "makeblastdb -in "+sys.argv[1]+" -dbtype prot -out "+ sys.argv[1]+".db"
	os.system(cmd)
	print "finished making db"
	#open infile
	infile = open(sys.argv[1],"rU")
	allseqs = []
	allseqs_dict = {}
	for i in SeqIO.parse(infile,"fasta"):
		allseqs.append(i)
		allseqs_dict[i.id] = i
	allseqs.reverse()
	outfile = open(sys.argv[2],"w")
	while len(allseqs) > 0:
		i = allseqs.pop()
		match = False
		blast2(i,sys.argv[1]+".db")
		result_handle = open(i.id+".xml","rU")
		try:
			for k in NCBIXML.parse(result_handle):
				qutitle = str(k.query)
				print qutitle
				for m in k.alignments:
					evalue = 0
					try:
						evalue = -math.log10(m.hsps[0].expect)
					except:
						evalue = 180 #largest -log10(evalue) seen
					hitid = str(m.hit_def)
					if hitid == qutitle: #don't want hits to self
						continue
					outfile.write(hitid+"\t"+qutitle+"\t"+str(evalue)+"\n")
			result_handle.close()
			os.remove(i.id)#remove file
			os.remove(i.id+".xml")#remove file
		except:#fix weird nonunicode characters
			result_handle.close()
			result_handle = open(i.id+".xml","rU")
			result_handle2 = open(i.id+".2.xml","w")
			for k in result_handle:
				result_handle2.write(str(unicode(k,errors="replace").replace(u'\ufffd',"-")))
			result_handle.close()
			result_handle2.close()
			result_handle2 = open(i.id+".2.xml","rU")
			for k in NCBIXML.parse(result_handle2):
				qutitle = str(k.query)
				print qutitle
				for m in k.alignments:
					evalue = 0
					try:
						evalue = -math.log10(m.hsps[0].expect)
					except:
						evalue = 180 #largest -log10(evalue) seen
					hitid = str(m.hit_def)
					if hitid == qutitle: #don't want hits to self
						continue
					outfile.write(hitid+"\t"+qutitle+"\t"+str(evalue)+"\n")
			result_handle2.close()
			os.remove(i.id)#remove file
			os.remove(i.id+".xml")#remove file
			os.remove(i.id+".2.xml")#remove file
	outfile.close()
