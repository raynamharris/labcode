import sys,sqlite3,os
from Bio import SeqIO
from colors import *

#database = "mollusk.db"


if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "usage: python update_trans_seqs_into_sqlite.py database species_name file"
		sys.exit(0)
	database = sys.argv[1]
	if os.path.exists(database) == False:
		print "the database has not been created or you are not in the right place"
	con = sqlite3.connect(database)
	species_name = sys.argv[2]
	cur = con.cursor()
	cur.execute("SELECT id FROM species_names where name = ?;",(species_name,))
	a = cur.fetchall()
	if len(a) == 0:
		print RED,"no species",species_name,"in database",RESET
		con.close()
		sys.exit(0)
	else:
		dbid = a[0][0]
		print BLUE+"found",species_name+RESET,"id"+GREEN,dbid,RESET
		handle = open(sys.argv[3],"rU")
		filen = os.path.join(os.path.abspath("."),sys.argv[3])
		seqstoenter = []
		for i in SeqIO.parse(handle,"fasta"):
			fsid = i.id
			sid = i.id
			#correcting the prot4EST letters in the front
			if len(sid) == 8:
				try:
					int(sid[3:])
					sid = sid[3:]
				except:
					sid = sid
			#correcting jgi ids
			if sid[:3] == "jgi":
				sid = sid.split("|")[2]
			cur.execute("SELECT seq_id,seq FROM translated_seqs where edited_seq_id = ? and species_names_id = ?",(sid,dbid));
			b = cur.fetchall();
			if len(b) > 0:
				print RED+"found a sequence with the same id, skipping"+RESET
				print "\t"+RED+fsid,sid+RESET
			else:
				seqstoenter.append([dbid,fsid,sid,i.seq.tostring(),filen])
		cur=con.cursor()
		for i in seqstoenter:
				cur.execute("INSERT INTO translated_seqs(species_names_id,seq_id,edited_seq_id,seq,file) values(?,?,?,?,?)",(i[0],i[1],i[2],i[3],i[4]))
		con.commit()
		handle.close()
	con.close()
