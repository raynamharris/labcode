# Written by Casey Dunn, 2011
# Brown University
# Please see the LICENSE file included as part of this package.
"""sanitize_illumina_reads.py removes illumina paired reads according to several criteria.

It excludes read pairs that:
- With low quality data
- Include illumina adapter sequences.


Reads that did not pass Illumina filter should be removed prior to running this script

type the following for more information:

sanitize_illumina_reads.py -h

"""

from numpy import mean
from optparse import OptionParser
import re
from biolite import seqlite
from biolite.seqliteio import SeqLiteIO


def sanitize_illumina_reads(read1_in_name, read2_in_name, read1_out_name, read2_out_name, input_format = 'fastq', verbose = False, removeadapters = True, thin = -1):
	"""Removes illumina paired reads according to several criteria, writing reduced set to a new file.
	
	"""
	
	print("Running sanitize_illumina_reads")
	print(" read1_in: {0}".format(read1_in_name))
	print(" read1_out: {0}".format(read1_out_name))
	print(" read2_in: {0}".format(read2_in_name))
	print(" read2_out: {0}".format(read2_out_name))
	
	qualoffset = seqlite.get_offset(input_format)
	
	if len(set([read1_in_name, read2_in_name, read1_out_name, read2_out_name])) < 4:
		raise ValueError("All input and output files must ahve different names, or you could clobber your data")
	
	# Prepare files
	read1_in = SeqLiteIO(read1_in_name, "r")
	read2_in = SeqLiteIO(read2_in_name, "r")
	
	read1_out = SeqLiteIO(read1_out_name, "w")
	read2_out = SeqLiteIO(read2_out_name, "w")
	
	# Initialize counters
	total_readpairs = 0
	total_fail_quality = 0
	total_fail_adapter = 0
	
	
	# Set to -1 when a criterion isn't evaluated
	if thin < 0:
		total_fail_quality = -1
	if not removeadapters:
		total_fail_adapter = -1
	
	#
	# In earlier versions of Illumina library prep, the adapter sequences are:
	# GATCGGAAGAGCGGTTCAGCAGGAATGCCGAG
	# ACACTCTTTCCCTACACGACGCTCTTCCGATCT
	#
	# These are annelaed into forked adapters that are ligated to the fragment ends, which have an
	# A overhang. The sequences that are seen in the reads when the inserts are shorter than the
	# read length are:
	# AGATCGGAAGAGCGGTTCAGCAGGAATGCCGAG   Seen in read 1, has A from ligation
	# AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT   Seen in read 2, reverse compliment of adapter
	
	
	# AGATCGGAAGAGCG is the portion of the adapter sequences that is adjacent to the insert and is 
	# common between both oligos in the forked adapters. GTTCAGCAGGAATG and TCGTGTAGGGAAA are 
	# specific to the adapters for each read end. They are added to catch the small number of 
	# cases where the common sequence is mutated in both reads. Of coursse they will miss cases 
	# where they are also partially or completely truncated and the common sequence is mutated,
	# but that will be very rare.
	
	# With TruSeq, they modified the adapter sequences:
	# AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT			TruSeq Universal Adapter, revcomp
	#  GATCGGAAGAGCACACGTCTGAACTCCAGTCACATCACGATCTCGTATGCCGTCTTCTGCTTG	TruSeq Adapter, Index 1
	#  GATCGGAAGAGCACACGTCTGAACTCCAGTCACCGATGTATCTCGTATGCCGTCTTCTGCTTG	TruSeq Adapter, Index 2
	#  GATCGGAAGAGCACACGTCTGAACTCCAGTCACTTAGGCATCTCGTATGCCGTCTTCTGCTTG	TruSeq Adapter, Index 3
	#  GATCGGAAGAGCACACGTCTGAACTCCAGTCACTGACCAATCTCGTATGCCGTCTTCTGCTTG	TruSeq Adapter, Index 4
	#  GATCGGAAGAGCACACGTCTGAACTCCAGTCACACAGTGATCTCGTATGCCGTCTTCTGCTTG	TruSeq Adapter, Index 5
	#  GATCGGAAGAGCACACGTCTGAACTCCAGTCACGCCAATATCTCGTATGCCGTCTTCTGCTTG	TruSeq Adapter, Index 6
	#  GATCGGAAGAGCACACGTCTGAACTCCAGTCACCAGATCATCTCGTATGCCGTCTTCTGCTTG	TruSeq Adapter, Index 7
	#  GATCGGAAGAGCACACGTCTGAACTCCAGTCACACTTGAATCTCGTATGCCGTCTTCTGCTTG	TruSeq Adapter, Index 8
	#  GATCGGAAGAGCACACGTCTGAACTCCAGTCACGATCAGATCTCGTATGCCGTCTTCTGCTTG	TruSeq Adapter, Index 9
	#  GATCGGAAGAGCACACGTCTGAACTCCAGTCACTAGCTTATCTCGTATGCCGTCTTCTGCTTG	TruSeq Adapter, Index 10
	#  GATCGGAAGAGCACACGTCTGAACTCCAGTCACGGCTACATCTCGTATGCCGTCTTCTGCTTG	TruSeq Adapter, Index 11
	#  GATCGGAAGAGCACACGTCTGAACTCCAGTCACCTTGTAATCTCGTATGCCGTCTTCTGCTTG	TruSeq Adapter, Index 12
	# 
	# An A is added to the start of all the indexed adapters during ligation.
	# Baring mutation, this sequence should be adjacent to the insert when the insert is shorter 
	# then read length:
	# AGATCGGAAGAGC
	# This is a subset of the adapter for the older chemistry, and therefore supercedes it.
	
	# The sequences specific to each read that are adjacent to this conserved region are:
	# GTCGTGTAGGGAAA
	# CACACGTCTGAACT
	# These are added in case there is a mutation in the conserved region
	#
	# So the combined regex for old and new adapters is:
	adapter_re = re.compile(r"AGATCGGAAGAGC|GTTCAGCAGGAATG|TCGTGTAGGGAAAG|GTCGTGTAGGGAAA|CACACGTCTGAACT")
	
	# Loop over the reads
	while 1:
		# Read the next records from the files
		try:
			record1 = read1_in.next()
		except StopIteration:
			record1 = None
		except ValueError:
			print ("{0} reads were read successfully prior to the error in {1}".format(total_readpairs, read1_in_name))
			raise
		
		try:
			record2 = read2_in.next()
		except StopIteration:
			record2 = None
		except ValueError:
			print ("{0} reads were read successfully prior to the error in {1}".format(total_readpairs, read2_in_name))
			raise
		
		# Finish up if both records were empty
		if (record1 == None) and (record2 == None):
			break
			
		# Point out that something is amiss if one file but not the other is out of reads
		if (record1 == None) or (record2 == None):
			raise ValueError("Error: The input files {0} and {1} do not have an equal number of reads, problem found after reading {2} reads.".format(read1_in_name, read2_in_name, total_readpairs))		

		
		# Increment read counter
		total_readpairs = total_readpairs + 1
		
		
		# Make sure the two reads belong to the same cluster
		if record1.get_illumina_cluster_id() != record2.get_illumina_cluster_id():
			raise ValueError("Headers for read pairs do not agree for reads {0}".format(total_readpairs))
		
		
		# Exclude sequences that have low mean quality
		if thin > 0:
			if (mean(record1.get_qual()) < thin) or (mean(record2.get_qual()) < thin):
				total_fail_quality = total_fail_quality + 1
				continue
		
		# Exclude sequence pairs that have adapter sequence in either read
 
		if removeadapters and (adapter_re.search( record1.get_seq() ) or adapter_re.search( record2.get_seq() )):
			total_fail_adapter = total_fail_adapter + 1
			continue

		# Write the sequences
		#seqlite.write_fastq_record(record1, read1_out)
		#seqlite.write_fastq_record(record2, read2_out)
		read1_out.write(record1)
		read2_out.write(record2)

		
	
	read1_out.close()
	read2_out.close()
	
	print "total_readpairs: " + str(total_readpairs)
	print "total_fail_quality: " + str(total_fail_quality)
	print "total_fail_adapter: " + str(total_fail_adapter)
	
	diagnostics = {'total_readpairs' : total_readpairs, 'total_fail_quality' : total_fail_quality, 'total_fail_adapter' : total_fail_adapter}
	
	print "Successful completion."
	
	return diagnostics

def main():
	"""Parse options for command line access
	
	"""
	
	# This bit is a bit kludgey since defaults are redundantly defined in parser.add_option and by
	# the sanitize_illumina_reads function. This needs to be migrated to argparse and cleaned up.
	
	parser = OptionParser()
	parser.add_option("-a", "--read1in", action="store", type="string", dest="read1_in_name", help="Path to the fastq file containing read 1")
	parser.add_option("-b", "--read2in", action="store", type="string", dest="read2_in_name", help="Path to the fastq file containing read 2")
	parser.add_option("-c", "--read1out", action="store", type="string", dest="read1_out_name", help="Path for the output fastq file for read 1")
	parser.add_option("-d", "--read2out", action="store", type="string", dest="read2_out_name", help="Path for the output fastq file for read 2")
	parser.add_option("-f", "--format", action="store", type="string", dest="input_format", default="fastq", help="Input file format, see BioPython SeqIO documentation")
	parser.add_option("-v", action="store_true", dest="verbose", default=False, help="Set this flag for verbose output")
	parser.add_option("-n", "--thin", action="store", type="float", dest="thin", default=-1, help="Remove pairs of reads where either read has a mean quality lower than this value")
	(options, args) = parser.parse_args()
	
	sanitize_illumina_reads(options.read1_in_name, options.read2_in_name, options.read1_out_name, options.read2_out_name, input_format = options.input_format, suffix = options.suffix, verbose = options.verbose, thin = options.thin)
	
	
	


if __name__ == "__main__":
	main()
	