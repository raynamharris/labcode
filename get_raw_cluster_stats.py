import os,sys
from Bio import SeqIO

"""
this assumes that the cluster files are the fasta files in something
like cluster_out_2_1 not in the aligned folder

this will ouput for the clusters the number of seqs, number of taxa, and the mean lkength of the sequences
"""

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python get_raw_cluster_stats.py folder outfile"
		sys.exit()	
	folder = sys.argv[1]
	clusters = []
	for i in os.listdir(folder):
		if "fasta" in i:
			x = open(str(folder)+"/"+str(i),"r")
			numseqs = 0
			meanlength = 0
			taxa = []
			for s in SeqIO.parse(x,"fasta"):
				numseqs += 1
				if s.id.split("@")[0] not in taxa:
					taxa.append(s.id.split("@")[0])
				meanlength += float(len(s.seq.tostring()))
			meanlength /= numseqs
			numtaxa = len(taxa)
			clusters.append([numseqs,numtaxa,meanlength])
			x.close()

	out = open(sys.argv[2],"w")
	for i in clusters:
		out.write(str(i[0])+"\t"+str(i[1])+"\t"+str(i[2])+"\n")
	out.close()
