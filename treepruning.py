import dendropy
import sys
import random

def get_species( labels ):
	"""Returns a list of species names given a list of taxon names, each name 
	in the format species@stuff
	
	"""
	species = list()
	
	for label in labels:
		sp = label.split("@")[0]
		species.append( sp )
	
	return species


def get_taxa( nodes ):
	"""Returns a list of taxa names given a list of nodes
	"""
	
	taxa = list()
	for node in nodes:
		taxa.append( node.get_node_str() )
	return taxa


def ortholog_count( leaves ):
	"""Returns the count of orthologs given a list of taxon names. If there 
	are as many species as there are taxon names, then the count is the number 
	of taxon names. If there are less species than taxon names, then the count 
	is 0 since there must be some paralogs
	
	"""
	species =  get_species( leaves )
	count = 0
	
	if len( species ) == len( set( species ) ):
		count = len( species )
	
	return( count )

def ortholog_count_on_tree( tree ):
	"""If all genes in the tree are orthologs, returns the number of orthologs.
	Otherwise, it reutrns 0.
	
	"""

	return( ortholog_count( get_taxa( tree.leaf_nodes() ) ) )


def monophyly_masking (tree):
	"""Takes a tree and identifies clades that have more than one sequence
	per taxon and prunes tip at random leaving a single representative sequence per taxon

	"""
	
	all_leaves = tree.leaf_nodes()
	
	for leaf in all_leaves:
		sister = leaf.sister_nodes()[0]
		if str(leaf.taxon).split('@')[0] == str(sister.taxon).split('@')[0]:
			tree.prune_taxa_with_labels([str(leaf.taxon)], update_splits = False)
	return tree	
		 

def break_tree_at_node ( tree1, node ):
	"""Takes a tree and a node, and returns a tuple of trees. If the node isn't 
	in the tree, the tuple contains the original tree. If the node is in the 
	tree, the tuple contains two trees. One is the 
	subtree that is defined by the node, the other is the tree that is left 
	after the subtree is removed. The trees may be rerooted in the process.
	"""
	
	# Check if the node is in the tree
	print "break_tree_at_node"
	if not node in tree1.nodes():
		print "node not in tree"
	#	print tree1.as_ascii_plot()
		return( [ tree1 ] )
	print "Breaking tree...", tree1
	#print tree1.as_ascii_plot()
	print "This is the node where clipping will happen: ", node
	
	# The code below won't work if node is the root. Reroot the tree if node 
	# is the root
	print "This is the root node in the original tree: ", tree1.seed_node
	print "If the previous node is really the root, it should have NO parents; parents?: ", tree1.seed_node.parent_node
	print "This is the parent node of the node where clipping will happen: ", node.parent_node
	if node == tree1.seed_node or node.parent_node == tree1.seed_node or node.parent_node == tree1.seed_node.parent_node:
		# Get internal children
		print "rerooting..."
		new_root = None
		children = tree1.nodes()
		for child in children:
			if child.parent_node:
				new_root = child
				print "this is the new root: ", new_root
				print "this is the parent of the new root", new_root.parent_node
				print "------------"
		# Reroot at the edge subtending the new_root node
		if new_root is not None:
			tree1.reroot_at_edge( new_root.edge )
			tree1.update_splits()
		else:
                        # both children are leaves, so this is a two taxon tree. Won't be 
                        # useful for anything, so pass it back as-is
                        return( [ tree1 ] )
	
	# tree2 is the subtree that arises from the node. This code is modified from Jeet's
	# approach at:
	# https://groups.google.com/group/dendropy-users/browse_frm/month/2011-04?pli=1
	tree2 = dendropy.Tree(tree1)
	tree2.seed_node = node 
	
	print "This is the original tree after rerooting: ", tree1
	#print tree1.as_ascii_plot()	
	print "This is the root after rerooting: ", tree1.seed_node
	print "This is the subtree to be clipped: ", tree2
	#print tree2.as_ascii_plot()
	print "Node of clipping point: ", node
	print "Parent of clipping node: ", node.parent_node
	#print "Child nodes of clipping node: ", node.child_nodes()
	print "Root of subtree to be clipped: ", tree2.seed_node
	print "Parent of root of subtree to be clipped: ", tree2.seed_node.parent_node	
	
	# Prune tree1 so that it doesn't include the clade specified by tree2
	tree1.prune_subtree(node)

	print "These are the resulting subtrees:"
	print "Tree1: ", tree1
	print "Tree2: ", tree2	
	#print tree1.as_ascii_plot()
	#print tree2.as_ascii_plot()

	return( [tree1, tree2] )
	

def clip_orthologs( tree ):
	"""Takes a tree and loops through the internal nodes (except root)
	and gets the maximum number of orthologs on either side of each node

	"""	
	# A dictionary will keep track of node (key) and maximum number of
	# orthologs at this node (value)	
	counts = dict()
	leaves_all = set( get_taxa(tree.leaf_nodes()) )
	for node in tree.nodes():
		if node.parent_node:
			
			leaves1 = set( get_taxa( node.leaf_nodes() ) )
			
			leaves2 = leaves_all - leaves1
			
			# print ortholog_count(leaves1), ortholog_count(leaves2)
			
			count = max( ortholog_count(leaves1), ortholog_count(leaves2) )
			
			counts[node] = count
			
			# print leaves1
			# print leaves2
			# print count
	
	# Calculate the maximum number of orthologs across all nodes
	m = max( counts.values() )
	
	# Create a list of the nodes that have this max value, these are the nodes 
	# we will clip at
	
	clips = list()
	for key in counts.keys():
		if counts[key] == m:
			clips.append( key )
			
	print "Maximum number of orthologs across all nodes: ", m
	print "Nodes where clipping will happen: ", clips
	
	subtrees = [ tree ]	

	# Now do the clipping. The challenge is that there may be multiple clip 
	# points, and once we have multiple subtrees we don't know which clip point 
	# is in which tree. So we try to clip in all of them.
	for clip in clips:
		clipped_trees = list()
		for subtree in subtrees:
			results = break_tree_at_node( subtree, clip )
			print  results
			clipped_trees = clipped_trees + results
		subtrees = clipped_trees
	
	return( subtrees )	
			

def paralogy_prune( tree ):
	"""Takes a tree and progressively prunes it creating subtrees that only include 
	 orthologs for each taxon.
	
	"""
	residual = None
	orthologs = list()
	if ortholog_count_on_tree( tree ) > 0:
		orthologs.append(tree)
		return(orthologs)

	residual = tree
	while( residual is not None ):
		clipped_trees = clip_orthologs( residual )
		residual = None
		for clipped_tree in clipped_trees:
			if ortholog_count_on_tree( clipped_tree ) == 0:
				if ( residual is not None ):
					raise ValueError('More than one subtree has paralogs')
				residual = clipped_tree
				print residual
			else:
				orthologs.append( clipped_tree )
	
	return( orthologs )
	

if __name__ == "__main__":
	if len(sys.argv) < 2:
		print "usage: python paralogyprune.py tree.tre"
		sys.exit( 0 )
	in_name = sys.argv[1]
	tree = dendropy.Tree(stream=open( in_name, "rU" ), schema="newick" )
	masked_tree = monophyly_masking (tree)
	mm_name = "{0}.mm".format(in_name)
	masked_tree.write_to_path(mm_name, 'newick' , as_unrooted = True)
	clipped_trees = paralogy_prune( masked_tree )
	for i, tree in enumerate(clipped_trees):
		name = "{0}.pp.{1}".format( in_name, i )
		tree.write_to_path( name, 'newick' , as_unrooted = True)

