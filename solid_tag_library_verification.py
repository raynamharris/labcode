import sys

"""
this is for taking the files that look like

and

and




and running them through so that tags located in results.tab
are found with qualities (.qual) greater than a score and then hash and 
counted from the csfasta file
"""

if __name__ == "__main__":
	if len(sys.argv) != 5:
		print "usage: python solid_tag_library_verification.py results.tab qual csfasta outfile"
		sys.exit()
	
	min = 4
	med = 4
	
	print "rtab"
	rtab = open(sys.argv[1],"r")
	tags = set([])
	first = True
	for i in rtab:
		if first == True:
			first = False
			continue
		spls = i.strip().split("\t")
		if int(spls[4]) < 3:
			tags.add(spls[3])
	rtab.close()
	
	print "qual"
	qual = open(sys.argv[2],"r")
	curid = ""
	qualmap = {}
	record = False
	for i in qual:
		if i[0] == "#":
			continue
		if i[0] == ">":
			curid = i.strip()
			if curid in tags:
				record = True
			else:
				record = False
		else:
			if record == True:
				spls = i.strip().split(" ")
				nums = []
				for j in spls:
					intj = int(j)
					nums.append(intj)
				qualmap[curid] = nums
	qual.close()
	
	print "csfile"
	csfasta = open(sys.argv[3],"r")
	curid = ""
	finaltagset = {} #key = tagcolorseq , value = count
	record = False
	for i in csfasta:
		if i[0] == "#":
			continue
		if i[0] == ">":
			curid = i.strip()
			if curid in qualmap:
				record = True
			else:
				record = False
		else:
			if record == True:
				line = i.strip()
				if line not in finaltagset:
					finaltagset[line] = 0
				finaltagset[line] += 1
	csfasta.close()

	outfile = open(sys.argv[4],"w")
	for i in finaltagset:
		outfile.write(str(i)+"\t"+str(finaltagset[i])+"\n")
	outfile.close()
