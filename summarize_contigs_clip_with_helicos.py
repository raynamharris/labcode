import sys
from Bio import SeqIO

"""
This should summarize the helicos alignments.txt file using isotigs (with contigs taken from fixed -clipped- 454AllContigs.clipped.fna). 

This also assumes that the cap3 contigs keep their capital C in the contig name.

This will assume that the input 454AllContigs.clipped.fna is the CORRECT clipping one

MAKE SURE MULTIPLE COUNTS ARE TAKEN INTO ACCOUNT< SO NO FRACTIONS

"""

MAX_MISMATCH = 2

def parse_cont_graph_file(filename):
	isograph = {}
	isodir = {}
	isolengths = {}
	contlength = {}
	firstC = False
	for i in contgra:
		if i[0] != "C" and firstC == False:
			spls = i.strip().split("\t")
			contlength[int(spls[0])] = float(spls[2])
		elif i[0] == "C":
			firstC = True
		elif i[0] == "S":
			spls = i.strip().split("\t")
			isograph[int(spls[1])] = []
			isodir[int(spls[1])] = []
			isolengths[int(spls[1])] = int(spls[2])
			spls3 = spls[3].split(";")
			for j in spls3:
				isograph[int(spls[1])].append(int(j.split(":")[0]))
				isodir[int(spls[1])].append(j.split(":")[1])
	return [isograph,isodir,isolengths,contlength]

"""
should add the zeros to the isotig and isogroup labels
"""
def add_zeros(num):
	length = len(str(num))
	addz = 5-length
	return (addz*'0')+str(num)


def get_num_times_found_in_isotigs(isograph,contig):
	num = 0
	for i in isograph:
		if contig in isograph[i]:
			num += 1
	if num == 0:
		num = 1
	return num

if __name__ == "__main__":
	if len(sys.argv) != 6:
		print "usage: python summarize_contigs_clip_with_helicos.py 454ContigGraph.txt 454AllContigs.clipped.fna cap3.contigs alignments.txt outfile"
		sys.exit(0)
	
	#open 454ContigGraph.txt
	contgra = open(sys.argv[1],"rU")
	isograph,isodir,isolengths,contlength = parse_cont_graph_file(contgra)
	contgra.close()

	#testing lengths
	#for i in isograph:
		#graph = isograph[i]
		#length = isolengths[i]
		#clength = 0
		#for j in graph:
			#clength += contlength[j]
		#if length - clength > 0:
			#print length - clength
	#sys.exit(0)
	
	#open the 454AllContigs.clipped.fna
	infile = open(sys.argv[2],"rU")
	contigs_lens = {}
	for i in SeqIO.parse(infile,"fasta"):
		contigs_lens[i.id] = len(i.seq)
	infile.close()

	#open the cap3.contigs
	infile = open(sys.argv[3],"rU")
	cap3lens = {}
	for i in SeqIO.parse(infile,"fasta"):
		cap3lens[i.id] = len(i.seq)
	infile.close()

	dict = {} #this is to keep the data in order to work with later
	dictrev = {} #this is the reverse
	
	infile = open(sys.argv[4],"rU")
	count = 0
	first = True
	for i in infile:
		if i[0] == '#' or "28S" in i or "Oligo" in i or "18S" in i:
			continue
		else:
			if first == True:
				first = False
			else:
				spls =  i.strip().split("\t")
				contig = ""
				if spls[0][0] == "i": #isotig
					contig = spls[0].split(" ")[0]
				else:
					contig = spls[0] #Contigs from cap3
				startv = int(spls[2])-1
				endv = int(spls[3])
				lenv = endv-startv
				strand = spls[11]
				if lenv < 0:
					print "error in length, turns out there are reverses"
					sys.exit(0)
				mismatch = lenv -int(spls[7])
				
				#print contig,startv,endv,lenv,mismatch,strand
				if mismatch <= MAX_MISMATCH:
					if contig not in dict:
						dict[contig] = {}
					if contig not in dictrev:
						dictrev[contig] = {}
					if strand == "+":
						for i in range(lenv):
							if (abs(startv)+i) not in dict[contig]:
								dict[contig][abs(startv)+i] = 0
							dict[contig][abs(startv)+i] += 1
					else:
						for i in range(lenv):
							if (abs(startv)+i) not in dictrev[contig]:
								dictrev[contig][abs(startv)+i] = 0
							dictrev[contig][abs(startv)+i] += 1
					count += 1
					if count % 1000000 == 0:
						print count
#					if count == 100:
#						break
	infile.close()
	
	newdict = {}
	newdictrev = {}
	
	#don't want to count multiple tags for a contig twice
	tagscounted = {}
	
	outfile = open(sys.argv[5],"w")
	outfile.write("contig\tfd\trd\n")
	for i in dict:
		start = 0
		end = 0
		if i[0] == "C": #cap3 contigs
			end = cap3lens[i]
			for j in range(end):
				d = 0
				dr = 0
				if j in dict[i]:
					d = dict[i][j]
				if j in dictrev[i]:
					dr = dictrev[i][j]
				outfile.write(str(i)+"\t"+str(d)+"\t"+str(dr)+"\n")
		else:#need to do things to get the contigs from the isotigs
			# labeled isotig####, splitting by isogroup
			isonumsf = dict[i]
			fkeys = sorted(isonumsf.keys())
			isonumsr = dictrev[i]
			rkeys = sorted(isonumsr.keys())
			graphnum = None
			graph = None
			dir = None
			if "isotig" in i:
				graphnum = int(i.split("isotig")[1])
				graph = isograph[graphnum]
				dir = isodir[graphnum]
			elif "contig" in i:
				graphnum = int(i.split("contig")[1].split(" ")[0])
				graph = [graphnum]
				dir = ["+"] #pretty sure these are all positive if only one member
			
			#print i,graph,dir,dict[i],dictrev[i]
			cur = 0
			prestart = 0
			for k in fkeys:
				try:
					while k > (prestart+contigs_lens["contig"+add_zeros(graph[cur])]):
						prestart += contigs_lens["contig"+add_zeros(graph[cur])]
						cur += 1
				except:
					print cur,graph,i,k,prestart
					for g in graph:
						print contigs_lens["contig"+add_zeros(g)]
					continue
				if ("contig"+add_zeros(graph[cur])) not in newdict:
					newdict["contig"+add_zeros(graph[cur])] = {}
				if ("contig"+add_zeros(graph[cur])) not in newdictrev:
					newdictrev["contig"+add_zeros(graph[cur])] = {}
				if (k-prestart) not in newdict["contig"+add_zeros(graph[cur])]:
					newdict["contig"+add_zeros(graph[cur])][k-prestart] = 0
				newdict["contig"+add_zeros(graph[cur])][k-prestart] += isonumsf[k] #+= 1
			cur = 0
			prestart = 0
			for k in rkeys:
				try:
					while k > (prestart+contigs_lens["contig"+add_zeros(graph[cur])]):
						prestart += contigs_lens["contig"+add_zeros(graph[cur])]
						cur += 1
				except:
					print cur,graph,i,k,prestart
					for g in graph:
						print contigs_lens["contig"+add_zeros(g)]
					continue
				if ("contig"+add_zeros(graph[cur])) not in newdict:
					newdict["contig"+add_zeros(graph[cur])] = {}
				if ("contig"+add_zeros(graph[cur])) not in newdictrev:
					newdictrev["contig"+add_zeros(graph[cur])] = {}
				if (k-prestart) not in newdictrev["contig"+add_zeros(graph[cur])]:
					newdictrev["contig"+add_zeros(graph[cur])][k-prestart] = 0
				newdictrev["contig"+add_zeros(graph[cur])][k-prestart] += isonumsr[k] #+= 1
	for k in newdict:
		end = contigs_lens[k]
		numfound = get_num_times_found_in_isotigs(isograph,k)
		for j in range(end):
			d = 0
			dr = 0
			if j in newdict[k]:
				try:
					d = int(newdict[k][j]/float(numfound))
				except:
					print k,newdict[k][j],numfound
					sys.exit(0)
			if j in newdictrev[k]:
				try:
					dr = int(newdictrev[k][j]/float(numfound))
				except:
					print k,newdictrev[k][j],float(numfound)
					sys.exit(0)
			outfile.write(str(k)+"\t"+str(d)+"\t"+str(dr)+"\n")
				

		
	outfile.close()
	
