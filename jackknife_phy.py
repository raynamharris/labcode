import sys,os
import random
"""
in order to conduct the phylobayes analyses we have to shorten the matrices and this is one way that we can do this


"""

modprop = 0.2

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python jackknife_phy.py infile.phy infile.model outdir"
		sys.exit(0)
	infile= open(sys.argv[1],"r")
	inmod = open(sys.argv[2],"r")
	outfile = open(sys.argv[3],"w")
	outfilemod = open(sys.argv[3]+".model","w")

	seqs = {} #key is name and value is seq
	finalseqs = {}
	first = True
	for i in infile:
		if first ==True:
			first = False
			continue
		i = i.strip()
		if len(i) > 1:
			spls = i.split()
			seqs[spls[0]] = spls[1]
			finalseqs[spls[0]] = ""
	
	models = {} #key is name and value is list [begin,end]
	for i in inmod:
		i = i.strip()
		if len(i) > 1:
			spls = i.split("=")
			models[spls[0]] = [spls[1].split("-")[0],spls[1].split("-")[1]]

	numgenes = len(models)
	numfin = int(numgenes * modprop)
	samp = random.sample(models.keys(),numfin)
	totallen = 0
	for i in samp:
		outfilemod.write(i+"="+models[i][0]+"-"+models[i][1]+"\n")
		totallen += int(models[i][1])-int(models[i][0])+1
	outfile.write(str(len(seqs))+" "+str(totallen)+"\n")
	for i in samp:
		for j in finalseqs:
			finalseqs[j] += seqs[j][int(models[i][0])-1:int(models[i][1])]
	for i in finalseqs:
		outfile.write(i+" "+finalseqs[i]+"\n")
