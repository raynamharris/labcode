#!/usr/bin/env python

import os
import sys
from glob import glob
from Bio import AlignIO

Usage = """
Compares two supermatrices generated in agalma to find number of
overlapping genes.

Usage:
   python compare_matrices.py clean_alignments_dir_matrix1 partition_file_matrix1 clean_alignments_dir_matrix2 partition_file_matrix2
"""

if len(sys.argv) < 4:
	print Usage
else:

	align1_dir = sys.argv[1]
	part1_file = sys.argv[2]
	align2_dir = sys.argv[3]
	part2_file = sys.argv[4]

	target_clusters = {}
	with open(part1_file, "r") as f1:
		for line in f1:
			line = line.strip('\n')
			elements = line.split(' ')
			target_clusters[elements[1]] = []

	source_clusters = []
	with open(part2_file, "r") as f2:
		for line in f2:
			line = line.strip('\n')
			elements = line.split(' ')
			source_clusters.append(elements[1])

	target_dict = {}
	for fasta in glob(os.path.join(align1_dir, '*.fa')):
		cluster_name = os.path.splitext(os.path.basename(fasta))[0]
		if cluster_name in target_clusters:
			msa = AlignIO.read(fasta, "fasta")
			for record in msa:
				seq_name = record.id.partition('@')[-1]
				target_dict[seq_name] = cluster_name
				target_clusters[cluster_name].append(seq_name)

	matches = 0
	found = set()
	for fasta in glob(os.path.join(align2_dir, '*.fa')):
		cluster_name = os.path.splitext(os.path.basename(fasta))[0]
		if cluster_name in source_clusters:
			msa = AlignIO.read(fasta, "fasta")
			for record in msa:
				seq_name = record.id.partition('@')[-1]
				if seq_name in target_dict and not seq_name in found:
					cluster = target_dict[seq_name]
					# add to found all the sequence ids in cluster
					map(found.add, target_clusters[cluster])
					matches += 1
					break

	print "number of overlapping genes: %d" % matches

