# Labcode

This is a set of many different programs that we use for a variety of tasks in the [Dunn Lab](http://dunnlab.org). They are variously buggy, incomplete, half done, finished, optimized, inefficient, poorly written for a once-off task, and beautifully implemented.

USE AT YOUR OWN RISK.


