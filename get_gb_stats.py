from Bio import SeqIO
import sys,os

"""
this is just for getting the stats, number of seqs and lengths, for gblock processed
aligned files

it can easily be used for the other files just by changing the file ending
"""

file_ending = ".aln-gb"

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python get_gb_stats.py DIR outfile"
		sys.exit(0)
	path = sys.argv[1]
	outfile = sys.argv[2]
	out = open(outfile,"w")
	for i in os.listdir(path):
		if i[-len(file_ending):] == file_ending:
			handle = open(path+"/"+i,"rU")
			number = 0
			length = 0
			for j in SeqIO.parse(handle,"fasta"):
				length = len(j.seq)
				number += 1
			handle.close()
			out.write(str(number)+"\t"+str(length)+"\n")
	out.close()
