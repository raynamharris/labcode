import sys
from svg_lib import *
from Bio import SeqIO

"""
creates and SVG of the technologies and the ACE for the contig

if using the GC content then takes the 454AllContigs.fna.clipped file as well.

"""

def average(nums):
	avg = 0
	for i in nums:
		avg += i/len(nums)
	return avg


SLIDING_WINDOW = 31

if __name__ == "__main__":
	gc = False
	if len(sys.argv) == 5:
		print "(not using gc)"
		print "(optional for gc content)"
		print "(usage: python convert_combined_to_svg_contig.py contig infile_3types infile_ace [454AllContigs.fna.clipped] outfile)"
	elif len(sys.argv) == 6:
		gc = True
	else:
		print "usage: python convert_combined_to_svg_contig.py contig infile_3types infile_ace outfile"
		print ""
		print "optional for gc content"
		print "usage: python convert_combined_to_svg_contig.py contig infile_3types infile_ace [454AllContigs.fna.clipped] outfile"
		sys.exit(0)

	contig = sys.argv[1]
	
	infile = open(sys.argv[2],"r")
	data = []
	lc = len(contig)
	MAX_Y = 0
	for i in infile:
		if i[:lc] == contig:
			spls = i.strip().split("\t")
			data.append(spls[1:])
			for j in spls[1:]:
				if int(j) > MAX_Y:
					MAX_Y = int(j)
			
	infile.close()
	
	XSCALE = 1
	YSCALE = 1 #lower this to something like 0.1 or 0.01 if too large to read in file
	XOFFSET = 5
	YOFFSET = 5
	
	MAX_Y *= 2
	
	ld = len(data)
	
	MAX_Y_ACE = 0
	ace_list = None
	infile_ace = open(sys.argv[3],"r")
	for i in infile_ace:
		if i[:lc] == contig:
			spls = i.strip().split(" ")
			ace_list = spls[1:]
			for j in spls[1:]:
				if int(j) > MAX_Y_ACE:
					MAX_Y_ACE = int(j)

	infile_ace.close()
	if ace_list == None:
		print "contig not found in the ace summary file"
	
	sliding = []
	
	outarg = 4
	if gc == False:
		outarg = 4
	else:
		tsliding = []
		infile_gc = open(sys.argv[4],"r")
		for i in SeqIO.parse(infile_gc,"fasta"):
			if i.id == contig:
				for curcount in range(len(i.seq)):
					if i.seq.tostring()[curcount].upper() == 'G' or i.seq.tostring()[curcount].upper() == 'C':
						tsliding.append(1.)
					else:
						tsliding.append(0.)
		count = 0
		sliding = tsliding
		for i in range(len(tsliding)):
			avg = average(tsliding[int(max(i-(SLIDING_WINDOW/2.),0)):int(min(i+(SLIDING_WINDOW/2.),len(tsliding)))])
			sliding[count] = avg
			count += 1
		outarg = 5

	scene = Scene(sys.argv[outarg],(((YOFFSET+MAX_Y)*5)-(MAX_Y/2)+MAX_Y_ACE)*YSCALE,((2*XOFFSET)+(len(data)))*XSCALE)
	#setup the space
	#he red
	scene.add(CLine(((0+XOFFSET)*XSCALE,((YOFFSET+MAX_Y)*1)*YSCALE),(((ld)+XOFFSET)*XSCALE,((YOFFSET+MAX_Y)*1)*YSCALE), "red"))
	#il green
	scene.add(CLine(((0+XOFFSET)*XSCALE,((YOFFSET+MAX_Y)*2)*YSCALE),(((ld)+XOFFSET)*XSCALE,((YOFFSET+MAX_Y)*2)*YSCALE), "green"))
	#so blue
	scene.add(CLine(((0+XOFFSET)*XSCALE,((YOFFSET+MAX_Y)*3)*YSCALE),(((ld)+XOFFSET)*XSCALE,((YOFFSET+MAX_Y)*3)*YSCALE), "blue"))
	#ace file
	scene.add(CLine(((0+XOFFSET)*XSCALE,(((YOFFSET+MAX_Y)*4)+MAX_Y_ACE)*YSCALE),(((ld)+XOFFSET)*XSCALE,(((YOFFSET+MAX_Y)*4)+MAX_Y_ACE)*YSCALE), "black"))
	count = 0
	for i in data:
		#so
		scene.add(CLine(((count + XOFFSET)*XSCALE,((YOFFSET+MAX_Y)*3)*YSCALE), ((count + XOFFSET)*XSCALE,(((YOFFSET+MAX_Y)*3)-int(i[5]))*YSCALE), "lightblue"))
		scene.add(CLine(((count + XOFFSET)*XSCALE,((YOFFSET+MAX_Y)*3)*YSCALE), ((count + XOFFSET)*XSCALE,(int(i[4])+((YOFFSET+MAX_Y)*3))*YSCALE), "darkblue"))
		##il
		scene.add(CLine(((count + XOFFSET)*XSCALE,((YOFFSET+MAX_Y)*2)*YSCALE), ((count + XOFFSET)*XSCALE,(((YOFFSET+MAX_Y)*2)-int(i[3]))*YSCALE), "lightgreen"))
		scene.add(CLine(((count + XOFFSET)*XSCALE,((YOFFSET+MAX_Y)*2)*YSCALE), ((count + XOFFSET)*XSCALE,(int(i[2])+((YOFFSET+MAX_Y)*2))*YSCALE), "darkgreen"))
		##he
		scene.add(CLine(((count + XOFFSET)*XSCALE,((YOFFSET+MAX_Y)*1)*YSCALE), ((count + XOFFSET)*XSCALE,(((YOFFSET+MAX_Y)*1)-int(i[1]))*YSCALE), "pink"))
		scene.add(CLine(((count + XOFFSET)*XSCALE,((YOFFSET+MAX_Y)*1)*YSCALE), ((count + XOFFSET)*XSCALE,(int(i[0])+((YOFFSET+MAX_Y)*1))*YSCALE), "darkred"))
		
		#ace
		try:
			scene.add(CLine(((count + XOFFSET)*XSCALE,(((YOFFSET+MAX_Y)*4)+MAX_Y_ACE)*YSCALE), ((count + XOFFSET)*XSCALE,((((YOFFSET+MAX_Y)*4)+MAX_Y_ACE)-int(ace_list[count]))*YSCALE), "black"))
			if gc == True:
				num  = 255-int(((100.*float(sliding[count]))/60.)*255)
				scene.add(CLine(((count + XOFFSET)*XSCALE,(((YOFFSET+MAX_Y)*4)+MAX_Y_ACE)*YSCALE), ((count + XOFFSET)*XSCALE,((((YOFFSET+MAX_Y)*4)+MAX_Y_ACE)+2)*YSCALE), "rgb("+str(num)+","+str(num)+","+str(num)+")"))

		except:
			print "length issue"
		#gc content
		count += 1
		
	scene.write_svg()
	#scene.display()
	
