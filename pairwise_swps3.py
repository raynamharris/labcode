import sys,os
from Bio import SeqIO
import pp

matrixfile = "/home/smitty/lib/matrices/blosum62.mat" 

num_jobs = 14
MIN_SCORE = 100

def calc_swps3(index,seqs,dbfile,matrixfile,minscore):
	outfile = open("pp."+str(index)+".temp","w")
	count = 0
	for i in seqs:
		tempfile = open("temp"+str(index)+".fasta","w")
		tempfile.write(">"+i.id+"\n"+str(i.seq)+"\n")
		tempfile.close()
		cmd = "swps3 "+matrixfile+" temp"+str(index)+".fasta "+dbfile+" > temp"+str(index)+".out"
		os.system(cmd)
		tempout = open("temp"+str(index)+".out","r")
		for j in tempout:
			spls = j.strip().split("\t")
			if i.id != spls[1] and int(spls[0]) > minscore:
				outfile.write(i.id+"\t"+spls[1]+"\t"+spls[0]+"\n")
		tempout.close()
		count += 1
	outfile.close()
	print "done",index
	return "pp."+str(index)+".temp"

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python pairwise_swps3.py infile outfile"
		sys.exit(0)
	infile = open(sys.argv[1],"r")
	infilen = sys.argv[1]
	seqs = list(SeqIO.parse(infile,"fasta"))
	ppservers = ()
	job_server = pp.Server(num_jobs,ppservers=ppservers)
	print "Starting pp with ",job_server.get_ncpus(), " workers"
	outfile = open(sys.argv[2],"w")
	start = 0
	jobs = []
	for index in range(num_jobs):
		print "starting worker "+str(index)
		if index == (num_jobs-1):
			jobs.append(job_server.submit(calc_swps3,(index,seqs[start:len(seqs)+1],infilen,matrixfile,MIN_SCORE),(),("Bio.SeqIO",)))
			print start,len(seqs)+1
		else:
			jobs.append(job_server.submit(calc_swps3,(index,seqs[start:start+(len(seqs)/int(num_jobs))],infilen,matrixfile,MIN_SCORE),(),("Bio.SeqIO",)))
			print start,start+(len(seqs)/int(num_jobs))
			start += (len(seqs)/int(num_jobs))

	for job in jobs:
		namef = job()
		print "combining " + str(namef)
		toufile = open(namef,"r")
		for i in toufile:
			outfile.write(i)
		toufile.close()
	outfile.close()
	infile.close()
