#!/usr/bin/env python	
import sys
from optparse import OptionParser
import random
import re
from Bio import SeqIO
from collections import defaultdict

Usage = """
Usage:

njal.py is a set of tools for iterative normalization and assembly of sequence reads

"""

# Some global stuff
verbose = 0
re_locus = re.compile(r'Locus_(\d+)')

# oases header example:
# >Locus_5_Transcript_2/5_Confidence_0.625_Length_664

# eland mapping example:
# >HWI-ST625_0051:8:1101:10011:2444#CGATGT/2      CGACGTTTACTTTGAAAAAATTAGAGTGTTCAAAG     1:1:0   bargm_ribo_seq.fasta/gi|60545000|gb|AY937334.1|:725F35,bargm_ribo_seq.fasta/gi|60544999|gb|AY937333.1|:726FT34
# >HWI-ST625_0051:8:1101:4918:1992#CGATGT/2	CACTTTCCACGTCAAAAAACATTTTACAAGCGTCA	NM	-


def convert_map_to_counts(map):
	# Takes a dictionary with key read name and value locus, and generates a dictionary with key
	# locus and value read count
	
	counts = {}
	
	for read, locus in map.iteritems():
		counts[locus] = counts.get(locus, 0) + 1

	return counts
	
def get_locus(header):
	# Takes a fasta header, returns the corresponding locus
	locus_result = re_locus.match(header)
	locus = None
	if locus_result:
		locus = int(locus_result.group(1))
	return locus



if __name__ == "__main__":
	parser = OptionParser()
	parser.add_option("-e", "--eland", action="store", type="string", dest="eland_name", help="Path to the eland mapping file, eg reanalysis_TTAGGC_L007_R1_001_eland_extended.txt")
	parser.add_option("-r", "--reference", action="store", type="string", dest="ref_name", help="Path to the fasta reference that was used for mapping, this is assumed to by transcripts.fa from an oases assembly")
	parser.add_option("-i", "--input-file", action="store", type="string", dest="input_name", help="Path to the reads that were mapped, in fastq format")
	parser.add_option("-o", "--output-file", action="store", type="string", dest="output_name", help="Path to write the the normalized fastq file to")
	parser.add_option("-c", "--count-file", action="store", type="string", dest="count_name", help="Path for text file to write the expression data to")
	parser.add_option("-v", action="store_true", dest="verbose", default=False, help="Set this flag for verbose output")
	parser.add_option("-d", action="store", type="int", dest="depth", default=500, help="Maximum sequencing depth; the ligrary is normalized for all transcripts to have coverage less than or equal to this")
	parser.add_option("-l", action="store", type="int", dest="read_length", default=100, help="The actual read lengths, used to calculate coverage")
	(options, args) = parser.parse_args()
	
	if options.verbose:
		verbose = 1
	
	
	
	print "Reading eland file " + options.eland_name
	# Parse eland file resulting from mapping to an oases assembly, 
	# returns a dictionary with key read and value locus
	
	# Unmapped reads are not in dictionary. Reads that map to more than one locus are not
	# in dictionary.
	
	# Allows up to one mismatch.
	
	eland_file = open(options.eland_name, "r")
	
	map = {}
	
	p = re.compile(r'(.+)/(.+):')
	
	
	
	total = 0 # The total number of reads read
	nm = 0 # The number of reads that don't map at all
	printfreq = 1000000
	
	for line in eland_file:
		total = total + 1
		if verbose:
			if total % printfreq == 0:
				print " line " + str(i)
		line = line.rstrip()
		fields = line.split("\t")
		if fields[3] == '-' :
			nm = nm + 1
		else:
			transcripts = fields[3].split(',')
			loci = set()
			for t in transcripts:
				if t.find("/") < 0:
					continue # skip double location records for one transcript, eg: transcripts.fa/Locus_1123_Transcript_2__2_Confidence_1.000_Length_409:3F35,182F35
				m = p.match(t)
				if m:
					file = m.group(1)
					seqname = m.group(2)
				else:
					sys.exit("ERROR: The target names in the following line are poorly formed:\n" + line + "\nBad target:\n" + t + "\n")
				locus = get_locus(seqname)
				if locus:
					loci.add(locus)
				else:
					sys.exit("ERROR: A Locus name in the following line is poorly formed:\n" + line + "\n")
			if len(loci) == 1:
				map[fields[0]] = list(loci)[0]
				
	
	print " " + str(total) + " reads read, " + str(len(map.keys())) + " mapped uniquely, " + str(nm) + " didn't map"
	
	print "Calculating counts for each reference locus..."
	counts = convert_map_to_counts(map)
	
	
	print "Reading reference file " + options.ref_name
	reference = defaultdict(list) # Dictionary with key locus name and value list of sequences belonging to that locus
	i = 0
	for record in SeqIO.parse(options.ref_name, "fasta"):
		locus = get_locus(record.name)
		reference[locus].append(record)
		i = i + 1
	
	print "  " + str(i) + " sequences read, belonging to " + str(len(reference.keys())) + " loci"
	
	lengths = dict()
	coverage = dict()
	print "Claculating reference length statistics..."
	for loc, seqs in reference.iteritems():
		longest = 0
		for seq in seqs:
			if len(seq.seq) > longest:
				longest = len(seq.seq)
		lengths[loc] = longest
		coverage[loc] = counts.get(loc, 0) * options.read_length / float(lengths[loc])
	
	
	
	if  options.count_name:
		count_file = open(options.count_name, 'w')
		count_file.write("locus\tlength\tcount\tcoverage\n")
		for locus in lengths.keys():
			count_file.write(str(locus) + "\t" + str(lengths.get(locus,-1)) + "\t" + str(counts.get(locus,0)) + "\t" + str(coverage.get(locus, 0)) + "\n")
	
	