import sys
import numpy
import math
from Bio import SeqIO
from scipy.stats.stats import *

VERBOSE = 0

"""
this should allow for processing of the ContigGraph and the IsotigsLayout to determine
the best path for the isogroup
"""

def custweightedgeomean(pilist,contiglenlist):
	return math.exp(sum(pilist)/sum(contiglenlist))

def parse_cont_graph_file(file):
	contcondepth = {}
	contdepth = {}
	contlength = {}
	isograph = {}
	firstC = False
	for i in contgra:
		if i[0] != "C" and firstC == False:
			spls = i.strip().split("\t")
			contdepth[int(spls[0])] = float(spls[3])
			contlength[int(spls[0])] = float(spls[2])
		elif i[0] == "C":
			firstC = True
			spls = i.strip().split("\t")
			if int(spls[1]) not in contcondepth:
				contcondepth[int(spls[1])] = {}
			if int(spls[3]) not in contcondepth:
				contcondepth[int(spls[3])] = {}
			contcondepth[int(spls[1])][int(spls[3])] = float(spls[5])
			contcondepth[int(spls[3])][int(spls[1])] = float(spls[5])
		elif i[0] == "S":
			spls = i.strip().split("\t")
			isograph[int(spls[1])] = []
			spls3 = spls[3].split(";")
			for j in spls3:
				isograph[int(spls[1])].append(int(j.split(":")[0]))
	return [contcondepth,contdepth,contlength,isograph]

"""
The IsotigsLayout file is just used to identify which isotigs are a part of which isogroups.
The actual layout is parsed from the S line in the ContigGraph file
"""
def parse_iso_layout_file_and_calc_stats(file,isograph):
	begin = False
	curisogroup = None
	contignums = []
	count = 0
	isogrouppathsum = {}
	isogrouppathmean = {}
	isogroupathgeomean = {}
	isogroupcontigsum = {}
	isogroupcontigmean = {}
	isogroupcontiggeomean = {}
	isogroupisotigpaths = {}
	for i in isolay:
		if len(i.strip()) == 0:
			begin = False
			contignums = []
		if i[0] == ">":
			begin = True
			curisogroup = i.strip().split(" ")
			curisogroup = int(i.strip().split(" ")[0].split(">isogroup")[1])
			isogrouppathsum[curisogroup] = {}
			isogrouppathmean[curisogroup] = {}
			isogroupathgeomean[curisogroup] = {}
			isogroupcontigsum[curisogroup] = {}
			isogroupcontigmean[curisogroup] = {}
			isogroupcontiggeomean[curisogroup] = {}
			isogroupisotigpaths[curisogroup] = {}
			#print "curisogroup: ",curisogroup
		elif begin == True:
			if "Contig" in i.strip():
				tempconts = i.strip().split(" ")[2:-1]
				#print tempconts
				for j in tempconts:
					#print int(j)
					contignums.append(int(j))
			if "isotig" in i.strip():
				spls = i.strip().split(" ")
				curisotigname =  spls[0]
				curisotig = int(spls[0].split("isotig")[1])
				path = isograph[curisotig]
				if len(path) > 1:
					statres  = calc_stats_on_path(path,contcondepth,contdepth,contlength)
					isogrouppathsum[curisogroup][curisotig]=statres[0]
					isogrouppathmean[curisogroup][curisotig]=statres[1]
					isogroupathgeomean[curisogroup][curisotig]=statres[2]
					isogroupcontigsum[curisogroup][curisotig]=statres[3]
					isogroupcontigmean[curisogroup][curisotig]=statres[4]
					isogroupcontiggeomean[curisogroup][curisotig]=statres[5]					
					isogroupisotigpaths[curisogroup][curisotig] = path
					if VERBOSE > 0:
						print curisogroup,curisotig,"path:",path
						print curisogroup,curisotig,"psum:",isogrouppathsum[curisogroup][curisotig],"pmean:",isogrouppathmean[curisogroup][curisotig],"pgeomean:",isogroupathgeomean[curisogroup][curisotig]
						print curisogroup,curisotig,"csum:",isogroupcontigsum[curisogroup][curisotig],"cmean:",isogroupcontigmean[curisogroup][curisotig],"cgeomean:",isogroupcontiggeomean[curisogroup][curisotig]
						print "---"
				else:#remove the isogroups with only one isotig with only one contig
					isogrouppathsum[curisogroup][curisotig] = 0
					isogrouppathmean[curisogroup][curisotig] = 0
					isogroupathgeomean[curisogroup][curisotig] = 0
					isogroupcontigsum[curisogroup][curisotig] = 0
					isogroupcontigmean[curisogroup][curisotig] = 0
					isogroupcontiggeomean[curisogroup][curisotig] = 0
					isogroupisotigpaths[curisogroup][curisotig] = 0
#					del isogrouppathsum[curisogroup]
#					del isogrouppathmean[curisogroup]
#					del isogroupathgeomean[curisogroup]
#					del isogroupcontigsum[curisogroup]
#					del isogroupcontigmean[curisogroup]
#					del isogroupcontiggeomean[curisogroup]
#					del isogroupisotigpaths[curisogroup]
		count += 1
		if count == 100000000:
			sys.exit(0)
	return [isogrouppathsum,isogrouppathmean,isogroupathgeomean,isogroupcontigsum,isogroupcontigmean,isogroupcontiggeomean,isogroupisotigpaths]

"""
path should be a list of the contigs
"""
def calc_stats_on_path(path,contcondepth,contdepth,contlength):
	meanlist = []
	meancontiglist = []
	meanweightcontiglist = []
	contiglenlist = []
	contigdeplist = []
	for j in range(len(path)):
		if j < len(path)-1:
			meanlist.append(contcondepth[path[j]][path[j+1]])
		meancontiglist.append(contdepth[path[j]] * contlength[path[j]])
		meanweightcontiglist.append(math.log(contdepth[path[j]]) * contlength[path[j]])
		contigdeplist.append(contdepth[path[j]])
		contiglenlist.append(contlength[path[j]])
	psm = sum(meanlist)
	pmn = numpy.mean(meanlist)
	pgmn = gmean(meanlist)
	ctsm = sum(meancontiglist)
	ctmn = sum(meancontiglist)/sum(contiglenlist)
	ctgmn = custweightedgeomean(meanweightcontiglist,contiglenlist)#gmean(meancontiglist)
	#print meanlist,contigdeplist
	#pathsum pathmean pathgeomean contigsum contigmean contiggeomean
	return [psm,pmn,pgmn,ctsm,ctmn,ctgmn]

"""
should take the contig storage (the connections from the C line) and the starting contig
can eventually take the contig depth and length
"""
def pick_best_connection(starting_contig,contcondepth):
	path = [starting_contig]
	going = True
	count = 0
	while going:
		possibles = contcondepth[path[-1]]
#		print "start:",path[-1],"possibles:",possibles
		bestscore = 0
		best = None
		counts = 0
		for i in possibles:
#			print i,contstorage[path[-1]][i]
			if len(path) > 1 and i in path:
				continue
			else:
				counts += 1
			if contcondepth[path[-1]][i] > bestscore:
				bestscore = contcondepth[path[-1]][i]
				best = i
		if counts > 0:
			path.append(best)
		else:
			going = False
		count += 1
		if count == 20:
			break
	#print path
	return path

"""
should add the zeros to the isotig and isogroup labels
"""
def add_zeros(num):
	length = len(str(num))
	addz = 5-length
	return (addz*'0')+str(num)



if __name__ == "__main__":
	if len(sys.argv) != 6:
		print "usage: python exemplar.py 454ContigGraph.txt 454IsotigsLayout.txt 454Isotigs.fna 454Isotigs.qual outfile"
		sys.exit(0)
	
	#open the 454ContigGraph
	contgra = open(sys.argv[1],"rU")
	contcondepth,contdepth,contlength,isograph = parse_cont_graph_file(contgra)
	contgra.close()

	#open the 454IsotigsLayout
	isolay = open(sys.argv[2],"rU")
	statres = parse_iso_layout_file_and_calc_stats(isolay,isograph)
	isogrouppathsum = statres[0]
	isogrouppathmean = statres[1]
	isogrouppathgeomean = statres[2]
	isogroupcontigsum = statres[3]
	isogroupcontigmean = statres[4]
	isogroupcontiggeomean = statres[5]
	isogroupisotigpaths = statres[6]
	isolay.close()

	#read the 454Isotigs fasta file
	isofna = open(sys.argv[3],"rU")
	isofnas = {}
	isofnas_seqs = {}
	for i in SeqIO.parse(isofna,"fasta"):
		if "isotig" in i.id:
			sid = int(i.id[-5:])
			seq = i.seq.tostring()
			isofnas[sid] = seq
			isofnas_seqs[sid] = i
	isofna.close()
	
	#read the 454Isotigs qual file
	isoqlt = open(sys.argv[4],"rU")
	isoqlts = {}
	for i in SeqIO.parse(isoqlt,"qual"):
		if "isotig" in i.id:
			sid = int(i.id[-5:])
			isoqlts[sid] = i
	isoqlt.close()

	#present the best isotigs for each isogroup based on a stat
	print "====="
	print "calculating best isotig by geometric mean of contig depths"
	print "====="
	best_seqs = []
	best_quals = []
	for i in isogrouppathgeomean:
		best = -1
		bestiso = None
		for j in isogrouppathgeomean[i]:
			if isogrouppathgeomean[i][j] > best:
				best = isogrouppathgeomean[i][j]
				bestiso = j
		stri = "isogroup: "+str(i)+" isotig: "+str(bestiso)+" score:"
		print stri,str(best),"path:",isogroupisotigpaths[i][bestiso]
		#outfile.write(">isotig"+add_zeros(bestiso)+"  gene=isogroup"+add_zeros(i)+"\n"+isofnas[bestiso]+"\n")
		best_seqs.append(isofnas_seqs[bestiso])
		best_quals.append(isoqlts[bestiso])
		#mpth = pick_best_connection(isogroupisotigpaths[i][bestiso][0],contcondepth)
		#print (" "*(len(stri)-6)),"mine:",calc_stats_on_path(mpth,contcondepth,contdepth,contlength)[5],"path:",mpth
	
	
	#write fastas
	outfile = open(sys.argv[5],"w")
	SeqIO.write(best_seqs, outfile, "fasta")
	outfile.close()
	
	#write quals
	outfile_qlt = open(sys.argv[5]+".qlt","w")
	SeqIO.write(best_quals, outfile_qlt, "qual")
	outfile_qlt.close()
