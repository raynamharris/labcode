from Bio import SeqIO
import sys

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "usage: python remove_short_sequences.py in.fasta out.fasta min_length"
		sys.exit(0)
	
	inhandle = open(sys.argv[1],"r")
	outhandle = open(sys.argv[2],"w")
	min_length = int(sys.argv[3])
	
	for record in SeqIO.parse(inhandle,"fasta"):
		if len(record.seq) >= min_length:
			SeqIO.write(record, outhandle, "fasta")
	inhandle.close()
	outhandle.close()
