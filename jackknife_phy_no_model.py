import sys,os
import random
"""
in order to conduct the phylobayes analyses we have to shorten the matrices and this is one way that we can do this


"""


if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python jackknife_phy_no_model.py infile.phy outfile proportion"
		sys.exit(0)
	infile= open(sys.argv[1],"r")
	outfile = open(sys.argv[2],"w")
	prop = float(sys.argv[3])

	seqs = {} #key is name and value is seq
	finalseqs = {}
	first = True
	seqlength = 0
	for i in infile:
		if first ==True:
			first = False
			continue
		i = i.strip()
		if len(i) > 1:
			spls = i.split()
			seqs[spls[0]] = spls[1]
			seqlength = len(spls[1])
			finalseqs[spls[0]] = ""
	
	numfin = int(prop * seqlength)
	print "final size: ",numfin
	samp = random.sample(range(0,seqlength),numfin)
	outfile.write("#NEXUS\n")
	outfile.write("BEGIN DATA;\n")
	outfile.write("\tDIMENSIONS NTAX="+str(len(seqs))+" NCHAR="+str(numfin)+";\n")
	outfile.write("\tFORMAT DATATYPE = protein GAP=- MISSING=?;\n")
	outfile.write("\tMATRIX\n")
	for i in samp:
		for j in finalseqs:
			finalseqs[j] += seqs[j][i]
	for i in finalseqs:
		outfile.write("\t"+i+"\t"+finalseqs[i]+"\n")
	outfile.write(";\n")
	outfile.write("END;\n")
