import os,sys
import newick3,phylo3

"""
This assumes that the names will be speciesnames@number and will ignore the number

This should check to see if for a particular sister species, there are genes that show the relationship
"""

if __name__ == "__main__":
	if len(sys.argv) != 5:
		print "usage: python calc_sister_species_in gene_trees.py sister1 sister2 folder outfile"
		sys.exit()
	sis1 = sys.argv[1]
	sis2 = sys.argv[2]
	folder = sys.argv[3]
	genes = [] #this will be a list of dictionaries for each tree
	for i in os.listdir(folder):
		if "RAxML_bestTree.RAxML_bestTree." == i[:len("RAxML_bestTree.RAxML_bestTree.")]:
			print i
			dict = {}
			x = open(str(folder)+"/"+str(i),"r")
			tree = newick3.parse(x.readline())
			for s in tree.leaves():
				nm = str(s.label.split("@")[0])
				if nm != sis1 and nm != sis2:
					continue
				other = "" #the sister to look for
				if nm == sis1:
					other = sis2
				else:
					other = sis1
				both = False
				sisters = s.get_sisters()
				for z in sisters:
					if z.label != None and z.label != nm:
						znm = z.label.split("@")[0]
						if znm == other:
							both = True
				if both == True:
					if i not in genes:
						genes.append(i)
	out = open(sys.argv[4],"w")
	for i in genes:
		out.write(i+"\n")
	out.close()
