#! /usr/bin/env python
import sys
import re

usage="""
parseblast.py

Usage: 

parseblast.py blast_in.xml blast_out.xml > out.txt

Summarizes blast results

Converts blast xml files where all records are in a single <BlastOutput> block to one where there
is one query per <BlastOutput> block
"""

e_cutoff = 0.00001

n_hits = 0

if len(sys.argv)<3:
	print usage	

else:
	# Create a dictionary of sequences
	filename = sys.argv[1]
	outfilename = sys.argv[2]
	
	infile = open(filename, "r")
	outfile = open(outfilename, "w")
	
	query = ''
	length = ''
	numreads = ''
	evals = []
	open_it_re = re.compile(" *<Iteration>", re.IGNORECASE)
	close_it_re = re.compile("</Iteration>", re.IGNORECASE)
	description_re = re.compile("<Iteration_query-def>(\w+)\s+length=(\d+)\s+numreads=(\d+)</Iteration_query-def>", re.IGNORECASE)
	eval_re = re.compile("<Hsp_evalue>(.+)</Hsp_evalue>", re.IGNORECASE)
	
	ignore_re = re.compile("</BlastOutput_iterations>|</BlastOutput>", re.IGNORECASE)
	
	print "query\tlength\tnumreads\teval"
	
	header = ""
	footer = "</Iteration>\n      </BlastOutput_iterations>\n</BlastOutput>"
	flag = 0
	
	
	for line in infile:
		if flag < 1:
			header = header + line
		
		#Skip the final closing lines since these are added in later and would otherwise double up
		m = ignore_re.search(line)
		if m:
			continue
		
		m = open_it_re.search(line)	
		if m:
			flag = 1
			line = open_it_re.sub(header, line)
		
		m = close_it_re.search(line)	
		if m:
			min_e = -1
			if len(evals) > 0:
				min_e = min(evals)
				if min_e <= e_cutoff:
					n_hits = n_hits + 1
			
			print query + "\t" + length + "\t" + numreads + "\t" + str(min_e)
			
			
			
			query = ''
			length = ''
			numreads = ''
			evals = []
			line = close_it_re.sub(footer, line)
			
		m = description_re.search(line)
		if m:
			query = m.group(1)
			length = m.group(2)
			numreads = m.group(3)
		
		m = eval_re.search(line)
		if m:
			evals.append(float(m.group(1)))
		
		

		#Write the line if the header flag has been set
		if flag > 0:
			outfile.write(line)
		
#print n_hits