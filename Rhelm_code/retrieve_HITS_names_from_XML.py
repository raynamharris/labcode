""" 
Written by RHelm
08DEC2011
last modified: 08DEC2011
"""
from string import *
import sys

if len(sys.argv) < 2:
	print "this script requires two input files as arguments"
	sys.exit(1)
	
InputFile = sys.argv[1]
NewFileName = sys.argv[2]


BLAST_XML_file = open(InputFile, 'r')
ReferenceName_File = open(NewFileName, 'w')


for line in BLAST_XML_file:
	if find(line, "<Hit_def>") != -1:
#		print line[19:-11]
		ReferenceName_File.write(">"+line[19:-11]+"\n")
		
BLAST_XML_file.close()
ReferenceName_File.close()
