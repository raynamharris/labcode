
"""
written by Rhelm
20FEB2012
last updated: 20FEB2012


Infile cluster format (tab sensitivie):
1		jgi|Nemve1|220216|fgenesh1_pg.scaffold_488000001	jgi|Nemve1|211245|fgenesh1_pg.scaffold_134000036	
2		jgi|Nemve1|137206|e_gw.393.3.1	jgi|Nemve1|222264|fgenesh1_pg.scaffold_1489000002	jgi|Nemve1|224438|fgenesh1_pg.scaffold_4737000001

eg infile test_data format (quote sensitive):

jgi|Nemve1|20000|gw.51.3.1	 4 2 4 1 2 4 4 5 23 29 32 19 4 0 1 1 0 1 0 0 0 0 0 3
jgi|Nemve1|220216|fgenesh1_pg.scaffold_488000001	 0 0 1 0 0 1 0 3 9 9 8 11 0 0 0 0 0 0 0 0 0 0 0 0
jgi|Nemve1|139805|e_gw.488.2.1	 23 47 26 19 22 14 8 8 69 110 72 90 10 3 5 5 10 1 7 0 1 5 6 34

"""

import sys

test_data = open(sys.argv[1])
raw_cluster = open(sys.argv[2])
cluster_data = raw_cluster.readlines()

#this turns the cluster file into proper format
split_clusters = []
for cluster in cluster_data:
	strip_clust = cluster.strip("\n")
	split = strip_clust.split("\t")
	split_clusters.append(split)

# This function moves through each line and adds the cluster number cluster[0] and reference name cluster[i] to a tuple, which then is appended into tuple_list
# from the tuple lis ta cluster_dict is made, with the key being the cluster[i], and the value being the cluster number cluster[0]
def cluster_to_dict(n):
	tuple_list = []
	tuples = ()
	for cluster in n:
		for i in range(len(cluster)):
			if i >= 2:							# ensure this number is correct based on file
				tuples = cluster[i], 
				tuples += cluster[0],
				tuple_list.append(tuples)
	cluster_dict = dict(tuple_list)	
	return cluster_dict

# this function takes a R data matrix with the reference name as the first column, and compares that name to the keys in the dict (calling the dict function in the key,value line), if they match, it appends the key to the data matrix
def name_comparison(data):
	new_data = []
	for gene in data:
		newline = gene.strip("\n")
		gene = newline.split("\t")
		for key, value in cluster_to_dict(split_clusters).iteritems():
			if gene[0] == key:
				gene.append(value)
				
				new_data.append(gene)
	return new_data

#calling the name comparision funciton on our data
print name_comparison(test_data)