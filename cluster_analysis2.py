import networkx as nx
import sys
import numpy as np
import matplotlib as mpl
#For viz, uncomment the following 2 lines
#mpl.use('Agg')
#import matplotlib.pyplot as plt

def graph(hits):
	'''Returns graph'''

	G=nx.Graph()
	for line in hits:
		id_from, id_to = line.split()[:2]
		if (id_from == id_to): continue
		G.add_node(id_from)
		G.add_node(id_to)
		G.add_edge(id_from, id_to)
	return G

def connected_components(graph):
	'''Takes full graph and returns connected components'''

	CC = nx.connected_component_subgraphs(graph)
	return CC

def hub_spoke_graph(connected_component):
	'''Takes connected component, if average degree centrality
	< 0.2, return connected_component'''

	avg = np.mean(nx.degree_centrality(connected_component).values())
	if avg < 0.2:
		return connected_component

def hub_nodes(connected_component):
	'''Takes connected component and returns nodes with
	high degree centrality (0.7 in this case).'''

	dg = nx.degree_centrality(connected_component)
	hubs = []
	for k, v in dg.iteritems():
		if v > 0.8:
			hubs.append(k)
	return hubs

def break_graph(graph):
	'''Takes connected component and breaks it at hub nodes.
	It returns the new graph'''

	h_nodes = [n for n in graph.nodes() if n in hub_nodes(graph)]
	if h_nodes:
		# For now, modifying the graph in place
		graph.remove_nodes_from(h_nodes)
		# For now, removing only isolated nodes
		graph.remove_nodes_from(nx.isolates(graph))
		return True
	else:
		return False

def process_graph(full_graph):
	'''Takes full graph and returns connected components with
	average degree centrality larger than 0.2'''

	to_process = [full_graph]
	result = []
	while to_process:
		g = to_process.pop()
		hub_spokes = []
		for c in connected_components(g):
			if hub_spoke_graph(c):
				hub_spokes.append(c)
			else:
				result.append(c.nodes())
		for h in hub_spokes:
			if break_graph(h):
				to_process.append(h)
			else:
				result.append(h.nodes())
	return result

if __name__=="__main__":
	if len(sys.argv) < 2:
		print "Usage: python cluster_analysis2.py hits.tab"
		sys.exit(0)

	infile = open(sys.argv[1])
	full_graph = graph(infile)
	all_components = process_graph(full_graph)
	print len(all_components)
	#spokes = hub_spoke_connected_components(full_graph)
	#print len(spokes)
	#hubs = hub_nodes(spokes)
	#print len(hubs)

#For viz, uncommment and modify the following

#for i, g in enumerate(spokes[1:3]):
#	pos = nx.spring_layout(g)
#	print g.number_of_nodes()
#	print g.number_of_edges()
#	h_nodes = [n for n in g.nodes() if n in hubs]
#	normal_nodes = [n for n in g.nodes() if n not in hubs]
#	nx.draw_networkx_nodes(g, pos,
#				nodelist=h_nodes,
#				node_color='r',
#				node_size=100,
#				with_labels=False,
#				alpha = 0.8)
#	nx.draw_networkx_nodes(g, pos,
#				nodelist=normal_nodes,
#				node_color='k',
#				node_size=10,
#				with_labels=False,
#				alpha = 0.5)
#	nx.draw_networkx_edges(g, pos, width=0.7, alpha = 0.7)
#	plt.axis("off")
#	plt.savefig("cluster%d.png" % i)
#	plt.clf()
#	nx.write_graphml(g, "graph%d" %i)

