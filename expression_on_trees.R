# R functions that assist with mapping gene expression data onto trees

library(ape)

build_character_vector <- function(x, phy, col = 2) {
	# Creates a character vector for the tips of phy from dataframe x. x can include character data for
	# a superset of taxa. If some taxa are missing, their character is recorded as NA. The taxon name 
	# is expected in column 1 of x, the character in column 2.
	
	y <- rep(NA, length(phy$tip.label))
	names(y) <- phy$tip.label
	for(i in 1:length(y)){
		name <- names(y[i])
		if (name %in% x[,1]){
			y[i] <- x[x[,1] == name,col]
		}
	}
	return(y)
}
 
prune_empty_taxa <- function(phydata){
	
	phydata$x_pruned <- phydata$x[! is.na(phydata$x)]
	phydata$sd_pruned <- phydata$sd[! is.na(phydata$x)] #prune on x rather than sd so both are the same
	
	phydata$phy_pruned <- phydata$phy
	
	tips <- phydata$phy_pruned$tip.label
	for(tip in tips){
		if (! tip %in% names(phydata$x_pruned)){
 			phydata$phy_pruned <- drop.tip(phydata$phy_pruned, tip)
 		}
 	}
 	
 	return(phydata)
}

remap_ace <- function(phydata){
	# Remaps ancestral character state reconstructions from pruned tree to unpruned tree
	
	ace_p <- phydata$ace_pruned$ace
	
	# Get ordered internal nodes of unpruned tree
	n_u <- internal_nodes(phydata$phy)
	n_u <- n_u[order(n_u)]
	
	# Create the vector for unpruned tree that ace values will be mapped to
	ace_u <- rep(NA, length(n_u))
	names(ace_u) <- n_u
	
	for (i in 1:length(ace_p)){
		a <- ace_p[i] # Get the ancestral value
		n <- names(ace_p)[i] # get the name of the internal node on the pruned tree
		n_u <- phydata$map[phydata$map[,1] == n,2] # Get the node number of the corresponding node on the unprunded tree
		ace_u [names(ace_u) == n_u] <- a # Place the ancestral value according to the node name
		
	}
	
	phydata$ace$ace <- ace_u
	
	return(phydata)
}


plot_expression <- function(phydata, main="", sub="", pruned=TRUE){
	# Plot gene expression on the tree
	# If pruned = FALSE, expression is plotted on the full tree
	
	
	# Load up the items to plot depending on whether the pruned or unpruned tree is to be shown
	x <- c()
	xsd <- c()
	phy <- phydata$phy_pruned
	n <- c()
	
	if( pruned ){
		phy <- phydata$phy_pruned
		x <- phydata$x_pruned
		xsd <- phydata$sd_pruned
		n <- phydata$ace_pruned$ace
	}
	else{
		phy <- phydata$phy
		x <- phydata$x
		xsd <- phydata$sd
		n <- phydata$ace$ace
	}
	
	
	# Calculate scaling for expression points
	max_size <- 6
	max_x <- max(abs(phydata$x_pruned))
	scaling <- max_size/max_x
	
	
	# Draw the tree
	# With cex=0.75, 45 tips are the max that can be displayed without overlap
	# Default fin is c(7,7)  width height, so say hight is number of tips/6. Leave width at 7. 
	
	plot(phy, cex=0.75, adj=2)
	
	# Plot expression
	tiplabels(pch = 21, cex=(sqrt(scaling * abs(x))), bg=3+sign(x))
	nodelabels(pch = 21, cex=(sqrt(scaling * abs(n))), bg=3+sign(n))
	
	# Plot +- 2 SD
	tiplabels(pch = 21, cex=(sqrt(scaling * (abs(x)+2*xsd))), bg="#00000000")
	tiplabels(pch = 21, cex=(sqrt(scaling * (abs(x)-2*xsd))), bg="#00000000")
	
	
	legend("bottomright", c(paste("LogFC =", round(max_x,2)), paste("LogFC =", round(-max_x,2))), pch=c(21,21), pt.cex=sqrt(max_size),  pt.bg=c(4,2), ncol=1)
	
	title(main = main, sub=sub)

}


thin_data <- function(x){
	# Removes rows from x where values are illegal for later analyses
	x_thin <- x[! is.na(x[,2]),]
	x_thin <- x_thin[abs(x_thin[,2]) != Inf,]
	
	return(x_thin)
}

add_id <- function(phydata, ids){
	tips <- phydata$phy_pruned$tip.label
	
	s <- ids[ids[,1] %in% tips,2] 
	concat <- paste(s, collapse="||")
	
	names <- unlist(strsplit(concat, "\\|\\|"))
	
	names <- names[! grepl("NA", names)]
	names <- names[! grepl("PREDICTED: similar to predicted protein", names)]
	names <- names[! grepl("PREDICTED: predicted protein", names)]
	names <- names[! grepl("^predicted protein", names)]
	names <- names[! grepl("PREDICTED: hypothetical protein", names)]
	names <- names[! grepl("unnamed", names)]
	
	phydata$id <- names[1]
	return(phydata)
}




##################################################################################
## Pre-processing expression data ################################################

apply_normalizations <- function(dge){
	# Takes a DGEList object, and returns an array of normalized counts
	
	# Calculate the normalization multipliers, scaled to reads per million
	norm <- 1e6 / (dge$samples[,"lib.size"] * dge$samples[,"norm.factors"])
	
	# Apply them to the counts
	norm_counts <- t(t(dge$counts)*norm)
	
	# Replace 0's
	# EdgeR uses min.val<-8.783496e-16, as set in estimatePs.R, though not sure why.
	norm_counts[ norm_counts == 0 ] <- 1e-6
	
	
	# For now, assume that there are three specimens with two paired samples each
	fc1 <- log2(norm_counts[,1]/norm_counts[,2])
	fc2 <- log2(norm_counts[,3]/norm_counts[,4])
	fc3 <- log2(norm_counts[,5]/norm_counts[,6])
	
	norm_counts <- cbind(norm_counts, fc1)
	norm_counts <- cbind(norm_counts, fc2)
	norm_counts <- cbind(norm_counts, fc3)
	
	fcmean <- apply(norm_counts[,7:9], 1, mean)
	fcsd <- apply(norm_counts[,7:9], 1, sd)
	
	norm_counts <- cbind(norm_counts, fcmean)
	norm_counts <- cbind(norm_counts, fcsd)
	
	#names(norm_counts[, 7:11]) <- c("fc1", "fc2", "fc3", "fc_mean", "fc_sd")
	
	
	return(norm_counts)
}


##################################################################################
## Some basic tree operations ####################################################

ancestors <- function(phy, a) {
	# returns a vector of the ancestors of node a in tree phy, ordered from root to most recent
	v <- c()
	while( length(phy$edge[phy$edge[,2] == a,1]) > 0 ){
		a <- phy$edge[phy$edge[,2] == a,1]
		v <- append(v, a)
	}
	v <- rev(v)
	return(v)
}


tips <- function(phy) {
	# returns a vector of all the tips, with their names
	
	t <- phy$edge[ ! phy$edge[,2] %in% phy$edge[,1] ,2]
	t <- t[order(t)]
	
	names(t) <- phy$tip.label
	
	return(t)
}

nodes <- function(phy) {
	# returns an ordered vector of all nodes in the tree, including tips and internal nodes
	
	n <- unique(c(phy$edge[,1], phy$edge[,2]))
	n <- n[order(n)]
	
	return(n)
}

internal_nodes <- function(phy) {
	n <- nodes(phy)
	t <- tips(phy)
	
	return(n[! n %in% t])
}

descendants <- function(phy, a) {
	# returns a vector of all the descendants of node a, including tips and internal nodes
	# Based on a breadth-first search
	q <- c(a)
	d <- c()
	while (length(q)>0){
		# dequeue first item
		n <- q[1]
		q <- q[q != n]
		
		# add it to the descendants vector
		d <- append(d, n)
		
		# add it's descendants to the queue
		q <- append(q, phy$edge[phy$edge[,1] == n,2])
	}
	
	# Remove the original source node from the list
	d <- d[d != a ]
	return(d)
}

tip_descendants <- function(phy, a) {
	# returns a vector of all the tips that are descendants of a
	t <- tips(phy)
	return(t[t %in% descendants(phy,a)])
}

mrca_multiple <- function(phy, v) {
	# returns the most recent common ancestor of nodes v in tree phy
	
	# take care of special case
	if (length(v) == 0){
		return(NA)
	}
	
	
	h <- c() # a vector of all common ancestors
	for (n in v) {
		a <- ancestors(phy, n)
		if (length(h) == 0){
			h <- a
		}
		else{
			h <- h[h %in% a]
		}
		
		
	}
	
	# the last element of h is the most recent common ancestor
	return(h[length(h)])
}

map_subtree_to_tree <- function (phya, phyb){
	# returns an array of nodes in phya (column 1) and their corresponding node in phyb (column 2). Assumes that phya is 
	# a pruned subset of phyb
	
	# First do the tips, using the names
	tipsa <- tips(phya)
	tipsb <- tips(phyb)
	
	tipsb <- tipsb[names(tipsb) %in% names(tipsa)] # take the subset of tipsb that are in tipsa
	tipsb <- tipsb[ match(names(tipsb), names(tipsa)) ] # order tipsb in same order as tips b
	
	v <- cbind(tipsa, tipsb)
	
	# Now do internal nodes
	na <- internal_nodes(phya)
	
	for (a in na){
		# get all the tip descendants of a on phya
		da <- tip_descendants(phya, a)
		
		# get the corresponding tips from phyb
		db <- c()
		for (i in da){
			j <- v[v[,1] == i ,2]
			db <- append(db, j)
		}
		
		# find the node in phyb that is the mrca of tips da
		b <- mrca_multiple(phyb, db) 
		
		# add it to the array
		v <- rbind(v, c(a,b))
	}
	
	return(v)
	
}

get_taxa <- function (tips){
	# Takes a vector of tip names, and returns a vector of the corresponding species names
	taxa <- c()
	for (tip in tips){
		taxon <- strsplit(tip, "@")[[1]][1]
		taxa <- append(taxa, taxon)
	}
	
	return(taxa)
}


restrict_taxa <- function(phy, taxa){
	# Prunes all tips from phy that do not belong to taxa
	tips <- phy$tip.label
	tip_taxa <- get_taxa(tips)
	tips_to_prune <- tips[! tip_taxa %in% taxa]
	if ((length(tips) -length(tips_to_prune)) < 2 ){
		# Return NULL if there would be less than two remaining tips. Pruning to less than two 
		# tips crashes drop.tip
		return(NULL)
	}
	else{
		return(drop.tip(phy, tips_to_prune))
	}
}


##################################################################################
## Interacting with primeTV and primeGSM##########################################

write_leaf_mapping <- function(phy, filename) {
	# Writes the leaf mapping file needed by prime*
	tips <- phy$tip.label
	taxa <- get_taxa(tips)
	map <- cbind(tips, taxa)
	write.table(map, row.names = FALSE, quote = FALSE, col.names = FALSE, file = filename)
}



##################################################################################
## Analysis workflows ############################################################


restrict_taxa_bulk <- function(taxa, suffix) {
	# Loops over all the gene trees in the current directory, pruning all tips in each tree that 
	# don't belong to the species included in the vector taxa. Output tree file names are appended 
	# with the specified suffix
	
	files<-list.files()
	for (file in files) {
		print(paste("Processing", file))
  		phy <-read.tree(file)
  		print("  Pruning...")
  		phy_pruned <- restrict_taxa(phy, taxa)
  		if(length(phy_pruned$tip.label) >= 2){ # Don't write the tree if it has less than two taxa
  			print("  Writing...")
  			write.tree(phy_pruned, file = paste(file, suffix, sep=""))
  		}
  	}
	
}



preprocess_phy <- function(x, phy){
	# Take care of all early steps of reconstruction
	# x is the character superset
	# phy is the input tree
	
	
	phydata <- list()
	phy <- root(phy, phy$tip.label[1], resolve.root=TRUE)
	phydata$phy <- phy
	phydata$x <- build_character_vector(x, phydata$phy,2)
	phydata$sd <- build_character_vector(x, phydata$phy,3)
	
	phydata <- prune_empty_taxa(phydata)
	
	phydata$map <- map_subtree_to_tree(phydata$phy_pruned, phydata$phy)
	
	return(phydata)
}

loop_over_clusters <- function(x, ids=data.frame()) {

	# x is the master character file, ids is an optional dataframe where column 1 is gene names 
	# and column 2 is putative ids from blast
	#
	
	# Returns a dataframe with details on all the evaluated trees
	
	files<-list.files()
	x <- thin_data(x)
	
	# Initialize the empty data frame
	header <- c("tree.name", "n.tips", "n.tips.with.data","n.tips.with.pos.data","n.tips.with.neg.data","tips.with.data", "phydata")
	m <- data.frame(array(dim= c(length(files),length(header)))) # A data frame with various stats on each tree
	names(m) <- header
	
	# Workaround for problems assigning lists to vector elements
	mode(m$phydata) <- "list"
	
	n.tips.with.pos.data <- 0
	n.tips.with.neg.data <- 0
	
	count <- 0
	
	for (file in files) {
		count = count+1
		print(paste("Processing", file))
  		phy <-read.tree(file)
  		tips_with_data <- phy$tip.label[phy$tip.label %in% x[,1]]
  		print(paste(length(tips_with_data), "tips with data"))
  		if (length(tips_with_data) >= 3){
  			print(" Preprocessing...")
			phydata <- preprocess_phy(x, phy)
			phydata <- add_id(phydata, ids)
			
			n.tips.with.pos.data <- sum(phydata$x_pruned > 0)
			n.tips.with.neg.data <- sum(phydata$x_pruned < 0)
			
			
			try({
				print(" Reconstructing...")
				phydata$ace_pruned <- ace(phydata$x_pruned, phydata$phy_pruned)
				
				print(" Remapping...")
				phydata <- remap_ace(phydata)
				
				#m$phydata [m$tree.name == file] <- phydata
				m$phydata [[count]] <- phydata
				
				ntips <- length(phydata$phy$tip.label)
				
				print(" Plotting...")
				
				# At current scaling, can fit about 6 taxon names per vertical inch, add an inch for margin
				# The average depth of the root on a random tree will be proportional to about
				# log2(number of tips), leave a couple inches for names
				
				
				pdf(paste(file, ".pdf", sep=""), height = ntips/6+3, width = log2(ntips)*2+2)
				plot_expression(phydata, main=file, sub=phydata$id, prune = FALSE)
				dev.off()	
				
				
				
			})
  		}
  		else{
  			print(" Skipping: Fewer than 3 tips with data")
  		}
  		
  		print(" Writing tree summary...")
  		
  		m$tree.name[count] <- file
  		
  		m$n.tips[count] <- length(phy$tip.label)
  		m$n.tips.with.data [count] <- length(tips_with_data)
  		
  		m$n.tips.with.pos.data [count] <- n.tips.with.pos.data
  		m$n.tips.with.neg.data [count] <- n.tips.with.neg.data
  		
  		
  		tips.string <- paste(tips_with_data, collapse=',')
  		m$tips.with.data [count] <- tips.string
  		
  		
  	}
  	
  	rownames(m) <- m$tree.name
  	return(m)
  	
}
