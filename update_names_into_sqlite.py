import sys,sqlite3,os
from colors import *
from Bio import SeqIO

#database = "mollusk.db"

#mollusks outgroups
#outgroups = ["Lingula_anatina","Schmidtea_mediterranea", "Chaetopterus_variopedatus","Terebratalia_transversa","Cerebratulus_lacteus", "Carinoma_mutabilis","Capitella","Helobdella_robusta","Macrostomum_lignano","Paraplanocera_oligoglena","Drosophila_melanogaster"]
#cnidaria grant outgroups
#outgroups = ["Hydra_magnipapillata","Nematostella_vectensis","Clytia_hemisphaerica","Podocoryna_carnea","Hydractinia_echinata"]
#metazoan
outgroups = ["Capsaspora_owczarzaki","Sphaeroforma_arctica","Salpingoeca_rosetta","Monosiga_ovata","Monosiga_brevicollis","Monosiga_ovata","Allomyces_macrogynus","Batrachochytrium_dendrobatidis","Phycomyces_blakesleeanus","Proterospongia_sp","Rhizopus_orizae","Spizellomyces_punctatus","Saccharomyces_cerevisiae","Cryptococcus_neoformans","Capsaspora_owczarzaki_ATCC_30864","Sphaeroforma_arctica_JP610","Amoebidium_parasiticum"]

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python update_names_into_sqlite.py database DIR"
		sys.exit(0)
	database = sys.argv[1]
	if os.path.exists(database) == False:
		print "the database has not been created or you are not in the right place"
	con = sqlite3.connect(database)
	DIR= sys.argv[2]
	cur = con.cursor()
	for i in os.listdir(DIR):
		cur.execute("SELECT * FROM species_names where name = '"+i+"';")
		a = cur.fetchall()
		if len(a) == 0:
			print BLUE,"adding ",RESET,GREEN,i,RESET
			ingroup = i not in outgroups
			cur.execute("INSERT INTO species_names(name,ingroup) VALUES (?,?)",(i,ingroup))
			con.commit()
		else:
			print RED,i,"already there",RESET
	con.close()
