#! /usr/bin/env python
import sys
import re
from Bio import SeqIO

usage="""

Usage: 

singleton_extractor.py [fasta or qual] 454ReadStatus.txt singletons_out.fasta rawreads1.fasta rawreads2.fasta ...

Combines all the singletons listed in 454ReadStatus.txt from rawreads1.fasta etc into a single fasta file, singletons_out.fasta

either fasta files or qual files can be specified

Prints a list of ids for sequences that aren't encountered at all or that are encountered multiple times
"""



notes="""

"""

type = 'qual'

if len(sys.argv)<4:
	print usage	

else:
	# Create a dictionary of sequences
	type = sys.argv[1]	
	ReadStatus_name = sys.argv[2]
	out_fasta_name = sys.argv[3]
	in_names = sys.argv[4:]
	
	# Create a dictionary keyed with the names of the singletons
	ReadStatus_handle = open(ReadStatus_name, "r")
	
	# Dictionary keyed by sequence ID with a value indicating the number of times a sequence
	# with this id is written to the output file
	singles = {}
	
	
	for line in ReadStatus_handle:
		line = line.rstrip()
		fields = line.split()
		if fields[1] == 'Singleton':
			singles[fields[0]] = 0
				

	#Open the ouput file for writing
	out_fasta_handle = open(out_fasta_name,"w")
	
	#Loop over the input sequnces and write singletons to the output
	for in_name in in_names:
		in_handle = open(in_name, "r")
		for record in SeqIO.parse(in_handle, type):
			id = record.id
			if singles.get(id, -1) > -1:
				singles[id] = singles[id] + 1
				outlist = [record]
				SeqIO.write(outlist, out_fasta_handle, type)
		in_handle.close()
	
	out_fasta_handle.close()
	
	for id in singles.keys():
		if singles[id] < 1:
			print id, 'not found'
		if singles[id] > 1:
			print id, 'found ', singles[id], ' times'
			
		
	
	#Print out a list of the sequences that were written more than once or not at all
