import sys,os

"""
this takes the output from an mcl run
the input to that mcl run
then it takes an out folder

give the threshold below, if any cluster is larger than that, it will cluster the cluster with mcl
"""

class Cluster:
	def __init__(self):
		self.folder = ""
		self.mcl = 0
		self.id = 0
		self.parent = 0

def extract_clusters(infile, outfolder):
	#work on the things below after
	print "extracting each resulting cluster"
	for i in final_clusters:
		cmd = "mkdir "+i.folder+"/"+i.id+"_out"
		print cmd
		os.system(cmd)
		cmd = "python ~/Dropbox/programming/phylogenomics/extract_clusters_from_mcl_out_sqlite_trans_masked.py /media/hd2/MOLLUSK/mollusk.db "+folder+"/"+str(i)+"_mcl_out "+folder+"/"+str(i)+"_out"
		print cmd
		os.system(cmd)

def calc_raw_stats(infile, outfile):
	print "calculating the raw stats on the runs"
	for i in range(len(final_clusters)):
		cmd = "python ~/Dropbox/programming/phylogenomics/get_raw_cluster_stats.py "+folder+"/"+str(i)+"_out "+folder+"/"+str(i)+"_out_raw_stats"
		print cmd
		os.system(cmd)

THRESH = 500
startI = 1.1
finishI = 7.1
tempfolder = "TEMP"

if os.path.exists(tempfolder):
	print tempfolder + " exists. please delete"
	sys.exit()
else:
	os.system("mkdir "+tempfolder)

if __name__ == '__main__':
	if len(sys.argv) != 3:
		print "python iterative_clustering_mcl_from_mcl_out_and_mcl_in.py infile_mcl_in outfile"
		sys.exit(0)
	print "reading clusters"
	infile = sys.argv[1]
	outfile = open(sys.argv[2],"w")
	
	root = Cluster()
	root.mcl = startI
	root.parent = None
	root.folder = tempfolder
	root.id = "root"
	cmd = "cp "+infile+" "+tempfolder+"/"+root.id
	os.system(cmd)

	
	graphs_to_run = [[infile,root]]
	
	while len(graphs_to_run) > 0:
		tinfile,curnode = graphs_to_run.pop()
		cmd = "mcl "+curnode.folder+"/"+curnode.id+" --abc -te 12 -I "+str(curnode.mcl)+" -tf 'gq(20)' -o "+curnode.folder+"/"+curnode.id+"_mcl_out"
		print cmd
		os.system(cmd)
		bigclusters = []
		tin = open(curnode.folder+"/"+curnode.id+"_mcl_out","r")
		count = 0
		for i in tin:
			spls = i.strip().split("\t")
			if len(spls) >= THRESH and curnode.mcl < finishI:
				a = Cluster()
				a.parent = curnode
				a.mcl = curnode.mcl + 1#start mcl I value
				a.folder = curnode.folder+"/"+curnode.id+"_sub"
				a.id = str(count)
				bigclusters.append(a)
			else:
				outfile.write(i)
			count += 1
		tin.close()
		
		if curnode.mcl < finishI and len(bigclusters) > 0:
			cmd = "mkdir "+curnode.folder+"/"+curnode.id+"_sub/"
			os.system(cmd)
			cmd = "/home/smitty/Dropbox/projects/phylogenomics_tools/iterative_mcl/create_cluster_files "+str(THRESH)+" "+curnode.folder+"/"+curnode.id+"_mcl_out "+curnode.folder+"/"+curnode.id+" "+curnode.folder+"/"+curnode.id+"_sub/"
			print cmd
			os.system(cmd)
			for i in bigclusters:
				graphs_to_run.append([curnode.folder+"/"+i.id,i])
		

		
	
