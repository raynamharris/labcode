# Written by Casey Dunn, ..., 2011
# Brown University
# Please see the LICENSE file included as part of this package.
"""
Randomizes the order of sequences in a fastq file.

To randomize s_1_1_sequence.txt, write the results to s_1_1_sequence.txt.shuffled, and write a text
file order.txt indicating the original position of each sequence in the shuffled file:

randomize_fastq.py -i s_1_1_sequence.txt -o s_1_1_sequence.txt.shuffled -w order.txt


To randomize s_1_2_sequence.txt, write the results to s_1_2_sequence.txt.shuffled, and read a text
file indicating the original position of each sequence in the randomized file:

randomize_fastq.py -i s_1_2_sequence.txt -o s_1_2_sequence.txt.shuffled -r order.txt

The above would be a typical workflow to shuffle two paired end files in the same order. If you are 
shuffing a single end file, just ommit -r and -w.

randomize_fastq.py -h will provide details on options

randomize_fastq.py -v gives verbose output

randomize_fastq.py -f gives the rporting frequency for verbose output


"""

from optparse import OptionParser
import random
from biolite import seqlite
from biolite.seqliteio import SeqLiteIO


def randomize_fastq(input_name, output_name, writeorder_name='', readorder_name='', verbose=False, freq=1000000):
	"""
	
	"""
	# Read in the sequences
	seqs = []
	s = ""
	if verbose:
		print("Reading sequences from " + input_name)
	input_file = SeqLiteIO(input_name, "r")


	while 1:
		# Read the next records from the files
		try:
			record = input_file.next()
		except StopIteration:
			break
		except ValueError:
			print ("{0} reads were read successfully prior to the error in {1}".format(len(seqs), input_name))
			raise
		seqs.append(record)
		if verbose and (len(seqs) % freq == 0):
			print(" {0}".format(len(seqs)))
	
	
	if verbose:
		print " " + str(len(seqs)) + " total sequences read"
	
	
	# Generate the line order
	ord = []
	if  readorder_name == '':
		# Create a random order
		if verbose:
			print "Creating a random sequence order"
		ord = range(0, len(seqs))
		random.shuffle(ord)
	else:
		# read the order form a file
		if verbose:
			print "Reading line order from " + readorder_name
		order_handle = open(readorder_name, "r")
		for line in order_handle:
			line = line.rstrip()
			if len(line) > 0:
				ord.append(int(line))
	
	if verbose:
		print str(len(ord)) + " entries in the order list"
	
	# Check things over
	if len(ord) != len(seqs):
		raise ValueError("ERROR: There are " + str(len(seqs)) + " sequences, but " + str(len(ord)) + " entries in the order list")
	
	
	# Write out the sequences
	if verbose:
		print "Writing sequences to " + output_name
		
	output_file = SeqLiteIO(output_name, "w")
	n = 0
	for i in ord:
		output_file.write(seqs[i])
		if verbose and (n % freq == 0):
			print " " + str(n) + " sequences written"
		n = n +1
	
	if verbose:
		print str(n) + " total sequences written"
	
	# Write the order
	if  writeorder_name != '':
		if verbose:
			print "Writing sequence order to " + writeorder_name
		writeorder_handle = open(writeorder_name, "w")
		for i in ord:
			writeorder_handle.write(str(i) + "\n")
			
	# Clean up
	if verbose:
		print "Done."

def main():
	"""Parse options for command line access
	
	"""
	
	# This bit is a bit kludgey since defaults are redundantly defined in parser.add_option and by
	# the sanitize_illumina_reads function. This needs to be migrated to argparse and cleaned up.
	parser = OptionParser()
	parser.add_option("-i", "--input", action="store", type="string", dest="input_name")
	parser.add_option("-o", "--output", action="store", type="string", dest="output_name")
	parser.add_option("-w", "--writeorder", action="store", type="string", dest="writeorder_name", default='')
	parser.add_option("-r", "--readorder", action="store", type="string", dest="readorder_name", default='')
	parser.add_option("-v", action="store_true", dest="verbose", default=False)
	parser.add_option("-f", action="store", type="int", dest="freq", default=100000)
	(options, args) = parser.parse_args()
	
	randomize_fastq(options.input_name, options.output_name, writeorder_name = options.writeorder_name, readorder_name = options.readorder_name, verbose = options.verbose, freq = options.freq)


if __name__ == "__main__":
	main()
	


			
	
"""
Two example fastq sequences:


@EAS1745:1:1:1973:1034#0/1
CCAAGGCAGAGGAACTTGAAGATGCCAAGCGCAAGCTCCAAGCCAAACTCAATGAAGCCGAACAGAATATGGAAGCTGCCAACGCCAAGGTCAGCCAGTTGGAG
+
hhhhhghhhhhhhchhhhhhghhhhhhgfhehhghgehhggghghggghhgefghhghhhdhffeadcddgffffdcdefadacddaaa`Y_a[a_\_\\__Y_
@EAS1745:1:1:2963:1032#0/1
CAAATCATTATCAGCCAGAAAACGTCCCCTTGCAATAAATGGACTCGGAATGTCAATGTCGTTATTGTCTTTGAATTCATAAATATCAAGTGCCAACTTATTAT
+
hhghhhfhhhhhhdhhhhhhgghhhhhhhhhhhhhffffcfffffhhfghhhghhhhhfhhhhghhhghfhhhghhhhghhhehhhhhggchhehdefddgdge

	
"""