import sys,os,sqlite3
import os.path
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
import newick3,phylo3
from colors import *

"""
this will take the output from the mcl analysis and write out the fasta
files for each cluster , this assumes that all labels are the sequence id labels

this will use the main mollusk.db for translations

USE THIS ONE!

this also requires identifying the outgroup taxa as only clusters with
at least 2 ingroup taxa will be included and at least 4 total taxa. 
"""

#database = "mollusk.db"

outgroups = ["Lingula_anatina","Schmidtea_mediterranea", "Chaetopterus_variopedatus","Terebratalia_transversa","Cerebratulus_lacteus", "Carinoma_mutabilis","Capitella","Helobdella_robusta","Macrostomum lignano","Paraplanocera oligoglena", "Drosophila melanogaster"]

SMALLESTCLUSTER = 4
SMALLESTINGROUP = 1

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "usage: python extract_seqs_from_mm_sqlite.py database INMMDIR OUTDIR"
		sys.exit(0)
	
	database = sys.argv[1]
	
	if os.path.exists(database) == False:
		print "the database has not been created or you are not in the right place"
	con = sqlite3.connect(database)
	cur = con.cursor()

	species_names = []
	species_names_id = {} #key id value species
	cur.execute("SELECT id,name FROM species_names;")
	for i in cur:
		species_names.append(str(i[1]))
		species_names_id[str(i[0])] = str(i[1])

	indir = sys.argv[2]
	OUTDIR = sys.argv[3]
	for i in os.listdir(indir):
		if i[-2:] != "mm":
			continue
		filen = indir+"/"+i
		file = open(filen,"r")
		tree = newick3.parse(file.readline())
		lvs = None
		try:
			lvs = tree.leaves()
		except:
			continue
		if len(lvs) >= SMALLESTCLUSTER:
			print i,len(lvs)
			ingroup_samp = []
			tree_out_seqs = []
			for j in lvs:
				seq = None
				sp_name = j.label.split("@")[0]
				seq_id = j.label.split("@")[1]
				cur.execute("SELECT seq FROM translated_seqs WHERE id = ?",(seq_id,))
				a = cur.fetchall()
				if len(a) > 0:
					seq = SeqRecord(Seq(str(a[0][0])))
					seq.id = str(j.label)
					seq.description = ""
					seq.name = ""
					tree_out_seqs.append(seq)
					if sp_name not in outgroups and sp_name not in ingroup_samp:
						ingroup_samp.append(sp_name)
				else:
					seq = None
				if seq == None:
					print "ERROR:no break"
					error_file.write(j+"\n")
					print j

			if len(ingroup_samp) >= SMALLESTINGROUP:
				filennumber = i.split(".")[1]
				seq_out_seqfile = open(OUTDIR+"/"+filennumber+".fasta","w")
				SeqIO.write(tree_out_seqs,seq_out_seqfile,"fasta")
				seq_out_seqfile.close()
		
