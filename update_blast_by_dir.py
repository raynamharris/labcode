import sys,sqlite3,os
from colors import *
from Bio import SeqIO

home_database = "mollusk.db"

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "usage: python update_blast_by_dir.py DIR"
		sys.exit(0)
	if os.path.exists(home_database) == False:
		print "the database has not been created or you are not in the right place"
	con = sqlite3.connect(home_database)
	species = []
	cur = con.cursor()
	cur.execute("SELECT * FROM species_names;")
	a = cur.fetchall()
	for i in a:
		species.append(str(i[1]))
	DIR = sys.argv[1]
	for i in species:
		print GREEN+i+RESET
		sDIR = sys.argv[1]+"/"+i+"/BLAST"
		database = "BLAST.db"
		if os.path.exists(sys.argv[1]+"/"+i+"/"+database) == False:
			print "the database has not been created or you are not in the right place"
			os.system("python ~/Dropbox/programming/phylogenomics/create_base_database.py "+sys.argv[1]+"/"+i+"/"+database)
			os.system("python ~/Dropbox/programming/phylogenomics/add_one_name_to_sqlite.py "+i+" "+sys.argv[1]+"/"+i+"/"+database)
		for j in os.listdir(sDIR):
			print "\t"+BLUE+j+RESET
			os.system("python ~/Dropbox/programming/phylogenomics/update_blast_into_sqlite.py "+i+" "+sDIR+"/"+j+" "+sys.argv[1]+"/"+i+"/"+database)
			#print "python ~/Dropbox/programming/phylogenomics/update_trans_seqs_into_sqlite.py "+i+" "+sDIR+"/"+j
	con.close()
