import sys,os

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print "python quickparanoid_process.py infile.txt outfile.txt"
        sys.exit(0)
    infile = open(sys.argv[1],"r")
    clusters = {} #cluster id = key, value is list of sequences
    first = True
    for i in infile:
        if first == True:
            first = False
            continue
        spls = i.strip().split("\t")
        if int(spls[0]) not in clusters:
            clusters[int(spls[0])] = []
        clusters[int(spls[0])].append(spls[2])
    infile.close()
    
    outfile = open(sys.argv[2],"w")
    for i in clusters:
        cls = clusters[i]
        outfile.write("\t".join(cls)+"\n")
    outfile.close()
