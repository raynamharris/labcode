#!/usr/bin/env python

import os, sys

Usage = """
Removes gaps (-) from a multiple sequence alignment (in Fasta format),
and returns an ungapped alignment (in Fasta format).

Usage:
  python remove_gaps_msa.py alignment.fa

"""

if __name__ == "__main__":
        if len(sys.argv) != 2:
                print Usage
                sys.exit(0)

        align_file = open(sys.argv[1], "r")
        outfile = open(os.path.splitext(os.path.basename(sys.argv[1]))[0]+"_ungapped.fa", "w")
        for line in align_file:
                line=line.strip("\n")
                if not line.startswith(">"):
                        line = line.replace('-', '')
                outfile.write(line + "\n")
        align_file.close()
        outfile.close()

