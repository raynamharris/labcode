#!/usr/bin/perl

use strict;

use Bio::SeqIO;
use Bio::Tools::Run::StandAloneBlast;
use Bio::SearchIO;
use DBI;
use Phylogenomics::Protol;

my $species = shift;
$species =~ s/_/ /g;

my $db = "protol";
my $dbh = Protol::open_dbi($db);

my $projectdir = "/Users/cdunn/est_solutions";

my $datetime = Protol::get_current_datetime();
print "Update time: $datetime\n";

my %nucleotide_hash;
my %quality_hash;
my %protein_hash;
#my %masked_hash;
my %type_hash;
my %method_hash;
my %blast_hash;

print "Processing $species\n";

my $basedir = $species;

$basedir =~ s/ /_/g;
$basedir = $projectdir . "/" . $basedir . "/";

#Delete prior database entries if they exist

my $sql = "delete from clusters where taxon = \'$species\'";
#print "$sql\n";
my $statement = $dbh->prepare($sql)
    or die "Couldn't prepare query '$sql': $DBI::errstr\n";
$statement->execute()
	or die "Couldn't execute query '$sql': $DBI::errstr\n";


#Get nucleotide sequences and quality scores

my $nucdir = $basedir . "postcluster/protein/";

opendir (IN_DIR, "$nucdir");	#open the directory to allow looping through the list of files
while (my $file= readdir IN_DIR) {
	#print "$file\n";
	if ($file =~ /(\w+)\.seq$/){
		my $header;
		my $sequence;
		my $quality;
		$file = $1;
		my $cluster = $file;
	
		# Load the sequence and quality data
		open (SEQFILE, "$nucdir$file.seq") or die "can't open $file.seq";
		while (my $line = <SEQFILE>) {
			chomp $line;
			if ($line =~ />/) {
				$header = $line;
				next;
			}
			$sequence .= $line;
		}
		close(SEQFILE) or die;
		
		$nucleotide_hash {$cluster} = $sequence;
	
		open (QUALFILE, "$nucdir$file.qlt") or die "can't open $file.qlt";
		while (my $line = <QUALFILE>) {
			chomp $line;
			if ($line =~ />/) {
				next;
			}
			$quality .= $line;
		}
		close(QUALFILE) or die;
		
		$quality_hash {$cluster} = $quality;
		
	}
}


#Get previous blast data
my $blastdir = $basedir . "partigene/blast/swissprot/";

opendir (BLAST_DIR, "$blastdir");	#open the directory to allow looping through the list of files
while (my $file= readdir BLAST_DIR) {
	#print "$file\n";
	if ($file =~ /(\w+)\.out$/){
		my $cluster = $1;
		#print "  processing $cluster, ";
		my $parser = new Bio::SearchIO(-format => 'blast',
                                 -file => "$blastdir$file",
                                 -report_type => 'blastx');
 		my $result = $parser->next_result;
 		my $nhits = $result -> num_hits;
 		my $hit_desc;
 		if ( $nhits > 0 ){
 			my $hit = $result ->next_hit;
 			$hit_desc = $hit->description;
 		}
 		$blast_hash {$cluster} = $hit_desc;
 		
 		#print "got $hit_desc\n";
	}
}


#Get the protein info

my $protdir = $basedir . "prot4est/output/";
my $prot_file = $protdir . "translations_xtn.fsa";

my $protein_in = Bio::SeqIO->new( '-format' => "FASTA", '-file' => "$prot_file" );
while (my $inseq = $protein_in->next_seq) {

    my $display_id = $inseq->display_id(); #The cluster name, but with C replaced by P, and with a suffix
    
    $display_id =~ s/_.*$//;  #get rid of the suffix
    $display_id =~ s/^...//;  #get rid of the lettering since it has an uninformative P in the third position
    
    
    my $desc = $inseq->desc();
    my $type;
    my $method;
    
    if ($desc =~ m/(.*)Method: (.*)$/ ){
    	$type = $1;
    	$method = $2;
    	
    	$type =~ s/\s*$//;
    	
    	
    	$method =~ s/ *$//;
    }
     
    my $seq = $inseq->seq();
     
	#print "display_id: $display_id.\n type: $type.\n method: $method.\n desc: $desc.\n";
	
	$protein_hash {$display_id} = $seq;
	$type_hash {$display_id} = $type;
	$method_hash {$display_id} = $method;
	

}

#my $masked_file = $protdir . "masked.fasta";

#my $masked_in = Bio::SeqIO->new( '-format' => "FASTA", '-file' => "$masked_file" );
#while (my $inseq = $masked_in->next_seq) {

#    my $display_id = $inseq->display_id(); #The cluster name, but with C replaced by P, and with a suffix
    
#    $display_id =~ s/_.*$//;  #get rid of the suffix
#    $display_id =~ s/^...//;  #get rid of the lettering since it has an uninformative P in the third position
    
#    my $seq = $inseq->seq();
	
#	$masked_hash {$display_id} = $seq;

#}


#Now loop through the stored info and stick it into the database
foreach my $cluster (sort keys %nucleotide_hash){
	my $cluster_num = $cluster;
	$cluster_num =~ s/^...//;
	
	print "Adding cluster $cluster (short name $cluster_num) to the database\n";
	
	my $nucleotide = $nucleotide_hash {$cluster};
	my $quality = $quality_hash {$cluster};
	my $protein = $protein_hash {$cluster_num};
	#my $masked = $masked_hash {$cluster_num};
	my $type = $type_hash {$cluster_num};
	my $method = $method_hash {$cluster_num};
	my $raw_blast = $blast_hash {$cluster};
	
	if (length($protein) < 1){
		print "Skipping $cluster; the protein is of 0 length.\n";
		next;
	}
	
	$raw_blast =~ s/\'/prime/g;
	
	print "  $nucleotide\n";
	print "  $quality\n";
	print "  $protein\n";
	#print "  $masked\n";
	print "  $type\n";
	print "  $method\n";
	print "  top blast hit: $raw_blast\n";
	
	
	
	
	#Check to see if there is a stop codon
	my $query_seq = Bio::Seq->new( -display_id => "protein_translation", -seq => $protein);
	my $subject_seq = Bio::Seq->new( -display_id => "raw_nucleotide", -seq => $nucleotide);
	my $stop_codon_present = Protol::check_for_stop_codon($query_seq, $subject_seq);
    
    print "  Stop codon present: $stop_codon_present\n";
    
    my $sql = "insert into clusters set name = \'$cluster\', taxon = \'$species\', homologene_id = \'\', nucleotide = \'$nucleotide\', quality = \'$quality\', protein = \'$protein\', translation_method = \'$method\', type = \'$type\', stop_codon= \'$stop_codon_present\', raw_blast = \'$raw_blast\', updated = \'$datetime\'";
 	print "$sql\n";
 	my $statement = $dbh->prepare($sql)
	    or die "Couldn't prepare query '$sql': $DBI::errstr\n";
	$statement->execute()
    	or die "Couldn't execute query '$sql': $DBI::errstr\n";
}