package	Protol;

use strict;
use DBI;
use Bio::SeqIO;
use Bio::Seq;
use Bio::SearchIO;
use Bio::TreeIO;
use Bio::Tools::Run::StandAloneBlast;
require Exporter;
our @ISA = qw("Exporter");


use strict;
use DBI;

#our @EXPORT = qw(get_valid_groups,testprint);

#my $dbh_p = open_dbi("protol_06Dec07");
#my $dbh_h = open_dbi("homologene_25Jun07");

my $dbh_p = open_dbi("protol");
my $dbh_h = open_dbi("homologene");

sub testprint(){
	print "Looks good.\n";
}

sub mean{
	my @array = @_;
	my $n = @array;
	my $sum =0;
	foreach my $element (@array){
		$sum += $element;
	}
	my $mean = $sum/$n;
	return $mean;
}

sub median{
	my @array = @_;
	@array = sort {$a <=> $b} (@array);
	my $length = @array;
	my $median;
	
	if (($length % 2) > 0){
		#odd number of elements
		my $midpoint = ($length-1)/2;
		$median = $array[$midpoint];
	}
	else{
		#even number of elements
		my $midpoint = ($length)/2;
		$median = ($array[$midpoint]+$array[$midpoint-1])/2;
	}
	#print "\n@array\n";
	return $median;
}

sub get_taxon_status{
	my $taxon = shift;
	my $clade = shift;
	my $sql = "select $clade from taxa where name=\'$taxon\'";
	my $statement = $dbh_p->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
	$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
	my $n;
	my $status = -1;
	while (my @row = $statement->fetchrow_array()) {
		$status = $row[0];
		$n++;
	}
	if ($n != 1){
		die "Taxon $taxon has $n entries in table taxa\n"
	}
	
	return $status;
}

sub get_full_cluster_hash{
	
	# Returns a hash of cluster names with values corresponding to current homologene group


	# Build a hash of clusters to consider
	my %clusters;


	my $sql= "select name, homologene_id from clusters";
	my $statement = $dbh_p->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
	$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
	while (my @row = $statement->fetchrow_array()) {
		my $name = $row[0];
		my $hid = $row[1];
	
		$clusters{$name} = $hid;
	
		#print "$name: $hid\n";
	}

	# Update hid where necessary
	my $sql= "select name, homologene_id, masked from modifications";
	my $statement = $dbh_p->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
	$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
	while (my @row = $statement->fetchrow_array()) {
		my $name = $row[0];
		my $hid = $row[1];
		my $masked = $row[2];
	
		if (exists $clusters{$name}){
			if ($hid > -1){
				$clusters{$name} = $hid;
			}
			if ($masked > 0){
				delete $clusters{$name};
			}
		}
		

	}
	return %clusters;
}

sub get_group_hash{
	# Returns a hash of groups containing an array of sequences in each group
	my %clusters = get_full_cluster_hash();
	my %groups;
	
	foreach my $name (keys %clusters) {
    	my $hid = $clusters{$name};
    	if ($hid > 0){
    		push( @{$groups{$hid}}, $name );
    	}
	}
	
	return %groups;
}


sub write_groups_to_fasta{
	# Takes a reference to a hash with key HID and value array of sequence names
	print "write_groups_to_fasta called\n";
	my $href = shift;
	
	my %ghash = %{$href};
	
	
	
	foreach my $group (keys %ghash) {
		print "writing $group to fasta...\n";
		my $name_string = "HG$group";
		my $file_name_string = $name_string . ".fasta";
		#my @seq_names = @{$%$href{$group}};
		my @seq_names = @{$ghash{$group}};
		
		my $groupfasta = Bio::SeqIO->new(-file => ">$file_name_string", -format => 'FASTA');
		
		foreach my $seq_name (@seq_names){
			my $headertype = 0;
			my $seqobj = get_seq_obj ($seq_name, $headertype);
			$groupfasta->write_seq($seqobj);
		}
	}

}

sub is_masked{
	my $name = shift;
	my $masked = 0;
	
	if ($name =~ m/^p(\d+)/){
		my $id = $1;
		my $sql = "select taxon, name from clusters where cluster_id = \'$id\'";
		my $statement = $dbh_p->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
		$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
		my $new_name;
		my $n;
		while (my @row = $statement->fetchrow_array()) {
			#$taxon_name = $row[0];
			$new_name = $row[1];
			#print "fixed_name: $fixed_name, header: $header\n";
			$n++;
		}
		$name = $new_name;
	}
	elsif ($name =~ m/^h(\d+)/) {
		my $id = $1;
		my $sql = "select taxon, protein_gi from homologene where homologene_key_id=\'$id\'";
		my $statement = $dbh_h->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
		$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
		my $n;
		my $new_name;
		while (my @row = $statement->fetchrow_array()) {
			#$taxon_name = $row[0];
			$new_name = $row[1];
			#print "fixed_name: $fixed_name, header: $header\n";
			$n++;
		}
		$name = "GI".$new_name;
	}
	
	
	

	
	if ($name =~ m/^GI(\d+)/){
		my $gi = $1;
		my $sql= "select masked from homologene where protein_gi=\'$gi\'";
		my $statement = $dbh_h->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
		$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
		my @row = $statement->fetchrow_array();
		if ($row[0] > 0){
			$masked = 1;
		}
	}
	else{
		my $sql = "select masked from modifications where name=\'$name\'";
		my $statement = $dbh_p->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
		$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
		my $num_entries = $statement->rows;
		my @row = $statement->fetchrow_array();	
		if ($row[0] > 0){
			$masked = 1;
		}
	}
	
	return $masked;
}

sub get_tribe_from_seq{
	#Returns an array of tribes for which a given sequence has significant hits
	my $seqobj = shift;
	my $blastdb = "protol";
	my $e = "1e-20";
	
	my @params = ('d' => "$blastdb" , 'program' => "blastp" , 'e' => "$e" );
	
	my $factory = Bio::Tools::Run::StandAloneBlast->new(@params);
	my $blast_report = $factory->blastall($seqobj);
	$blast_report = $blast_report->next_result;
	
	my $ebest = 1;
	my $genebest = "";
	my %tribes;
	while (my $hit = $blast_report->next_hit){
		my $raw_name = $hit->name;
		my @fields = split / /, $raw_name;
		my $seq_name = $fields[0];
		#print "  hit:$seq_name\n";
		my $sql = "select mcl_group from mclmap where seq_name=\'$seq_name\'";
		my $statement = $dbh_p->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
		$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
		my $num_entries = $statement->rows;
		#print "    $num_entries entries\n";
		my @row = $statement->fetchrow_array();
		my $tribe = $row[0];
		if (length($tribe)<1){next;} #Some records in the blast database may not have been present at the time of clustering
		$tribes{$tribe}++;
		
	}
	
	return keys(%tribes);
}


sub gblock{
	my $infile = shift;
	my $executable = "Gblocks ";
	my $paramlist;
	
	$paramlist .= " $infile";
	$paramlist .= " -b5=a"; #Allowed gap positions all
	my $call = "$executable $paramlist";
	print "$call\n";
	`$call`;
}


sub is_excluded{
	my $taxon = shift;
	#my @list = ("Myzostoma symourocollegiorum", "Acropora palmata","Aiptasia pulchella", "Anopheles gambiae", "Canis familiaris", "Eisenia andrei", "Haliotis discus", "Hemicentrotus pulcherrimus", "Hydra vulgaris", "Julida sp", "Montastraea faveolata", "Neochildia fusca", "Podocoryne carnea", "Symsagittifera roscofensis");
	#my @list = ("Caenorhabditis elegans", "Monosiga brevicollis", "Acropora palmata","Aiptasia pulchella", "Anopheles gambiae", "Canis familiaris", "Eisenia andrei", "Haliotis discus", "Hemicentrotus pulcherrimus", "Hydra vulgaris", "Julida sp", "Montastraea faveolata", "Podocoryne carnea"); #Commented out on 7Feb08
	
	#my @list = ("Caenorhabditis elegans", "Acropora palmata","Aiptasia pulchella", "Anopheles gambiae", "Canis familiaris", "Eisenia andrei", "Haliotis discus", "Hemicentrotus pulcherrimus", "Hydra vulgaris", "Julida sp", "Montastraea faveolata", "Podocoryne carnea", "Daphnia magna", "Laevipilina hyalina", "Folsomia candida"); #commented out 26Jun08
	
	my @list = ("Caenorhabditis elegans", "Acropora palmata","Aiptasia pulchella", "Anopheles gambiae", "Canis familiaris", "Eisenia andrei", "Haliotis discus", "Hemicentrotus pulcherrimus", "Hydra vulgaris", "Julida sp", "Montastraea faveolata", "Podocoryne carnea", "Daphnia magna", "Folsomia candida");

	
	foreach my $element (@list){
		if ($taxon eq $element) { return 1; }
	}
	
	return 0;
}



sub tribe_length_stats{
	#Takes an array of tribe names and write out some data for each sequence
	my @tribes = @_;
	print "tribe\tseq_name\tlength\tstop_codon\tsource\ttaxon\n";
	foreach my $tribe (@tribes){

		
		my $sql= "select seq_name from mclmap where mcl_group=\'$tribe\'";
		my $statement = $dbh_p->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
		$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
		
		while (my @row = $statement->fetchrow_array()) {
			my $seq_name = $row[0];
			my $taxon = get_taxon($seq_name);

			# check to see of the taxon is excluded
			my $excluded = Protol::is_excluded ($taxon);
			if ($excluded > 0){next;}
			
			my $masked = is_masked($seq_name);
			if ($masked > 0){
				next;
			}
			
			my $seqobj = get_seq_obj ($seq_name, 1);
			
			my $length = $seqobj->length;
			if ($length < 1){ next;}
			
			my $taxon = $seqobj->display_id;
			
			my $sql= "select stop_codon, source from clusters where name=\'$seq_name\'";
			my $statement = $dbh_p->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
			$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
			my @seqrow = $statement->fetchrow_array();
			my $stop_codon = $seqrow[0];
			my $source = $seqrow[1];
			
			if ($seq_name =~ m/^GI\d/){
				$source = 4;
				$stop_codon = 1;
				
			}
			
			print "$tribe\t$seq_name\t$length\t$stop_codon\t$source\t$taxon\n";
		}
		
	}
}

sub fix_name{
	#Due to typos and name changes, taxon names sometimes have to be fixed for output;
	my $name = shift;
	my $reverse = shift;
	my $has_underscore=0;
	if ($name =~ m/_/) {
		$has_underscore=1;
	}
	else{
		$name =~ s/ /_/g;
	}
	
	my %lookup = (
		Xenoturbella_blocki => 'Xenoturbella_bocki',
		Turbanella_cornuta => 'Turbanella_ambronensis',
		Saccoglossus_kowalesvskii => 'Saccoglossus_kowalevskii',
		Myzostoma_symourocollegiorum => 'Myzostoma_seymourcollegiorum',
		Symsagittifera_roscofensis => 'Symsagittifera_roscoffensis',
		Flacisagitta_enflata => 'Flaccisagitta_enflata',
		Chaetopterus_variopedatus => 'Chaetopterus_sp',
		Paraplanocera_oligoglena => 'Paraplanocera_sp',
		Convoluta_tribola => 'Convolutriloba_longifissura',
	);
	
	if ($reverse < 1){
		if (exists $lookup{$name}){
			$name = $lookup{$name};
		}
	}
	else{
		my %revlookup;
		foreach my $key (keys %lookup){
			$revlookup{$lookup{$key}} = $key;
		}
		if (exists $revlookup{$name}){
			$name = $revlookup{$name};
		}
		
	}
	
	if ($has_underscore<1){
		$name =~ s/_/ /g;
	}
	
	return $name;
}

sub write_tribes_to_fasta_old{
	#Takes an array of tribe names (from mcl clustering) and writes them to a fasta
	# $filter ==0, write all groups
	# $filter ==1, write only groups where each taxon has only one cluster
	# $filter ==2, write only groups where at least one taxon has more than one cluster
	
	my $filter = shift;
	my @tribes = @_;
	
	my $headertype = 0;
	if ($filter == 1){
		$headertype = 1;
	}
	
	my %alltaxa;	# A hash of all the taxa encountered accross all the groups
	my %groups;		# A hash with key tribe of hashes containing all the taxa with sequences for a particular tribe
	
	foreach my $tribe (@tribes){

		my @seqnames;
		my $sql= "select seq_name from mclmap where mcl_group=\'$tribe\'";
		my $statement = $dbh_p->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
		$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
		my %taxa;
		while (my @row = $statement->fetchrow_array()) {
			my $seq_name = $row[0];
			
			
			
			# Check masking
			my $masked = is_masked($seq_name);
			if ($masked < 1){
				# Add unmasked taxa to the hash so it is possible to evaluate whether the cluster is good or bad
				my $taxon = get_taxon($seq_name);
				
				# check to see of the taxon is excluded
				my $excluded = Protol::is_excluded ($taxon);
				if ($excluded > 0){next;}
				
				
				$taxa{$taxon} ++;
			}
			# elsif ($filter < 2) { #CWD 06Nov07
			elsif ($filter == 1) {
				# If the sequence is masked, stop here unless writing bad clusters (in which you want masked and unmasked sequences)
				next;
			}
			push @seqnames, $seq_name;

		}
		
		my $multiple_flag = 0;

		foreach my $taxon (keys(%taxa)){
			if ($taxa{$taxon} > 1){
				$multiple_flag = 1;
			}
		}
		
		if (($multiple_flag>0)and($filter == 1)){next;}
		if (($multiple_flag==0)and($filter == 2)){next;}


		foreach my $taxon (keys(%taxa)){
			$alltaxa{$taxon} = 1;
			$groups{$tribe}{$taxon} = 1;
		}
		
		
		my $name_string = "Tribe$tribe";
		my $file_name_string = $name_string . ".fasta";
		my $groupfasta = Bio::SeqIO->new(-file => ">$file_name_string", -format => 'FASTA');
		
		foreach my $name (@seqnames){
			my $seqobj = get_seq_obj ($name, $headertype);
			
			my $length = $seqobj->length;
			if ($length < 1){ next;}
			
			my $display_id = $seqobj->display_id;
			#my $header = "$display_id,Tribe$tribe"; #Casey 31Jul08
			my $header = "$display_id";
			$seqobj->display_id($header);
			$groupfasta->write_seq($seqobj);
		}
		
		
		
	}
	return;
	print "group\tdescription\thomologene\tphilippe\trokas";
	my @alltaxa_array = sort keys(%alltaxa);
	
	foreach my $taxon (@alltaxa_array){
		print "\t$taxon";
	}
	print "\n";
	
	my @group_array = keys(%groups);
	@group_array = sort {$a <=> $b} (@group_array);
	
	foreach my $group (@group_array){
		print "$group";
		
		my $sql= "select distinct HID from mclmap where mcl_group=\'$group\'";
		my $statement = $dbh_p->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
		$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
		my $hid_string = "";
		my $description = "";
		while (my @row = $statement->fetchrow_array()) {
			my $HID = $row[0];
			if ($HID < 0) {next};
			if (length($description) < 1){
				$description = get_hid_description($HID);
			}
			my $h = "$HID";
			#$h =~ s/,//g; #Get rid of commas that are inserted when integer is converted to string
			$hid_string .= "$h, ";
		}
		
		

		
		print "\t$description";
		chop $hid_string;
		chop $hid_string;
		print "\t$hid_string";
		
		my $pname = get_philippe_from_tribe($group, "philippe");
		
		print "\t$pname";
		
		my $rname = get_philippe_from_tribe($group, "rokas");
		
		print "\t$rname";
		
		foreach my $taxon (@alltaxa_array){
			if ( $groups{$group}{$taxon} > 0){
				print"\t1";
			}
			else {
				print"\t0";
			}
		}
		print"\n";
	}
}


sub write_tribes_to_fasta{
	#Takes an array of tribe names (from mcl clustering) and writes them to a fasta
	# $filter ==0, write all groups
	# $filter ==1, write only groups where each taxon has only one cluster
	# $filter ==2, write only groups where at least one taxon has more than one cluster
	
	my $filter = shift;
	my @tribes = @_;
	
	my $headertype = 0;
	if ($filter == 1){
		$headertype = 1;
	}
	
	#Code added on 26Jun08 to allow for writing matrices of a particular clade
	my %taxsubset;	#Hash of all taxa to be in analysis
	my $clade="Cnidarian";

	#Grab the protol taxa from the database
	my $sql = "select name, $clade from taxa where $clade>'0'";
	#my $sql = "select name, $clade from taxa where $clade='1'";
	my $statement = $dbh_p->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
	$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
	while (my @row = $statement->fetchrow_array()) {
		my $taxon= $row[0];
		$taxon =~s/_/ /g;
		#if ($taxon eq "" or $taxon eq ""){ next;};
		my $status = $row[1];
		$taxsubset{$taxon}=$status;
	}
	
	#Add the homologene taxa to write one by one
	$taxsubset{"Drosophila melanogaster"}=3;

	foreach my $key (keys %taxsubset){
		print "$key $taxsubset{$key}\n";
	}

	
	my %alltaxa;	# A hash of all the taxa encountered accross all the groups
	my %groups;		# A hash with key tribe of hashes containing all the taxa with sequences for a particular tribe
	
	foreach my $tribe (@tribes){

		my @seqnames;
		my $sql= "select seq_name from mclmap where mcl_group=\'$tribe\'";
		my $statement = $dbh_p->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
		$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
		my %taxa;
		while (my @row = $statement->fetchrow_array()) {
			my $seq_name = $row[0];
			
			
			
			# Check masking
			#my $masked = is_masked($seq_name);
			my $masked = 0;
			if ($masked < 1){
				# Add unmasked taxa to the hash so it is possible to evaluate whether the cluster is good or bad
				my $taxon = get_taxon($seq_name);
				
				# check to see of the taxon is excluded
				my $excluded = Protol::is_excluded ($taxon);
				if ($excluded > 0){next;}
				
				if ($taxsubset{$taxon} < 1){next;}
				
				$taxa{$taxon} ++;
			}
			# elsif ($filter < 2) { #CWD 06Nov07
			elsif ($filter == 1) {
				# If the sequence is masked, stop here unless writing bad clusters (in which you want masked and unmasked sequences)
				next;
			}
			push @seqnames, $seq_name;

		}
		
		my $multiple_flag = 0;

		foreach my $taxon (keys(%taxa)){
			if ($taxa{$taxon} > 1){
				$multiple_flag = 1;
			}
		}
		
		
		if (($multiple_flag>0)and($filter == 1)){next;}
		if (($multiple_flag==0)and($filter == 2)){next;}
		
		#Loop and evaluation added 26Jun08 to apply ingroup and total taxon sampling criteria
		my $n_ingroup = 0;
		my $n_taxa = 0;
		foreach my $taxon (keys(%taxa)){
			$n_taxa ++;
			if ($taxsubset{$taxon} == 1){
				$n_ingroup ++;
			}
		}
		if ($n_taxa < 4){
			print "Skipping $tribe because it has $n_taxa taxa\n";
			next;
		}
		if ($n_ingroup < 3){
			print "Skipping $tribe because it has $n_ingroup ingroup taxa\n";
			next;
		}


		foreach my $taxon (keys(%taxa)){
			$alltaxa{$taxon} = 1;
			$groups{$tribe}{$taxon} = 1;
		}
		
		
		my $name_string = "Tribe$tribe";
		my $file_name_string = $name_string . ".fasta";
		my $groupfasta = Bio::SeqIO->new(-file => ">$file_name_string", -format => 'FASTA');
		
		foreach my $name (@seqnames){
			my $seqobj = get_seq_obj ($name, $headertype);
			
			my $length = $seqobj->length;
			if ($length < 1){ next;}
			
			my $display_id = $seqobj->display_id;
			#my $header = "$display_id,Tribe$tribe";
			my $header = "$display_id";
			$seqobj->display_id($header);
			$groupfasta->write_seq($seqobj);
		}
		
		
		
	}
	return;
	print "group\tdescription\thomologene\tphilippe\trokas";
	my @alltaxa_array = sort keys(%alltaxa);
	
	foreach my $taxon (@alltaxa_array){
		print "\t$taxon";
	}
	print "\n";
	
	my @group_array = keys(%groups);
	@group_array = sort {$a <=> $b} (@group_array);
	
	foreach my $group (@group_array){
		print "$group";
		
		my $sql= "select distinct HID from mclmap where mcl_group=\'$group\'";
		my $statement = $dbh_p->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
		$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
		my $hid_string = "";
		my $description = "";
		while (my @row = $statement->fetchrow_array()) {
			my $HID = $row[0];
			if ($HID < 0) {next};
			if (length($description) < 1){
				$description = get_hid_description($HID);
			}
			my $h = "$HID";
			#$h =~ s/,//g; #Get rid of commas that are inserted when integer is converted to string
			$hid_string .= "$h, ";
		}
		
		

		
		print "\t$description";
		chop $hid_string;
		chop $hid_string;
		print "\t$hid_string";
		
		my $pname = get_philippe_from_tribe($group, "philippe");
		
		print "\t$pname";
		
		my $rname = get_philippe_from_tribe($group, "rokas");
		
		print "\t$rname";
		
		foreach my $taxon (@alltaxa_array){
			if ( $groups{$group}{$taxon} > 0){
				print"\t1";
			}
			else {
				print"\t0";
			}
		}
		print"\n";
	}
}

sub get_hid_description{
	my $HID = shift;
	my $sql = "select description from descriptions where HID = \'$HID\'";
	my $statement = $dbh_p->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
	$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
	my @row = $statement->fetchrow_array();
	my $description = $row[0];
	
	$description =~ s/PREDICTED: //;
	$description =~ s/similar to //;
	return $description;
}

sub get_philippe_from_tribe{
	# checks to see if a tribe is similar to one of philippes genes and, if so, returns the name of the gene
	my $tribe = shift;
	my $blastdb = shift;
	my $e = "1e-20";
	#my $blastdb = "philippe";
	my @seqnames;
	my $sql= "select seq_name from mclmap where mcl_group=\'$tribe\'";
	my $statement = $dbh_p->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
	$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
	while (my @row = $statement->fetchrow_array()) {
		my $seq_name = $row[0];
		my $masked = is_masked($seq_name);
		if ($masked > 0){next;}
		push @seqnames, $seq_name;
	}
	
	my %results;
	foreach my $seqname (@seqnames){
		my $seqobj = get_seq_obj ($seqname, 0);
		my @params = ('d' => "$blastdb" , 'program' => "blastp" , 'e' => "$e" );
	
		my $factory = Bio::Tools::Run::StandAloneBlast->new(@params);
		my $blast_report = $factory->blastall($seqobj);
		$blast_report = $blast_report->next_result;
	
		my $ebest = 1;
		my $genebest = "";
		while (my $hit = $blast_report->next_hit){
			my $raw_name = $hit->name;
			my @fields = split /,/, $raw_name;
			my $gene_name = $fields[1];
			my $hitEvalue = $hit->significance;
			if ($hitEvalue < $ebest){
				$ebest = $hitEvalue;
				$genebest = $gene_name;
			}
		}
		if ($ebest < 1){
			#print "\n$seqname of $tribe hit $genebest with evalue $ebest\n";
			if (exists  $results{$genebest}){
				if ($ebest < $results{$genebest}){
					$results{$genebest} = $ebest;
				}
			}
			else{
				$results{$genebest} = $ebest;
			}
		}
	}
	
	my $philippename = "";
	my @keys = keys %results;
	foreach my $key (@keys){
		$philippename .= "$key, ";
	}
	chop $philippename;
	chop $philippename;
	return $philippename;
}




sub write_focal_fastas{
	# Writes fasta files for each focal group returned by get_focal_hashes
	# Use filter as in get_focal_hashes, using taxon names when filter==1 and sequence ids where filter == 2
	
	my $filter = shift;
	
	my %focal_groups = get_focal_groups ($filter);
	
	foreach my $group (keys %focal_groups) {
		my $name_string = "HG$group";
		my $file_name_string = $name_string . ".fasta";
		my @seq_names = @{$focal_groups{$group}};
		
		my $groupfasta = Bio::SeqIO->new(-file => ">$file_name_string", -format => 'FASTA');
		
		foreach my $seq_name (@seq_names){
			my $headertype = 0;
			if ($filter == 1){
				$headertype = 1;
			}
			my $seqobj = get_seq_obj ($seq_name, $headertype);
			$groupfasta->write_seq($seqobj);
		}
	}
}

sub summarize_groups{
	my %groups = get_group_hash();
	foreach my $group (keys %groups) {
		my @clusters = @{$groups{$group}};
		if (@clusters < 4){
			next;
		}
		
		my $sql = "select description from descriptions where HID = \'$group\'";
		my $statement = $dbh_p->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
		$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
		my @row = $statement->fetchrow_array();
		my $description = $row[0];
		my %counts = get_count_by_species (@clusters); 
		my $nspecies = keys(%counts);
		my $ncnidaria = 0;
		my $necdys = 0;
		my $nlopho = 0;
		my $noriginal = 0;
		foreach my $taxon (keys %counts) {
			my $sql = "select source, Lophotrochozoan, Ecdysozoan, Cnidarian from taxa where name = \'$taxon\'";
			my $statement = $dbh_p->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
			$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
			my @row = $statement->fetchrow_array();
			if ($row[0] == 0) {$noriginal++;}
			if ($row[1] > 0) {$nlopho++;}
			if ($row[2] > 0) {$necdys++;}
			if ($row[3] > 0) {$ncnidaria++;}
		}
		
		print "$description\t$group\t$nspecies\t$noriginal\t$nlopho\t$necdys\t$ncnidaria\n";
	}

}


sub get_focal_groups{
	# returns a hash of groups containing sequences from at least $focal_min focal taxa, including sequences from the actual homologene dataset
	
	# $filter ==0, return all focal groups that meet above criteria
	# $filter ==1, return only focal groups where each species has only one cluster
	# $filter ==2, return only focal groups where at least one species has more than one cluster
	my $filter = shift;
	
	
	print "Getting focal groups from protol...\n";
	my %protol_focal_groups = get_protol_focal_groups(0);
	print "Adding data from homologene taxa...\n";
	
	my %focal_groups;
	

	foreach my $group (keys %protol_focal_groups) {
		my @pclusters = @{$protol_focal_groups{$group}};
		
		my $sql= "select homologene_key_id, taxon from homologene where HID=\'$group\' and masked < '1' and length(sequence)>'0'";
		my $statement = $dbh_h->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
		$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
		my $n = 0;
		my %hcounts;
		my @hgheyids;
		while (my @row = $statement->fetchrow_array()) {
			my $hgkeyid = $row[0];
			my $taxon = $row[1];
			$hcounts{$taxon} ++;
			
			my $hgkeyidstring = "h$hgkeyid";
			push @hgheyids, $hgkeyidstring;
		}
		
		my %pcounts = get_count_by_species (@pclusters);
		
		if ( $filter > 0 ){
			my $pass = 1;
			foreach my $taxon (keys %hcounts) {
				my $count = $hcounts{$taxon};
				if ( $count > 1 ){
					$pass = 0;
					print "  Problem: Group $group, taxon $taxon has $count sequences\n";
				}
			}
			
			if ($pass > 0){
				# Passed accoring to homologene, but still have to check protol
				my %pcounts = get_count_by_species (@pclusters);
				foreach my $taxon (keys %pcounts) {
					my $count = $pcounts{$taxon};
					if ( $count > 1 ){
						$pass = 0;
						print "  Problem: Group $group, taxon $taxon has $count sequences\n";
					}
				}
				
			}
			
			if ( ($filter == 1) and ($pass == 0) ){
				next;
			}
			
			if ( ($filter == 2) and ($pass == 1) ){
				next;
			}
			
		}
		my @clusters_combined = (@pclusters, @hgheyids);
		
		@{$focal_groups{$group}} = (@clusters_combined);
		
		#Write a fasta file
		#my %onegroup;
		#@{$onegroup{$group}} = (@clusters);
		#write_groups_to_fasta(\%onegroup);
	}
	
	return %focal_groups;
}

sub get_focal_groups_old{
	# returns a hash of groups containing sequences from at least $focal_min focal taxa, including sequences from the actual homologene dataset
	
	# $filter ==0, return all focal groups that meet above criteria
	# $filter ==1, return only focal groups where each species has only one cluster
	# $filter ==2, return only focal groups where at least one species has more than one cluster
	my $filter = shift;
	
	
	print "Getting focal groups from protol...\n";
	my %protol_focal_groups = get_protol_focal_groups($filter);
	print "Adding data from homologene taxa...\n";
	
	my %focal_groups;
	
	
	foreach my $group (keys %protol_focal_groups) {
		my @clusters = @{$protol_focal_groups{$group}};
		
		my $sql= "select homologene_key_id, taxon from homologene where HID=\'$group\' and masked < '1' and length(sequence)>'0'";
		my $statement = $dbh_h->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
		$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
		my $n = 0;
		my %counts;
		my @hgheyids;
		while (my @row = $statement->fetchrow_array()) {
			my $hgkeyid = $row[0];
			my $taxon = $row[1];
			$counts{$taxon} ++;
			
			my $hgkeyidstring = "h$hgkeyid";
			push @hgheyids, $hgkeyidstring;
		}
		
		if ( $filter > 0 ){
			my $pass = 1;
			foreach my $taxon (keys %counts) {
				my $count = $counts{$taxon};
				if ( $count > 1 ){
					$pass = 0;
					print "  Problem: Group $group, taxon $taxon has $count sequences\n";
				}
			}
			
			# Don't pass on the group if it was good coming from protol and bad with respect to homologene and filter == 1
			if ( ($filter == 1) and ($pass == 0) ){
				next;
			}
			
			# If filter==2 the group is still bad even if homologene is good because protol sent on only bad sequences
			#if ( ($filter == 2) and ($pass == 1) ){
			#	next;
			#}
		}
		
		@clusters = (@clusters, @hgheyids);
		
		@{$focal_groups{$group}} = (@clusters);
		
		#Write a fasta file
		#my %onegroup;
		#@{$onegroup{$group}} = (@clusters);
		#write_groups_to_fasta(\%onegroup);
	}
	
	return %focal_groups;
}

sub is_taxon_focal{
	# A sub that returns 1 if a taxon is focal, 0 otherwise
	# different criteria can be used at different times
	# ONLY LOOKS AT PROTOL TAXA FOR NOW

	my $taxon = shift;
	my $sql= "select source from taxa where name=\'$taxon\'";
	my $statement = $dbh_p->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
	$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
	my $n = 0;
	my $value = 1;
	while (my @row = $statement->fetchrow_array()) {
		my $n++;
		$value = $row[0];
	}
	
	
	if ($value > 0){
		return 0;
	}
	else{
		return 1;
	}
}

sub get_protol_focal_groups{
	# returns a hash of groups containing sequences from at least $focal_min focal taxa
	
	# $filter ==0, return all focal groups that meet above criteria
	# $filter ==1, return only focal groups where each species has only one cluster
	# $filter ==2, return only focal groups where at least one species has more than one cluster
	
	my $filter = shift;
	
	my $focal_min = 3;
	my $total_min = 10;	#skip clusters if they have less than this many tax in total
	
	my %groups = get_group_hash();
	my %focal_groups;
	foreach my $group (keys %groups) {
		my @clusters = @{$groups{$group}};
		

		
		if (@clusters < $focal_min){
			#Cut to the chase if there aren't even enough clusters to reach the min
			next;
		}
		
		my %counts = get_count_by_species (@clusters);
		
		my $ntaxa = keys(%counts);
		
		if ($ntaxa < $total_min){
			next;
		}
		
		my $n_focal_taxa = 0;
		foreach my $taxon (keys %counts) {
			#my $source = get_source_from_taxon($taxon);
			my $focal = is_taxon_focal($taxon);
			if ($focal > 0){
				$n_focal_taxa ++;
			}
		}
		
		if ( $n_focal_taxa < $focal_min ){
			next;
		}		
		
		
		
		
		if ( $filter > 0 ){
			my $pass = 1;
			foreach my $taxon (keys %counts) {
				my $count = $counts{$taxon};
				
				if ( $count > 1 ){
					$pass = 0;
					#print "  Problem: Group $group, taxon $taxon has $count sequences\n";
				}
			}
			
			#print "  pass $pass\n";
			
			if ( ($filter == 1) and ($pass == 0) ){
				next;
			}
			
			if ( ($filter == 2) and ($pass == 1) ){
				next;
			}
			
		}
		
		print "$group\t$n_focal_taxa\n";
		@{$focal_groups{$group}} = (@clusters);
		
	}
	
	return %focal_groups;
}


sub get_count_by_species{
	# Returns a hash with key $name and and a value with the number of clusters belonging to the respective species
	my @clusters = @_;
	my %counts;
	
	foreach my $name (@clusters) {
		my $taxon = get_taxon ($name);
		$counts{$taxon} ++;
	}
	return %counts;
}


sub get_source_from_name{
	my $name = shift;
	my $source = 0;
	


	my $sql= "select source from clusters where name=\'$name\'";
	my $statement = $dbh_p->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
	$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
	my $n = 0;
	while (my @row = $statement->fetchrow_array()) {
		$source = $row[0];
		$n++;
	}
	
	if ($n>1){
		die "$n entries for $name in clusters !!!\n";
	}
	elsif ($n==0){
		die "No entries for $name in clusters !!!\n";
	}
	
	return $source;

}

sub get_source_from_taxon{
	my $taxon = shift;
	my $source = 0;

	my $sql= "select source from taxa where name=\'$taxon\'";
	my $statement = $dbh_p->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
	$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
	my $n = 0;
	while (my @row = $statement->fetchrow_array()) {
		$source = $row[0];
		$n++;
	}
	
	if ($n>1){
		die "$n entries for $taxon in taxa !!!\n";
	}
	elsif ($n==0){
		die "No entries for $taxon in taxa !!!\n";
	}
	
	return $source;

}

sub get_taxon{
	my $name = shift;
	my $taxon = 0;

	if ($name =~ m/^GI(\d+)/){
		#Sequence is in homologene
		my $gi = $1;
		my $sql= "select taxon from homologene where protein_gi=\'$gi\'";
		my $statement = $dbh_h->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
		$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
		my $n = 0;
		while (my @row = $statement->fetchrow_array()) {
			$taxon = $row[0];
			$n++;
		}
		
		if ($n>1){
			die "$n entries for $name in clusters !!!\n";
		}
		elsif ($n==0){
			die "No entries for $name in clusters !!!\n";
		}
	}
	else{
		#Look for sequence in protol
		my $sql= "select taxon from clusters where name=\'$name\'";
		my $statement = $dbh_p->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
		$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
		my $n = 0;
		while (my @row = $statement->fetchrow_array()) {
			$taxon = $row[0];
			$n++;
		}
		
		if ($n>1){
			die "$n entries for $name in clusters !!!\n";
		}
		elsif ($n==0){
			die "No entries for $name in clusters !!!\n";
		}
	}
	return $taxon;

}


sub get_start_stop{
	# takes a protein translation and an original nucleotide sequence, aligns them, and gives the start and stop position of the cds with respect to the nucleotide seq
	my $stop_codon_present = 0;
	my $start = -1;
	my $stop = -1;
	my $blastfilename = "blastfile.out";
	my $query_seq = shift; #Protein
	my $subject_seq = shift; #Nucleotide
	my $factory = Bio::Tools::Run::StandAloneBlast->new( 'program'  => 'tblastn', 'outfile' => "$blastfilename");
	$factory->io->_io_cleanup();  #Workaround for BPbl2seq file cleanup bug
 	$factory->bl2seq($query_seq, $subject_seq);
 	
 	my $parser = new Bio::SearchIO(-format => 'blast',
                              -file => $blastfilename,
                              -report_type => 'tblastn');
 	

 	my $bl2seq_result = $parser->next_result;
 	my $hitcount= $bl2seq_result->num_hits;
 	
 	
 	
 	
 	if ($hitcount > 0){
 		my $hit = $bl2seq_result ->next_hit;
 		my $slength = $subject_seq -> length;
 		my $qlength = $query_seq -> length;
 		my $hmin;
		my $hmax;
		my $qmin;
		my $qmax;
		my $nhsps;
	
	 	while (my $hsp = $hit-> next_hsp){
		 		my $e = $hsp->evalue;
	 		if ($e > 0.0001){
	 			next;
	 		}
    
	 		my $qstart;
    		my $qend;
    		my $hstart;
    		my $hend;
    
    		$qstart = $hsp->query->start;
    		$qend = $hsp->query->end;
    		$hstart = $hsp->sbjct->start;
    		$hend = $hsp->sbjct->end;
    
    		if ($hend>$hmax) { $hmax = $hend; }
     		 	if ( $nhsps < 1 ){
     				$hmin = $hstart;
     			}
     			else {
     				if ($hstart<$hmin) { $hmin = $hstart; }
     			}
     	
     	
     			if ($qend>$qmax) { $qmax = $qend; }
     			if ( $nhsps < 1 ){
     				$qmin = $qstart;
     			}
     			else {
     				if ($qstart<$qmin) { $qmin = $qstart; }
     			}
     			$nhsps ++;
    
    	}
    
    
    
    	if ( $nhsps > 0 ) {
    	
    		if ($qmin > 1){
    			#The hit was not fully extended to the 5', need to fix this
    			my $nmissing = $qmin - 1;
    			my $newhmin = $hmin - ($nmissing*3);
    			if ($newhmin > 0){
    				$hmin = $newhmin;
    			}
    		}
    		
    		if ($qmax < $qlength){
    			#The hit was not fully extended to the 3', need to fix this
    			my $nmissing = $qlength - $qmax;
    			my $newhmax = $hmax + ($nmissing*3);
    			if ($newhmax <= $slength){
    				$hmax = $newhmax;
    			}
    		}
    		
    		#if (($slength-$hmax)<3){
    		#	#CDS goes to end, but the last codon is partial. Extend CDS to end of sequence.
    		#	$hmax = $slength;
    		#}
    		
    		#print "  aligned protein and nucleotide sequences...\n";
    		if ( ($hmax + 3) <= $slength){
    			my $next_codon = $subject_seq -> subseq ($hmax+1,$hmax+3);
    			#print "  next codon: $next_codon\n";
    			if ( ($next_codon eq 'TAA') | ($next_codon eq 'TAG') | ($next_codon eq 'TGA')){
    				$stop_codon_present = 1;
    			}
    		}
    		

    		
    		$start = $hmin;
    		$stop = $hmax;
    		

    		
    		#print "$qmin $qmax $hmin $hmax\n";
    		if ($stop_codon_present > 0){
    				$stop += 3;
    		}
    	}
    
   	}
   	my @result;
   	push @result, $start;
   	push @result, $stop;
   	push @result, $stop_codon_present;
	return @result;
}



sub check_for_stop_codon{
	# takes a protein translation and an original nucleotide sequence, aligns them, and looks at the next codon to see if it is a stop codon
	my $stop_codon_present = 0;
	my $blastfilename = "blastfile.out";
	my $query_seq = shift; #Protein
	my $subject_seq = shift; #Nucleotide
	my $factory = Bio::Tools::Run::StandAloneBlast->new( 'program'  => 'tblastn', 'outfile' => "$blastfilename");
	$factory->io->_io_cleanup();  #Workaround for BPbl2seq file cleanup bug
 	$factory->bl2seq($query_seq, $subject_seq);
 	
 	my $parser = new Bio::SearchIO(-format => 'blast',
                              -file => $blastfilename,
                              -report_type => 'tblastn');
 	

 	my $bl2seq_result = $parser->next_result;
 	my $hitcount= $bl2seq_result->num_hits;
 	
 	
 	
 	
 	if ($hitcount > 0){
 		my $hit = $bl2seq_result ->next_hit;
 		my $slength = $subject_seq -> length;
 		my $qlength = $query_seq -> length;
 		my $hmin;
		my $hmax;
		my $qmin;
		my $qmax;
		my $nhsps;
	
	 	while (my $hsp = $hit-> next_hsp){
		 		my $e = $hsp->evalue;
	 		if ($e > 0.0001){
	 			next;
	 		}
    
	 		my $qstart;
    		my $qend;
    		my $hstart;
    		my $hend;
    
    		$qstart = $hsp->query->start;
    		$qend = $hsp->query->end;
    		$hstart = $hsp->sbjct->start;
    		$hend = $hsp->sbjct->end;
    
    		if ($hend>$hmax) { $hmax = $hend; }
     		 	if ( $nhsps < 1 ){
     				$hmin = $hstart;
     			}
     			else {
     				if ($hstart<$hmin) { $hmin = $hstart; }
     			}
     	
     	
     			if ($qend>$qmax) { $qmax = $qend; }
     			if ( $nhsps < 1 ){
     				$qmin = $qstart;
     			}
     			else {
     				if ($qstart<$qmin) { $qmin = $qstart; }
     			}
     			$nhsps ++;
    
    	}
    
    
    
    	if ( $nhsps > 0 ) {
    		print "  aligned protein and nucleotide sequences...\n";
    		if ( ($hmax + 3) <= $slength){
    			my $next_codon = $subject_seq -> subseq ($hmax+1,$hmax+3);
    			#print "  next codon: $next_codon\n";
    			if ( ($next_codon eq 'TAA') | ($next_codon eq 'TAG') | ($next_codon eq 'TGA')){
    				$stop_codon_present = 1;
    			}
    		}
    	}
    
   	}
	return $stop_codon_present;
}



sub mask_whole_sequences{
	my @clusters = (@_);
	my $datetime = get_current_datetime();
	foreach my $name (@clusters) {
		if ($name =~ m/^h(\d+)/){
			# Get the sequence from homologene
			my $id = $1;

			my $sql = "update homologene set masked =\'1\', mask_comment = \'automated taxon monophyly mask, $datetime\' where homologene_key_id=\'$id\'";
			#print "$sql\n";
			my $statement = $dbh_h->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
			$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
		
		
		}
		elsif ($name =~ m/^p(\d+)/){
			# Get the sequence from protol
			my $id = $1;

			my $sql = "select name from clusters where cluster_id=\'$id\'";
			my $statement = $dbh_p->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
			$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
			my @row = $statement->fetchrow_array();
			my $name = $row[0];
			
			my $sql = "select comments from modifications where name=\'$name\'";
			my $statement = $dbh_p->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
			$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
			my $num_entries = $statement->rows;
			my @row = $statement->fetchrow_array();
			my $comment = $row[0];
			if (length $comment > 0){
				$comment .= ", ";
			}
			print "$num_entries previous modifications for $name\n";
			if ( $num_entries < 1 ){
				my $sql = "insert into modifications set name=\'$name\', homologene_id=\'-1\', updated=\'$datetime\', masked = \'1\', comments = \'automated taxon monophyly mask\'";
				#print "$sql\n";
				my $statement = $dbh_p->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
				$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
			}
			elsif ( $num_entries == 1){
				my $comment .= "automated taxon monophyly mask";
				my $sql = "update modifications set updated=\'$datetime\', masked = \'1\', comments = \'$comment\' where name=\'$name\'";
				#print "$sql\n";
				my $statement = $dbh_p->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
				$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
			}
			else{
				die "$num_entries entries for $name in the modifications table!!!\n";
			}
			
		}
		else{
			die "Cannot identify sequence $name!\n";
		}
	}
}



################################################################################
############Sequence masking based on promiscuous domains#######################



sub do_rps_blast{
	my $seqobject = shift;
	#show_seq_info ($seqobject);
	
	my $seq_file_name = "rpsblast.fasta";
	my $blast_file_name = "rpsblast.out";
	my $blastfasta = Bio::SeqIO->new(-file => ">$seq_file_name", -format => 'FASTA');
	$blastfasta->write_seq($seqobject);
	
	open (PROG, "|rpsblast -i $seq_file_name -p T -d Cdd &> $blast_file_name") or die;
	close (PROG) or die;
	
	my $searchio = new Bio::SearchIO(-format => 'blast', -file   => $blast_file_name);
	
	return $searchio;
	
	
}


sub update_masked_sequences{
	# Reads all sequences for a given species from the database, masked promiscuous domains, and writes the masked sequence to the propper filed in the database
	my $species = shift;
	my $e = 0.000001;

	
	# Remove old masks
	print "Deleting old masked proteins...\n";
	my $sql = "update clusters set masked_protein = \'\' where taxon=\'$species\' and length(masked_protein) > 0";
	my $statement = $dbh_p->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
	$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
	
	print "Getting sequences from db...\n";
	my $sql = "select name, protein from clusters where taxon=\'$species\'";
	my $statement = $dbh_p->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
	$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
	while (my @row = $statement->fetchrow_array()) {
		my $name = $row[0];
		print "Processing $name\n";
		my $seqstring = $row[1];
		my $seq = Bio::Seq->new( -display_id => $name, -seq => $seqstring);
		
		my $searchio = do_rps_blast($seq);
		my $result = $searchio->next_result;

		my $new_seq = $seq;

	    while( my $hit = $result->next_hit ) {
	    	my $hit_name = $hit->name();
	    	my $hit_desc = $hit->description();
	    	#print "  hit name: $hit_name, description: $hit_desc\n";
	    	if ( domain_of_interest ($hit_desc) > 0 ){
	    		#print "  of interest.\n";
 	   		#print "  hit name: $hit_name, description: $hit_desc\n";
    	    	while( my $hsp = $hit->next_hsp ) {
     	    	   my $start = $hsp->query->start;
     			   my $end = $hsp->query->end;
     		   		#print "    start: $start, end: $end\n";
     		   
     		   		# Mask the sequence
     		   		my $seqstring = $new_seq->seq;
     		   		my @seqarray = split //, $seqstring;
     		   
     		   		$start --; #convert from first position = 1 to first position = 2
     		   		$end --;
     		   		for (my $i = $start; $i <= $end; $i++) {
     		   			$seqarray[$i] = 'X';
     		   		} #end for
     		   
     		   		$seqstring = join "", @seqarray;
     		   		$new_seq->seq($seqstring);
     			} #end while
     		} #end if
    	} #end while

    
    	#$seq_out->write_seq($new_seq);
    	my $newseqstring = $new_seq -> seq;
    	my $sql= "update clusters set masked_protein=\'$newseqstring\' where name = \'$name\'";
    	#print "$sql\n";
    	my $statement = $dbh_p->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
		$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
	}
}



sub domain_of_interest {
	my $desc = shift;
	
	my @domains_to_mask = (
	"pfam01535",
	"pfam00400",
	"pfam00047",
	"smart00407",
	"cd00099",
	# ", IG_like,", #couldn't find a match in the cdd
	"pfam00076",
	"pfam00023",
	"pfam01576",
	"pfam00041",
	"cd00031",
	"smart00112",
	"cd00096",
	"cd00204",
	"pfam00023",
	"smart00248",
	"pfam01344",
	# ", OAD_kelch,", #couldn't find a match in the cdd
	"pfam00018",
	"pfam00038",
	"pfam00096",
	"pfam00595",
	"pfam00651",
	"pfam00169",
	"pfam00105",
	"pfam00435",
	"pfam00084",
	"pfam00017",
	"smart00225",
	"smart00367",
	"smart00135",
	"cd00020",
	"pfam00514",
	"cd00020",
	"smart00185",
	"cd00014",
	"pfam00307",
	"smart00033"
	);
	
	
	#print "  hit name: $hit_name, description: $desc\n";
	foreach my $domain (@domains_to_mask){
		if ($desc =~ m/^$domain,/){
			#print "      $domain\n"; 
			return 1;
		}
	}
	return 0;
}

################################################################################
############Tree manipulation###################################################

sub mask_monophyletic_taxa {
	# takes the name of a nexus tree file, and then checks to see if any given species has more than
	# one sequence. If so, it checks to see if they are monophyletic and masks them if they are.
	
	my $tree_file = shift;
	my $output = ""; #Text output
	
	my $bootstrap_cutoff = 80;

	
	my $treeio = Protol::get_nexus_tree($tree_file);
	my $tree = $treeio->next_tree;
	
	my @leaves =  $tree->get_leaf_nodes;
	
	#foreach my $leaf (@leaves){
	#	my $id = $leaf-> id;
	#	print "$id\n";
	#}
	
	# build a hash of arrays, where the key is taxon name and the array contains all nodes belonging to that taxon
	my %taxa;
		
	foreach my $leaf (@leaves){
		my $description = $leaf-> description;
		my $id = $leaf-> id;
		my $seqobj = get_seq_obj("$id", 1);
		my $taxon = $seqobj -> display_id;
		push( @{$taxa{$taxon}}, $leaf );
	}
	
	foreach my $taxon (keys %taxa) {
		my @taxon_leaves = @{$taxa{$taxon}};
		
		my $n_seqs = @taxon_leaves;
		
		if ($n_seqs > 1){
			print "$taxon has $n_seqs seqs\n";
			
			
			$output .= "$taxon has $n_seqs seqs\n";
			
			my $outgroup = get_node_from_different_taxon(\%taxa, $taxon);
			
			my @taxon_leaves_copy = (@taxon_leaves);
			
			if( $tree->is_monophyletic(-nodes    => \@taxon_leaves_copy, -outgroup => $outgroup) ){
				my @taxon_leaves_copy = (@taxon_leaves);

				
				my $lca = Protol::find_lca($tree, \@taxon_leaves_copy);
				my $bootstrap = $lca->branch_length;	#Paup stored the bootstrap value in the branchlength
				#print "  monophyletic with bootstrap: $bootstrap\n";
				$output .= "  monophyletic with bootstrap: $bootstrap\n";

				if ( $bootstrap >= $bootstrap_cutoff ){
					
					
					# check to see if the masking is already OK
					my $n_unmasked = 0;
					foreach my $leaf (@taxon_leaves){
						my $id = $leaf-> id;
						my $seqobj = get_seq_obj("$id", 1);
						print ("          $id\n");
						my $mask = Protol::is_masked($id);
						if ($mask < 1){
							$n_unmasked ++;
						}
					}
					if ($n_unmasked <= 1){
						$output .= "   masking already OK\n";
						next;
					}
					
					# Mask all but one of the sequences
					my @to_mask;	#array of sequencecs to mask
					my $found_flag = 0;	#Flag indicating that a sequence with a stop codon has been found and not masked
					
					foreach my $leaf (@taxon_leaves){
						my $id = $leaf-> id;
						#print "    $id\n";
						my $mask = Protol::is_masked($id);
						if ($mask > 0){
							#already masked, just ignore it
							next;
						}
						
						my $stop_status = 0;
						if ($id =~ m/^h/){
							#assume that all proteins in the homologene dataset are full length
							$stop_status =1;
						}
						elsif ($id =~ m/^p(\d+)/){
							my $cluster_id = $1;
							my $sql = "select stop_codon from clusters where cluster_id=\'$cluster_id\'";
							my $statement = $dbh_p->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
							$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
							my @row = $statement->fetchrow_array();
							$stop_status = $row[0];
						}
						#print "  $id has stop codon: $stop_status\n";
						if ( $stop_status > 0){
							if ( $found_flag < 1 ){
								$found_flag =1;
								next;
							}
							else{
								push @to_mask, $id;
							}
						}
						else{
							push @to_mask, $id;
						}
					}
					
					if ($found_flag < 1){
						# All of the nodes were added to the mask array, so remove one from the array
						shift @to_mask;
					}
					
					foreach my $seqid (@to_mask){
						$output .= "   masking $seqid\n";
						#print "   masking $seqid\n";
					}
					
					Protol::mask_whole_sequences(@to_mask);
					
				}
			}
		}
	}
	return $output;
} # end mask_monophyletic_taxa





sub get_node_from_different_taxon {
	#takes a refrence to a hash with keys taxa and containing arrays of terminal 
	#nodes belonging to the repective taxa. Also takes a taxon name, and picks a
	#node not belonging to that taxon. Useful for testing the monophyly of nodes
	#belonging to a taxon.
	
	my $href = shift; #reference to hash of node arrays
	my $current_taxon = shift; #the taxon you Don't want
	
	my %hash = (%$href);
	foreach my $taxon (keys %hash) {
		if ($taxon eq $current_taxon){
			next;
		}
		my @taxon_leaves = @{$hash{$taxon}};
		my $node = $taxon_leaves[0];
		return $node;
	}
	
} #end get_node_from_different_taxon

sub get_nexus_tree{
	# A work around for a bug in Bio::TreeIO::nexus where the last taxon is not parsed from the lookup table
	# unless a comma is added after it
	
	my $tree_file = shift;
	my $name_fixed_file = $tree_file . ".treeio";
	open (INFILE, "$tree_file") or die "No can do...";
	open (OUTFILE, ">$name_fixed_file") or die "No can do...";
	my $flag=0;
	while (my $line = <INFILE>){
		chomp $line;
		if ($flag >0) {
			if ($line =~ m/;/){
				$flag = 0;
			}
			else{
				if (!($line =~ m/,$/)){
					$line .= ",";
				}
			}
		}
		if ($line =~ m/Translate/){
			$flag = 1;
		}

		print OUTFILE "$line\n";
	}

	close(INFILE) or die;;
	close(OUTFILE) or die;
	
	my $treeio = new Bio::TreeIO(-file   => "$name_fixed_file",  -format => "nexus");
	
	`rm $name_fixed_file`;
	
	return $treeio;
}


sub find_lca
{
	# Takes a TreeI and a reference to nn array of nodes, and finds the most 
	# recent common ancestor of those nodes
	
	my $tree = shift;
	my $aref = shift;
	my @nodes = (@$aref);
	
	
	my @orig = @nodes;
	while( @nodes > 1 ) {
  		my $lca = $tree->get_lca(-nodes => [shift @nodes, shift @nodes]);
  		push @nodes, $lca;
	}
	my $lca = shift @nodes;
	#print "lca is ",$lca->id, " for ", join(",",map { $_->id } @orig), "\n";
	return $lca;
}


################################################################################
######Genereal system tasks#####################################################





sub get_current_datetime{
	my @lt = localtime;
	my $year = $lt[5] + 1900;
	my $month = expand_num($lt[4] + 1);
	my $day = expand_num($lt[3]);
	my $hour = expand_num($lt[2]);
	my $min = expand_num($lt[1]);
	my $sec = expand_num($lt[0]);

	my $datetime = "$year-$month-$day $hour:$min:$sec";
	
	return $datetime;

}

sub expand_num
{
	my $num = shift;
	my $width = shift;
	
	if ( $width < 1 ){
		$width = 2;
	}
	
	$width -= length($num);
	
	my $zeroes = "";
	
	for (my $i =0; $i <= $width; $i++) {
     	$zeroes .= "0";
    } #end for
	
	

	$num = "$zeroes$num";
	
	return $num;
}


sub get_seq_obj 
{
	#Returns a seq object
	#If the first three characters of the name are letters, it gets the sequence from protol
	#If only the first character is a letter, it looks elsewhere
	
	#If headertype is 0 it uses the sequence id along with a letter identifying the database as the sequence name, modified this on 24Apr08 to be Genus_species@uniquesequenceid
	#If headertype is 1 it uses the taxon name as the sequence name
	
	my $name = shift;
	my $headertype = shift;
	my $nucleotide = shift; # if greater than 0, returns nucleotide sequence if possible
	$nucleotide = 0;
	
	my $taxon = "";
	my $seq = "";
	my $idstring = "";
	
	
	if ($name =~ m/^[a-zA-Z][a-zA-Z][a-zA-Z]\d/){
		# Get the sequence from protol
		my $sql = "select cluster_id, taxon, protein from clusters where name=\'$name\'";
		
		if ($nucleotide > 0){
			$sql = "select cluster_id, taxon, nucleotide from clusters where name=\'$name\'";
		}
		my $statement = $dbh_p->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
		$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
		my $n = 0;
		while (my @row = $statement->fetchrow_array()) {
			my $id = $row[0];
			$idstring = "p$id";
			$taxon = $row[1];
			$seq = $row[2];
			$n++;
		}
	
		if ($n>1){
			die "$n entries for $name in clusters !!!\n";
		}
		elsif ($n==0){
			die "No entries for $name in clusters !!!\n";
		}
		
	}
	elsif ($name =~ m/^GI(\d+)/){
		# Get the sequence from homologene using GI
		my $gi = $1;

		my $sql = "select homologene_key_id, taxon, sequence from homologene where protein_gi=\'$gi\'";
		my $statement = $dbh_h->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
		$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
		my $n = 0;
		while (my @row = $statement->fetchrow_array()) {
			my $id = $row[0];
			$taxon = $row[1];
			$seq = $row[2];
			$idstring = "h$id";
			$n++;
		}
	
		if ($n>1){
			die "$n entries for $name in homologene !!!\n";
		}
		elsif ($n==0){
			die "No entries for $name in homologene !!!\n";
		}
		
		if ($nucleotide > 0){
			$seq = "";
		}
	}
	elsif ($name =~ m/^h(\d+)/){
		# Get the sequence from homologene
		my $id = $1;
		

		my $sql = "select taxon, sequence from homologene where homologene_key_id=\'$id\'";
		my $statement = $dbh_h->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
		$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
		my $n = 0;
		while (my @row = $statement->fetchrow_array()) {
			$taxon = $row[0];
			$seq = $row[1];
			$idstring = $name;
			$n++;
		}
	
		if ($n>1){
			die "$n entries for $name in homologene !!!\n";
		}
		elsif ($n==0){
			die "No entries for $name in homologene !!!\n";
		}
		
		if ($nucleotide > 0){
			$seq = "";
		}
		
	}
	elsif ($name =~ m/^p(\d+)/){
		# Get the sequence from protol
		my $id = $1;

		my $sql = "select cluster_id, taxon, protein from clusters where cluster_id=\'$id\'";
		
		if ($nucleotide > 0){
			$sql = "select cluster_id, taxon, nucleotide from clusters where cluster_id=\'$id\'";
		}
		
		my $statement = $dbh_p->prepare($sql) or die "Couldn't prepare query '$sql': $DBI::errstr\n";
		$statement->execute() or die "Couldn't execute query '$sql': $DBI::errstr\n";
		my $n = 0;
		while (my @row = $statement->fetchrow_array()) {
			my $id = $row[0];
			$idstring = "p$id";
			$taxon = $row[1];
			$seq = $row[2];
			$n++;
		}
	
		if ($n>1){
			die "$n entries for $name in clusters !!!\n";
		}
		elsif ($n==0){
			die "No entries for $name in clusters !!!\n";
		}
		
	}
	else{
		die "Cannot identify sequence $name!\n";
	}
	
	$taxon =~ s/ /_/;
	$taxon = fix_name($taxon);
	
	my $header = "";
	if ($headertype < 1){
		$header = $taxon . "@" . $idstring;
	}
	else{
		$header = $taxon;
	}
	
	my $alphabet = "protein";
	if ($nucleotide > 0){
		$alphabet = "dna";
	}
	
	my $seqobj = Bio::Seq->new( -display_id => $header, -seq => $seq, -alphabet => $alphabet);
	return $seqobj;

}



sub open_dbi
{
   # Declare and initialize variables
   my $host = 'localhost';
   my $db = shift;
   #print "Connecting to $db\n";

   my $db_user = 'perlstuff';
   my $db_password = 'chun!';

   # Connect to the requested server
   #print "Connecting to dbi:mysql:$db:$host, $db_user, $db_password";
   my $dbh = DBI->connect("dbi:mysql:$db:$host", "$db_user", "$db_password", {RaiseError => 0, PrintError => 0} ) or die("Cannot connect to the database");
   return $dbh;
}#end: open_dbi

sub stripstring {
	#gets rid of characters that can cause problems in mysql queries
	my $string = shift;
	$string =~ s/\'/prime/g;
	return $string;

}

sub check_complexity
{
	# Returns a 0 if a sequence is so simple that it is likely to crash blast
	my $seq = shift;
	my $length_cutoff = 30;
	
	my $length = length($seq);
	
	if ($length < $length_cutoff){
		return 0;
	}
	
	
	return 1;

}



################################################################################
######Arrays####################################################################


sub randomize {
#Take an array and randomize the order of its elements
    my @start;
    @start = @_;
   
    my @out;
    
    while ( @start ) {
        my $n = @start;
        my $r = &randint( $n ) - 1;
        my $element = splice (@start, $r, 1);
        push (@out, $element);
    }


    return @out;
}

sub randint {
#Calculate a random integer from 1 to the specified number
   my $upper= shift @_;
   my $size = 1/$upper;

   my $n;
   my $f = rand;
   while ($f > 0) {
      $n++;
      $f = $f - $size;
   }

   return $n;

}

################################################################################
######Debugging#################################################################


sub show_seq_info {
	my $seqobj = shift;
	my $display_id = $seqobj->display_id;
	my $seq = $seqobj -> seq;
	my $accession_number = $seqobj->accession_number(); # when there, the accession number
	my $alphabet = $seqobj->alphabet();         # one of 'dna','rna',or 'protein'
	#my $seq_version = $seqobj->seq_version();       # when there, the version
	#my $keywords = $seqobj->keywords();         # when there, the Keywords line
	my $length = $seqobj->length();            # length
	my $desc = $seqobj->desc();             # description
	my $primary_id = $seqobj->primary_id();       # a unique id for this sequence regardless
								 # of its display_id or accession number
		
	
	print "display_id: $display_id\n";
	print "desc: $desc\n";
	print "seq: $seq\n";
	

}
