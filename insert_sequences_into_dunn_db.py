import os,sys,sqlite3
from Bio import SeqIO
import datetime


if __name__ == '__main__':
	if len(sys.argv) != 7:
		print "python insert_sequences_into_dunn_db.py database taxon_with_underscores nucleotide_seqs quality_seqs translated_seqs masked_seqs"
		sys.exit(0)
	
	database = sys.argv[1]
	if os.path.exists(database) == False:
		print "the database has not been created or you are not in the right place"
	con = sqlite3.connect(database)
	cur = con.cursor()
	
	taxon = sys.argv[2]
	tid =""
	cur.execute("SELECT id FROM taxa where name = ?;",(taxon,))
	a = cur.fetchall()
	if len(a) == 0:
		print "inserting "+taxon+" into taxa table"
		tid = cur.execute("INSERT taxa(name,last_updated_on) values(?,?)",(taxon,datetime.datetime.now()))
		print tid
		cur.commit()
	else:
		tid = a[0]
	print "taxon_id: "+str(tid)

	nucl = sys.argv[3]
	nf = open(nucl,"r")
	for i in SeqIO.parse(nf,"fasta"):
		print i.id
	nf.close()
	
	qual = sys.argv[4]
	qf = open(qual,"r")
	
	qf.close()
	
	tran = sys.argv[5]
	tf = open(tran,"r")
	
	tf.close()
	
	mask = sys.argv[6]
	mf = open(mask,"r")
	
	mf.close()