import sys
from Bio import SeqIO
from Bio.Alphabet import generic_dna
from Bio import Seq

"""
This will verify the hexamers from the 454Allcontigs.clipped and cap3.contigs with the solid count files (the ones that
are produced by my counting procedure -- two columns, one for F and one for R)


"""

MAX_MISMATCH = 2

"""
should add the zeros to the isotig and isogroup labels
"""
def add_zeros(num):
	length = len(str(num))
	addz = 5-length
	return (addz*'0')+str(num)


if __name__ == "__main__":
	if len(sys.argv) != 6:
		print "usage: python verifying_hexamer_normalization.py 454AllContigs.clipped.fna cap3.contigs s_1_sorted.txt reads_distrib_ss outfile"
		sys.exit(0)
		
	#open the 454AllContigs.clipped.fna
	infile = open(sys.argv[1],"rU")
	contigs_lens = {}
	allseqs = {}
	for i in SeqIO.parse(infile,"fasta"):
		contigs_lens[i.id] = len(i.seq)
		allseqs[i.id] = i
	infile.close()

	#open the cap3.contigs
	infile = open(sys.argv[2],"rU")
	cap3lens = {}
	for i in SeqIO.parse(infile,"fasta"):
		cap3lens[i.id] = len(i.seq)
		allseqs[i.id] = i
	infile.close()

	#open the combined counts file
	infile = open(sys.argv[4],"rU")
	solid_for = {}
	solid_rev = {}
	first = True
	curcontig = ""
	cur_f = 0
	cur_r = 0
	for i in infile:
		if first == True:
			first = False
			continue
		spls = i.strip().split()
		contig = spls[0]
		if contig != curcontig:
			if curcontig == "":
				curcontig = contig
			else:
				solid_for[curcontig] = cur_f
				solid_rev[curcontig] = cur_r
				cur_f = 0
				cur_r = 0
				curcontig = contig
		sf = int(spls[5])
		sr = int(spls[6])
		cur_f += sf
		cur_r += sr
	solid_for[curcontig] = cur_f
	solid_rev[curcontig] = cur_r
	infile.close()

	ref = {} #contig is key and value is 1 for forward and -1 for rev
	#get the direction based on the solid counts
	solid_mult = {}
	counted = 0
	notcounted = 0
	for i in solid_for:
		dir = 1
		#just getting rid of the modulo error
		solid_for[i] += 0.000000000000000001
		solid_rev[i] += 0.000000000000000001
		if float(solid_for[i]/solid_rev[i]) >= 2:
			ref[i] = 1
			counted += 1
			solid_mult[i] = solid_for[i]
			#print solid_mult[i]
		elif float(solid_for[i]/solid_rev[i])  < 0.5:
			ref[i] = -1
			counted += 1
			solid_mult[i] = solid_rev[i]
			#print solid_mult[i]
		else:
			notcounted += 1		
	print counted,notcounted
	#generate the hex list
	hexlist = {} #contig is key and value is hash with key hex and value is count
	hexlist_mult = {} #multiplied by the solid_mult
	for i in ref:
		hexlist[i] = {}
		se = ""
		if ref[i] == -1:
			se = allseqs[i].seq.tostring()
		else:
			se = allseqs[i].seq.reverse_complement().tostring()
		#print i
		for j in range(len(se)-29):
			he = se[j:j+6].upper()
			if he not in hexlist[i]:
				hexlist[i][he] = 0
			if he not in hexlist_mult:
				hexlist_mult[he] = 0
			hexlist[i][he] += 1
			hexlist_mult[he] += (1 * solid_mult[i])
		#print hexlist
		#sys.exit(0)
	#open the illumina counts file
	ill_hex = {} #key is contig and value is dict with hex as key and count as value
	
	infile = open(sys.argv[3],"rU")
	count = 0
	first = True
	for i in infile:
		if first == True:
			first = False
		else:
			spls =  i.strip().split("\t")
			#actually sequence name , cap3 is cap3_ prefix for now
			match = spls[11]
			if len(match) == 0: #no match
				continue
			#start position
			pos = int(spls[12])
			length = 35 #seems to be fixed
			#negative values seem to be if the mismatch is overhanging before the seq starts
			if pos < 0:
				length += pos
				pos = 1
			#forward or reverse
			strand = spls[13]#will be F or R
			startv = pos-1 #indexing starts at 0
			mismatch = 0 #NOT calculated on this end
			
			#for cap3
			if match[:5] == "cap3_":
				match = match.split("cap3_")[1]
				match = match.replace("contig","Contig")
			if match not in ref:
				continue
			#print contig,startv,endv,lenv,mismatch,strand
			if mismatch <= MAX_MISMATCH:
				if strand == "F":#then to count ref[contig] needs to == -1
					if ref[match] == -1:
						if match not in ill_hex:
							ill_hex[match] = {}
						se = Seq.Seq(spls[8],generic_dna)
						stri = se.tostring().upper()[0:6]
						if stri not in ill_hex[match]:
							ill_hex[match][stri] = 0
						ill_hex[match][stri] += 1
				else: #then to count ref[contig] needs to == 1
					if ref[match] == 1:
						if match not in ill_hex:
							ill_hex[match] = {}
						se = Seq.Seq(spls[8],generic_dna)
						stri = se.tostring().upper()[0:6]
						if stri not in ill_hex[match]:
							ill_hex[match][stri] = 0
						ill_hex[match][stri] += 1
				count += 1
				if count % 1000000 == 0:
					print count
					if count == 100:
						break
	infile.close()
	
	outfile = open(sys.argv[5]+".hg","w")
	outfile.write("hex\tglobal\n")
	for i in hexlist_mult:
		outfile.write(i+"\t"+str(hexlist_mult[i])+"\n")
	outfile.close()

	outfile = open(sys.argv[5]+".choe","w")
	outfile.write("contig\thex\tobs\texp\n")
	for j in ill_hex: #for each contig
		both_hexs = set(ill_hex[j])
		both_hexs = both_hexs.union(set(hexlist[j]))
		scale1 = 0
		for i in ill_hex[j]:
			scale1 += ill_hex[j][i]
		scale2 = 0
		for i in hexlist[j]:
			scale2 += hexlist[j][i]
		scale = float(scale1)/float(scale2)
		for i in both_hexs: # for each hex	
			if i not in ill_hex[j]:
				ill_hex[j][i] = 0
			if i not in hexlist[j]:
				hexlist[j][i] = 0
			outfile.write(str(j)+"\t"+str(i)+"\t"+str(ill_hex[j][i])+"\t"+str(hexlist[j][i]*scale)+"\n")
	outfile.close()

	outfile = open(sys.argv[5]+".cs","w")
	outfile.write("contig\tsolid\n")
	for i in solid_mult:
		outfile.write(i+"\t"+str(solid_mult[i]-0.000000000000000001)+"\n")
	outfile.close()
