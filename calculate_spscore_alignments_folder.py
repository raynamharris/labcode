import os,sys
import subprocess as sp

def spscore_cmd(filen):
	cmd = "muscle -spscore "+str(filen)
	return cmd

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python calculate_spscore_alignments_folder.py folder line_ending"
		sys.exit(0)
	
	folder = sys.argv[1]
	line_ending = sys.argv[2]
	
	for i in os.listdir(folder):
		if i[-len(line_ending):] == line_ending:
			#print folder+"/"+i
			#os.system(spscore_cmd(folder+"/"+i))
			p1 = sp.Popen(["grep","-c","\>",str(folder+'/'+i)],stdout = sp.PIPE,stderr=sp.STDOUT)
			numseqs = p1.stdout.read().strip()
			p1 = sp.Popen(["muscle","-spscore",str(folder+'/'+i)], stdout = sp.PIPE,stderr=sp.STDOUT)
			tstr = p1.stdout.read()
			print numseqs+"\t"+tstr.split("\n")[-2].split(";")[1].split("=")[1]