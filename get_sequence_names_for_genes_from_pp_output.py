import sys,os
import newick3,phylo3

"""
this is taking the gene file from labcode/get_gb_min_sites_taxa.py 
and the output from the pp files in a folder with the names like


gene file names are like
RAxML_bestTree.OG5_210023.mm.pp.0.aln-gb

and will correspond to pp files like
RAxML_bestTree.OG5_128563.mm.pp
the line from mm.pp.#.aln-gb will be the line number in the pp file

these pp files will often be ../../../../

"""

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print "python get_sequence_names_for_genes_from_pp_output.py genefile ppdir outfile"
        sys.exit(0)
    gfile = open(sys.argv[1],"r")
    genes = gfile.readline().strip().split(" ")
    gfile.close()
    
    ppdir = sys.argv[2]
    outfile = open(sys.argv[3],"w")
    for i in genes:
        gname = i.split(".")[0]+"."+i.split(".")[1]+".mm.pp"
        num = int(i.split(".")[-2])
        print i,gname,num
        ppfile = open(ppdir+"/"+gname,"r")
        tr = newick3.parse(ppfile.readlines()[num])
        for i in tr.leaves():
            outfile.write(i.label+" ")
        outfile.write("\n")
    outfile.close()
