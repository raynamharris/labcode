import sys,os
from Bio import SeqIO

univecloc = "/media/data/lib/univec_only_ill.fasta"

if __name__=="__main__":
    if len(sys.argv) != 3:
        print "python remove_adaptors_illumina_reads.py infile.fastq infile2.fastq"
        sys.exit(0)
    print "blasting the first file"
    SeqIO.convert(sys.argv[1],"fastq-illumina",sys.argv[1]+".fasta","fasta")
    cmd = "makeblastdb -in "+sys.argv[1]+".fasta -dbtype nucl -out temp.db"
    os.system(cmd)
    cmd = "blastn -query "+univecloc+" -db temp.db -outfmt 6 -num_threads 8 -num_descriptions 10000000 | awk '{print $2}' | sort | uniq > temp.out"
    os.system(cmd)
    infile = open("temp.out","r")
    seq_names = []
    for i in infile:
        seq_names.append(i.strip()[:-1])
    infile.close()
    os.system("rm "+sys.argv[1]+".fasta")
    os.system("rm temp.db*")
    
    print "blasting the second file"
    SeqIO.convert(sys.argv[2],"fastq-illumina",sys.argv[2]+".fasta","fasta")
    cmd = "makeblastdb -in "+sys.argv[2]+".fasta -dbtype nucl -out temp.db"
    os.system(cmd)
    cmd = "blastn -query "+univecloc+" -db temp.db -outfmt 6 -num_threads 8 -num_descriptions 10000000 | awk '{print $2}' | sort | uniq > temp.out"
    os.system(cmd)
    infile = open("temp.out","r")
    for i in infile:
        seq_names.append(i.strip()[:-1])
    infile.close()
    os.system("rm "+sys.argv[2]+".fasta")
    os.system("rm temp.db*")
    

    print "processing the first and second files at the same time"
    infile = open(sys.argv[1],"r")
    infile2 = open(sys.argv[2],"r")
    count = 0
    outfile = open(sys.argv[1]+".cleaned","w")
    outfile2 = open(sys.argv[2]+".cleaned","w")
    outfile_errors = open("illumina_primers.fastq","w")
    secondg = SeqIO.parse(infile2,"fastq-illumina")
    for first in SeqIO.parse(infile,"fastq-illumina"):
        second = secondg.next()
        if first.id[:-1] not in seq_names and second.id[:-1] not in seq_names:
            tseqs1 = [first]
            tseqs2 = [second]
            SeqIO.write(tseqs1,outfile,"fastq-illumina")
            SeqIO.write(tseqs2,outfile2,"fastq-illumina")
        else:
            tseqs1 = [first,second]
            SeqIO.write(tseqs1,outfile_errors,"fastq-illumina")
        count += 1
        if count % 10000==0:
            print count
    infile.close()
    infile2.close()
    outfile.close()
    outfile2.close()
    outfile_errors.close()
