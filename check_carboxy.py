#!/usr/bin/env python	

from Bio import AlignIO
import sys


Usage = """
Usage:

%s alignment.aln end

Prints out a list of each taxon in an alignment, with a 0 if there are no unambiguous characters 
after poition end and a 1 if there are


""" % sys.argv[0]


if __name__ == "__main__":
	if len(sys.argv) != 3:
		print Usage
		sys.exit(0)
	end = int(sys.argv[2])
	alignment = AlignIO.read(open(sys.argv[1]), "fasta")
	print "Alignment length %i" % alignment.get_alignment_length()
	for record in alignment :
		tail = record.seq[end:]
		nongap = 0
		for a in tail:
			if a != '-':
				nongap = 1
		print record.id + "\t" + str(nongap)