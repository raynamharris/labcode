import os,sys

"""
IMPORTANT!! use the c++ program for this as it is much more efficient
"""

"""
this should make the graph based on the results from the blast with
the mcl_input file

the mcl output will be a subset of these clusters

the format is the same as teh mcl output so we can get the stats
using the same scripts
"""

CUTOFF = 40

if __name__ == '__main__':
	if len(sys.argv) != 3:
		print "python create_raw_graphs_from_mcl_input.py input output"
		sys.exit(0)
	
	graphs = []#this will be a list of lists
	input = open(sys.argv[1],"r")
	count = 0
	for i in input:
		spls = i.strip().split("\t")
		fro = spls[0]
		to = spls[1]
		val = float(spls[2])
		if val > CUTOFF:
			if len(graphs) == 0:
				graphs.append([fro,to])
				continue
			inthere = False
			for j in graphs:
				if fro in j:
					if to not in j:
						j.append(to)
					inthere=True
					break
				if to in j:
					if fro not in j:
						j.append(fro)
					inthere=True
					break
			if inthere==False:
				graphs.append([fro,to])
		if count % 10000 == 0:
			print count,len(graphs)
		count += 1
	input.close()
	
	output = open(sys.argv[2],"w")
	for i in graphs:
		outstr = ""
		for j in i:
			outstr += str(j)+"\t"
		outstr = outstr[:-1]
		output.write(outstr+"\n")
	output.close()
