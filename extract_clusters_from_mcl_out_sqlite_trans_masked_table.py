import sys,os,sqlite3
import os.path
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from colors import *

"""
this will take the output from the mcl analysis and write out a table with the overlap
from the included taxa for each cluster, this assumes that all labels are the sequence id labels

this makes no assumption of outgroup or ingroup as that can be filtered later
"""

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "usage: python extract_clusters_from_mcl_out_sqlite_trans_masked_table.py database mcl_in outfile"
		sys.exit(0)
	
	database = sys.argv[1]
	mcl_in = sys.argv[2]
	mcl_file = open(mcl_in,"rU")
	clusters = {}
	count = 0
	for i in mcl_file:
		spls = i.strip().split("\t")
		tcl = []
		for j in spls:
		       	tcl.append(j)
		clusters[count]=tcl
		count += 1

	if os.path.exists(database) == False:
		print "the database has not been created or you are not in the right place"
	con = sqlite3.connect(database)
	cur = con.cursor()

	species_names = []
	species_names_id = {} #key id value species
	cur.execute("SELECT id,name FROM species_names;")
	for i in cur:
		species_names.append(str(i[1]))
		species_names_id[str(i[0])] = str(i[1])

	outfile = open(sys.argv[3],"w")
	for i in species_names:
		outfile.write(i)
		if i != species_names[-1]:
			outfile.write(",")
	outfile.write("\n")
	count = 0
	error_file = open("cluster_errors","w")
	for i in clusters:
		print i,len(clusters[i])
		sp_names_list = []
		for j in clusters[i]:
			sp_name = None
			cur.execute("SELECT species_names_id FROM translated_seqs WHERE id = ?",(j,))
			a = cur.fetchall()
			if len(a) > 0:
				sp_name = species_names_id[str(a[0][0])]
				sp_names_list.append(sp_name)
			if sp_name == None:
				print "ERROR:no break"
				error_file.write(j+"\n")
				print j
				#sys.exit(0)
		printstr = ""
		for j in species_names:
			printstr += str(sp_names_list.count(j))
			if (j != species_names[-1]):
				printstr += ","
		outfile.write(printstr+"\n")
		count += 1
	error_file.close()
	outfile.close()
	mcl_file.close()
		
