import networkx as nx
import sys
import sqlite3

db = sqlite3.connect(sys.argv[1])
taxa = dict(db.execute("SELECT sequence_id, catalog_id FROM sequences;"))

print "Found", len(taxa), "sequences records"

hist = {}

graph = nx.Graph()

for line in open(sys.argv[2]):
	id_from, id_to = line.split()[:2]
	graph.add_node(id_from)
	graph.add_node(id_to)
	graph.add_edge(id_from, id_to)

for subgraph in nx.connected_component_subgraphs(graph):
	unique = set()
	for i in subgraph.nodes_iter():
		unique.add(taxa[int(i)])
	n = len(unique)
	hist[n] = hist.get(n, 0) + 1

for n in sorted(hist):
	print n, hist[n]

