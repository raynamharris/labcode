import argparse
import networkx as nx
import sys
import numpy as np
import matplotlib as mpl

from biolite import utils
from biolite import diagnostics

MIN_NODES = 4
MIN_DEG_CENTRALITY = 0.2

def graph(blast_hits, min_overlap, min_bitscore, seq_type):
	'''Returns graph'''

	G=nx.Graph()

	nseqs = 0
	nedges = {
	   'all': 0,
	   'non-self': 0,
	   'passed-overlap': 0,
	   'passed-bitscore': 0}

	for line in blast_hits:
		# fields:
		# qseqid sseqid bitscore qlen length
		id_from, id_to, bitscore, qlen, length = line.rstrip().split()
		# Sometimes blast outputs a bad query id if the query is longer
		# than 10Kb
		if id_from.startswith('Query_'):
			utils.info("discarding bad hit with query id '%s'" % id_from)
			continue
		bitscore = float(bitscore)
		length = float(length) / float(qlen)
		# Correct for nucleotide vs. amino acid length
		if seq_type == 'nucleotide':
			length *= 3.0
		# Filter out self hits, low scoring hits, and short hits
		nedges['all'] +=1
		if id_from != id_to:
			nedges['non-self'] += 1
			if length > min_overlap:
				nedges['passed-overlap'] += 1
				if bitscore > min_bitscore:
					nedges['passed-bitscore'] += 1
					# If an edge already exists between the nodes, update
					# its score to be the max
					if G.has_edge(id_from, id_to):
						e = G.edge[id_from][id_to]
						e['score'] = max(e['score'], bitscore)
					else:
						G.add_node(id_from)	
						G.add_node(id_to)
						G.add_edge(id_from, id_to, score=bitscore)
#	diagnostics.prefix.append('nedges')
#	diagnostics.log_dict(nedges)
#	diagnostics.prefix.pop()
	
	return G

def connected_components(graph):
	'''Takes full graph and returns connected components'''

	for c in nx.connected_component_subgraphs(graph):
		if c.number_of_nodes() >= MIN_NODES:
			yield c

def hub_spoke_graph(connected_component):
	'''Takes connected component, if average degree centrality
	<= 0.2, return connected_component'''

	avg = np.mean(nx.degree_centrality(connected_component).values())
	if avg <= MIN_DEG_CENTRALITY:
		return True
	else:
		return False

def hub_nodes(connected_component):
	'''Takes connected component and returns nodes with
	high degree centrality (0.7 in this case).'''

	dg = nx.degree_centrality(connected_component)
        degrees = sorted(dg.itervalues())
        percentile = 0.7
        cutoff = degrees[int(len(degrees)*percentile)]
	hubs = []
	for k, v in dg.iteritems():
		if v >= cutoff:
			hubs.append(k)
	return hubs

def break_graph(graph):
	'''Takes connected component and breaks it at hub nodes.
	It returns the new graph'''

	h_nodes = hub_nodes(graph)
	if h_nodes:
		# For now, modifying the graph in place
		graph.remove_nodes_from(h_nodes)
		# For now, removing only isolated nodes
		graph.remove_nodes_from(nx.isolates(graph))
		return True
	else:
		return False

def print_subgraph(graph):
	'''Prints edges with scores'''

	if graph.number_of_nodes() >= MIN_NODES:
		for e in graph.edges_iter(data=True):
			print e

def process_graph(full_graph):
	'''Takes full graph and returns connected components with
	average degree centrality larger than 0.2'''

	to_process = [full_graph]
	result = []
	while to_process:
		g = to_process.pop()
		hub_spokes = []
		for c in connected_components(g):
			if hub_spoke_graph(c) and c.number_of_nodes() >= MIN_NODES:
				hub_spokes.append(c)
			else:
				print_subgraph(c)
		for h in hub_spokes:
			if break_graph(h) and h.number_of_nodes() >= MIN_NODES:
				to_process.append(h)
			else:
				print_subgraph(h)
	return result

if __name__=="__main__":
	parser = argparse.ArgumentParser(description='Takes a blast report \
		and creates components to be parsed by MCL')
	parser.add_argument('blast_hits', type=argparse.FileType('r'), help="""BLAST report in
		tabular format""")
	parser.add_argument('min_overlap', default=0.5, type=float, help="""
		Filter out BLAST hit edges with a max HSP length less 
		than this fraction of target length [default = 0.5]""")
	parser.add_argument('min_bitscore', default=200, type=int, help="""
		Filter out BLAST hit edges with a bitscore below min_bitscore
		[default=200]""")
	parser.add_argument('seq_type', default="protein", help="""The data are
		protein or nucleotide sequences [default=protein]""")
	args = parser.parse_args()
	process_graph(graph(args.blast_hits, args.min_overlap, args.min_bitscore, args.seq_type))
