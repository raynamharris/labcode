import sys
from Bio import SeqIO
from numpy import *

"""
used to calculate N50 (50 will be the N in the input) for a set of sequences in the fna file

The size of the smallest contig such that 50% of the length of the genome is contained in contigs of size N50 or greater.
or
from http://jermdemo.blogspot.com/2008/11/calculating-n50-from-velvet-output.html
N50 length is the length of the shortest contig such that the sum of contigs of equal length or longer is at least 50% of the total length of all contigs

For example's sake imagine an assembler has created contigs of the following length (in descending order):
91 77 70 69 62 56 45 29 16 4
The sum of these is 519bp, so the sum of all contigs equal to or greater than N50 must be equal to or greater than 519/2 or 259.5
We can see by brute force that
91+77=168
91+77+70=238
91+77+70+69=307 (that'll do)
so the N50 for this assembly is 69bp

Another way to look at this:
at least half the nucleotides in this assembly belong to contigs of size 69bp or longer.

"""

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python calculate_N_contig_length.py N fna"
		sys.exit(0)
	N = int(sys.argv[1])
	infile = open(sys.argv[2],"r")
	lens = []
	total = 0
	for i in SeqIO.parse(infile,"fasta"):
		length = len(i.seq.tostring())
		total += length
		lens.append(length)
	lens.sort(reverse=True)
	Nnum = total * float(N/100.)
	cur = 0
	for i in lens:
		cur += i
		if cur > Nnum:
			print "N"+str(N)+": "+str(i)
			break
	infile.close()
