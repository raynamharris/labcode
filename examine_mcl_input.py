import os,sys
import sqlite3

"""
for each species all the connections are printed to a file for each 
species

"""



if __name__ == '__main__':
	if len(sys.argv) != 4:
		print "python examine_mpi_input.py database input output_folder"
		sys.exit(0)
	
	database = sys.argv[1]
	if os.path.exists(database) == False:
		print "the database has not been created or you are not in the right place"
	con = sqlite3.connect(database)
	cur = con.cursor()
	
	openfiles = {}
	dir = sys.argv[3]
	try:
		os.mkdir(dir)
	except:
		print "directory already exists"
	
	species_names = []
	species_names_id = {} #key id value species
	cur.execute("SELECT id,name FROM species_names;")
	for i in cur:
		species_names.append(str(i[1]))
		species_names_id[str(i[0])] = str(i[1])
		openfiles[str(i[1])] = open(dir+"/"+str(i[1]),"w")
	
	input = open(sys.argv[2],"r")
	count = 0
	for i in input:
		spls = i.strip().split("\t")
		fro = spls[0]
		to = spls[1]
		val = spls[2]
		cur.execute("SELECT species_names_id FROM translated_seqs where id = ?;",(fro,))
		a = cur.fetchall()
		name1 = species_names_id[str(a[0][0])]
		cur.execute("SELECT species_names_id FROM translated_seqs where id = ?;",(to,))
		a = cur.fetchall()
		name2 = species_names_id[str(a[0][0])]
		openfiles[name1].write(name1+"@"+fro+"\t"+name2+"@"+to+"\t"+val+"\n")
		if count % 10000 == 0:
			print count
		count += 1
	input.close()
	
	for i in openfiles:
		openfiles[i].close()
