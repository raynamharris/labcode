import os,sys
import newick3,phylo3

"""
This assumes that the names will be speciesnames@number and will ignore the number
"""

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python calc_sister_species_in gene_trees.py folder outfile"
		sys.exit()	
	folder = sys.argv[1]
	counts = {} #this will be a list of dictionaries for each tree
	names = []
	count = 0
	for i in os.listdir(folder):
		if "mm" == i[-2:]:
			count += 1
			dict = {}
			x = open(str(folder)+"/"+str(i),"r")
			tree = newick3.parse(x.readline())
			for s in tree.leaves():
				nm = str(s.label.split("@")[0])
				if nm not in names:
					names.append(nm)
				if nm not in counts:
					counts[nm] = []
				sisters = s.get_sisters()
				for z in sisters:
					if z.label != None and z.label != nm:
						znm = z.label.split("@")[0]
						counts[nm].append(str(znm))
	out = open(sys.argv[2],"w")
	outstr = "names\t"
	for j in names:
		outstr += j+"\t"
	outstr = outstr[:-1]
	out.write(outstr+"\n")
	for i in counts:
		outstr = i+"\t"
		for j in names:
			outstr += str(counts[i].count(j))+"\t"
		outstr = outstr[:-1]
		out.write(outstr+"\n")
	out.close()
