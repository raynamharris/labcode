import sys
import re
from Bio import SeqIO

"""
This is derived from the trim investigation script but is
intended to simply trim a fastq input file and output
the trimmed fastq file
"""

#Takes a dictionary of strings and returns a dictionary of compiled regular expressions
def re_compile (dict_in):
	dict_out = {}
	for key in dict_in.keys():
		dict_out[key] = re.compile(dict_in[key], re.IGNORECASE)
	return dict_out

DEBUG = False


if __name__ == "__main__":
	if len(sys.argv)!=3:
		print "python 454_adaptor_trim_file_fastq.py 454Reads.fastq out.fastq"
		sys.exit(0)
	
	diagsuffix = ".diagnostics"
	maskedsuffix = ".m"
	
	#454 primers
		#ROCHE_5'		ACACGACGACT
		#ROCHE_3'		ACACCACGACT rev comp is AGTCGTGGTGT
	
	#3' adapters
		#PD243			ATTCTAGAGGCCACCTTGGCCGACATGTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTVN
		#PD243-30TC		ATTCTAGAGGCCACCTTGGCCGACATGTTTTCTTTTCTTTTTTTTTCTTTTTTTTTTVN
		#PD243Mme-30TC	ATTCTAGAGCGCACCTTGGCCTCCGACTTTTCTTTTCTTTTTTTTTCTTTTTTTTTTVN
		#PD243Mme-24TC	ATTCTAGAGCGCACCTTGGCCTCCGACTTTTCTTTTTTTTCTTTTTTTTTTVN
		#three prime	           CACCTTGGCC
		#Mme			                     TCCRAC
		#Mme cut		                     TCCRAC                  $$$$
	
	#5' adapters
		#PD242      AAGCAGTGGTATCAACGCAGAGTGGCCACGAAGGCCGGG
		#PD242AsiSI AAGCAGTGGTATCAACGCAGAGTGCGAT     CGCGGG
		
	
	
	roches5 = {
		#'5prime1' : r"ACACGACGACT"
		"RL1_5":  "ACACGACGACT",
		"RL2_5":  "ACACGTAGTAT",
		"RL3_5":  "ACACTACTCGT",
		"RL4_5":  "ACGACACGTAT",
		"RL5_5":  "ACGAGTAGACT",
		"RL6_5":  "ACGCGTCTAGT",
		"RL7_5":  "ACGTACACACT",
		"RL8_5":  "ACGTACTGTGT",
		"RL9_5":  "ACGTAGATCGT",
		"RL10_5": "ACTACGTCTCT",
		"RL11_5": "ACTATACGAGT",
		"RL12_5": "ACTCGCGTCGT"
	}
	roches3 = {
		#'3prime' : r"AGTCGTGGTGT",
		#'3prime2' : r"TAGTCGTGGTGT"
		"RL1_3":  "AGTCGTGGTGT",
		"RL2_3":  "ATACTAGGTGT",
		"RL3_3":  "ACGAGTGGTGT",
		"RL4_3":  "ATACGTGGCGT",
		"RL5_3":  "AGTCTACGCGT",
		"RL6_3":  "ACTAGAGGCGT",
		"RL7_3":  "AGTGTGTGCGT",
		"RL8_3":  "ACACAGTGCGT",
		"RL9_3":  "ACGATCTGCGT",
		"RL10_3": "AGAGACGGAGT",
		"RL11_3": "ACTCGTAGAGT",
		"RL12_3": "ACGACGGGAGT"
	}
	
	roches5_comp = re_compile(roches5)
	roches3_comp = re_compile(roches3)
	
	# Adapter sequences are used for masking as well as finding adapters, so they need to extend
	# from the anchor sequence
	adapters = {
	'PD243' : r"\w*ATTCTAGA\w{17,21}[TCN]{4,}|[GAN]{4,}\w{17,21}TCTAGAAT\w*",
	'PD243_SfiI_residual' : r"^TGGCCGAC|GTCGGCCA$",
	'PD243Mme' : r"\w+CCTCCGAC[CTN]+|[GAN]+GTCGGAGG\w+",
	'PD243Mme-30TC_MmeI_site': r"AAAAAAAAAAGAAAAAAAAAGAAAAGAAAAGTCGGAGGCCAAGGTGCGCTCTAGAA",
	'PD242' : r"A{1,2}GCAGTG\w{21,31}GG|CC\w{21,31}CACTGCT{1,2}$", # Works for SfiI or AsiSI version
	'PD242_SfiI_residual' : r"AGGCCGG+|C+CGGCCT$",
	'PD242_AsiSI_residual' : r"CGCGGG|CCCGCG$"
	}
	
	adapters_comp = re_compile(adapters)
	
	endonuc = {
	'AsiSI' : r"GCGATCGC",
	'Asi4SI_cut' : r"GCG$|^CGC",
	'MmeI' : r"tcc[ag]ac|gt[ct]gga",
	'MmeI_cut' : r"tcc[ag]ac\w{16,20}$|^\w{16,20}gt[ct]gga",
	'SfiI' : r"GGCC\w{5}GGCC",
	'SfiI_cut' : r"GGCC\w{1}$|^\w{1}GGCC",
	'polytail' : r"^TTTTTT[CT]*|[GA]*AAAAAA$"
	}
	
	endonuc_comp = re_compile(endonuc)
	
	# Create a dictionary of sequences
	infile = open(sys.argv[1],"r")
	
	count = 0
	
	print "trimming sequences"
	outseqs = []
	#Loop over the sequences, in the order they were read from the file
	for record in SeqIO.parse(infile,"fastq"):
		seqr = record
		raw_l = len(seqr.seq)
		seq = str(seqr.seq)
		#first need to take off the sequencing primers of the 454machine
		
		cur_front_trim = 0
		cur_rear_trim = 0
		
		#print "orig",seq
		
		for key in roches5_comp.keys():
			hits = roches5_comp[key].search(seq)
			if hits:
				#print "hit:", hits.start(),hits.end()
				if hits.start() == 0 and cur_front_trim < hits.end():
					cur_front_trim = hits.end()
		#print "5pri",seq[cur_front_trim:len(seq)-cur_rear_trim]
		
		for key in roches3_comp.keys():
			hits = roches3_comp[key].search(seq)
			if hits:
				#print "hit:", hits.start(),hits.end()
				if hits.end() == (len(seq)-cur_rear_trim) and cur_rear_trim < (len(seq)-hits.start()):
					cur_rear_trim = len(seq)-hits.start()
		#print "3pri",seq[cur_front_trim:len(seq)-cur_rear_trim]
		
		
		for key in endonuc_comp.keys():
			hits = endonuc_comp[key].search(seq)
			if hits:
				#fff_results[key] = len(hits)
				#print "hit:", hits.start(),hits.end(),key
				if hits.end() >= (len(seq)-cur_rear_trim) and cur_rear_trim < (len(seq)-hits.start()):
					cur_rear_trim = len(seq)-hits.start()
				elif hits.start() <= cur_front_trim:
					cur_front_trim = hits.end()
		#print "endo",seq[cur_front_trim:len(seq)-cur_rear_trim]
		
		
		for key in adapters_comp.keys():
			hits = adapters_comp[key].search(seq)
			if hits:
				#print "hit:", hits.start(),hits.end(),key,cur_front_trim
				if hits.end() >= (len(seq)-cur_rear_trim) and cur_rear_trim < (len(seq)-hits.start()):
					cur_rear_trim = len(seq)-hits.start()
				elif hits.start() <= cur_front_trim:
					cur_front_trim = hits.end()
		#print "adap",seq[cur_front_trim:len(seq)-cur_rear_trim]
		#print "fina",(len(seq[0:cur_front_trim])*'-')+seq[cur_front_trim:len(seq)-cur_rear_trim]+(len(seq[len(seq)-cur_rear_trim:])*'-')
		#print "trim",cur_front_trim,len(seq) - cur_rear_trim
		#print "=================================================================================="
		cur_front_trim = cur_front_trim + 1 # Offset by one, since the trim points are what is to 
											# be maintained
		
		if (len(seq)-cur_rear_trim) > cur_front_trim:
			outseqs.append(record[cur_front_trim:(len(seq)-cur_rear_trim)])
		else:
			outseqs.append(record[0:len(seq)])
		count += 1
		#if count == 10:
		#	sys.exit(0)
	infile.close()
	out = open(sys.argv[2],"w")
	SeqIO.write(outseqs,out,"fastq")
	out.close()
