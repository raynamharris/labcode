import sys,os,newick3,phylo3

def mean(li):
	lenli = float(len(li))
	retmn = 0
	for i in li:
		retmn += i/lenli
	return retmn

def leaf_distance(leaf1,leaf2):
	ret = 0.
	path1 = {}
	nd = leaf1
	path1[nd] = nd.length
	while nd.parent != None:
		path1[nd.parent] = path1[nd]+nd.parent.length
		nd = nd.parent
	nd = leaf2
	curlen = 0
	while nd != None:
		if nd in path1:
			curlen += path1[nd] - nd.length
			break
		else:
			curlen += nd.length
		nd = nd.parent
	ret = curlen
	return ret

if __name__ == "__main__":
	if len(sys.argv) < 4:
		print "python get_branch_length_distributions.py mean/all infile.tre name1 ..."
		sys.exit(0)
	if sys.argv[1] not in ["all","mean"]:
		print "second arg must be all or mean"
		sys.exit(0)
	infile = open(sys.argv[2],"r")
	tree = newick3.parse(infile.readline())
	infile.close()
	names = sys.argv[3:]
	bl = []
	nodes = []
	for i in tree.leaves():
		if i.label in names:
			nodes.append(i)
	for i in range(len(nodes)):
		for j in range(len(nodes)):
			if i < j:
				bl.append(leaf_distance(nodes[i],nodes[j]))
	if sys.argv[1] == "all":
		for i in bl:
			print i
	if sys.argv[1] == "mean":
		print sys.argv[2]+"\t"+str(mean(bl))
