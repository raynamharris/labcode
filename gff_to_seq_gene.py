#! /usr/bin/env python
import sys
import re

usage="""
gff_to_seq_gene.py

Usage: 


"""


if len(sys.argv)<2:
	print usage	

else:
	filename = sys.argv[1]
	infile = open(filename, "r")
	
	for line in infile:
		if re.match("#", line):
			continue
		fields = line.strip().split("\t")
		
		"""
		From http://genome.ucsc.edu/FAQ/FAQformat.html#format3
		Here is a brief description of the GFF fields:

		seqname - The name of the sequence. Must be a chromosome or scaffold.
		source - The program that generated this feature.
		feature - The name of this type of feature. Some examples of standard feature types are "CDS", "start_codon", "stop_codon", and "exon".
		start - The starting position of the feature in the sequence. The first base is numbered 1.
		end - The ending position of the feature (inclusive).
		score - A score between 0 and 1000. If the track line useScore attribute is set to 1 for this annotation data set, the score value will determine the level of gray in which this feature is displayed (higher numbers = darker gray). If there is no score value, enter ".".
		strand - Valid entries include '+', '-', or '.' (for don't know/don't care).
		frame - If the feature is a coding exon, frame should be a number between 0-2 that represents the reading frame of the first base. If the feature is not a coding exon, the value should be '.'.
		group - All lines with the same group are linked together into a single item.
		"""
		
		# eg:
		# scaffold_1	JGI	CDS	33145	33495	.	-	2	name "fgenesh1_pg.scaffold_1000005"; proteinId 196074; exonNumber 5
		
		
		
		# Only consider CDS
		if fields[2] != "CDS":
			continue
		
		group_fields = fields[8].split("; ")
		
		# seq_gene.md header from sample file:
		#tax_id	chromosome	chr_start	chr_stop	chr_orient	contig	ctg_start	ctg_stop	ctg_orient	feature_name	feature_id	feature_type	group_label	transcript	evidence_code
		
		# parse position data from gff into both chromosome and contig data for seq_gene.md
		tax_id = "45351" # For Nematostella
		
		out_fields = [tax_id, fields[0], fields[3], fields[4], fields[6], fields[0], fields[3], fields[4], fields[6], group_fields[2], group_fields[1], fields[2], "from_GFF", group_fields[1], "-"]
		out = '\t'.join(out_fields)
		print out