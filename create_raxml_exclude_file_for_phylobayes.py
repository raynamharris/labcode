import os,sys

"""
should be helpful for generating an exclude file for
parts of the model that are not very long.
"""

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python create_raxml....py infile.model outfile"
		sys.exit(0)
	
	infile = open(sys.argv[1],"r")
	outfile = open(sys.argv[2],"w")
	for i in infile:
		spls = i.strip().split("=")
		spls2 = spls[1].split("-")
		one = int(spls2[0])
		two = int(spls2[1])
		if ( two - one ) < 5:
			outfile.write(str(one)+"-"+str(two)+" ")
	outfile.close()
	infile.close()
