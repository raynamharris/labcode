import sys,os
from Bio import SeqIO

"""
makes individual seq and qlt files for the set of seqs in infile.fna
"""

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python make_individual_seq_qlt_files.py infile.fna outdir"
		sys.exit(0)
	infas = open(sys.argv[1],"rU")
	outdir = sys.argv[2]
	if os.path.isdir(outdir) == False:
		os.mkdir(outdir)
	for i in SeqIO.parse(infas,"fasta"):
		outf = open(outdir+"/"+str(i.id)+".seq","w")
		outf2 = open(outdir+"/"+str(i.id)+".qlt","w")
		outf2.write(">"+i.id+"\n")
		for j in range(len(i.seq)):
			outf2.write("40")
			if j != len(i.seq)-1:
				outf2.write(" ")
		outf2.write("\n")
		SeqIO.write([i], outf, "fasta")
		outf.close()
		outf2.close()
	infas.close()
