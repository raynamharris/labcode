import os,sys

"""
right now just looking at specific taxa
"""

if __name__ == '__main__':
	if len(sys.argv) != 3:
		print "python examine_mpi_input.py input output"
		sys.exit(0)
	
	store_from = {}
	
	input = open(sys.argv[1],"r")
	output = open(sys.argv[2],"w")
	count = 0
	for i in input:
		spls = i.strip().split("\t")
		fro = spls[0]
		to = spls[1]
		val = float(spls[2])
		#print fro +"\t"+to+"\t"+str(val)
		if fro not in store_from:
			store_from[fro] = {}
		if to not in store_from:
			store_from[to] = {}
		if to not in store_from[fro] and fro not in store_from[to]:
			store_from[fro][to] = float(val)
		else:
			if to in store_from[fro]:
				val = (val+store_from[fro][to])/ 2.
				store_from[fro].pop(to)
			else:
				val = (val+store_from[to][fro])/ 2.
				store_from[to].pop(fro)
			output.write(fro+"\t"+to+"\t"+str(val)+"\n")
			#break
		if count % 10000 == 0:
			print count
		count += 1
	input.close()
	for i in store_from:
		for j in store_from[i]:
			output.write(i+"\t"+j+"\t"+str(store_from[i][j])+"\n")
	output.close()