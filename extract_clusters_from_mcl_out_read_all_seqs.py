import sys,os
import os.path
from Bio import SeqIO
from colors import *

"""
this will take the output from the mcl analysis and write out the fasta
files for each cluster (excluding the genbank names)

this requires the directory structure to be somewhat predictable

it will look for files in the directory translations

this is similar to the script called extract_clusters_from_mcl_out.py
but this reads in all the sequences first to make things much faster

USE THIS ONE!


this also requires identifying the outgroup taxa as only clusters with
at least 2 ingroup taxa will be included and at least 4 total taxa. 
"""

outgroups = ["Lingula_anatina","Schmidtea_mediterranea", "Chaetopterus_variopedatus","Terebratalia_transversa","Cerebratulus_lacteus", "Carinoma_mutabilis","Capitella","Helobdella_robusta"]

SMALLESTCLUSTER = 4
SMALLESTINGROUP = 2

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "usage: python extract_clusters_from_mcl_out.py mcl_in INDIR OUTDIR"
		sys.exit(0)

	mcl_in = sys.argv[1]
	mcl_file = open(mcl_in,"rU")
	clusters = {}
	count = 0
	for i in mcl_file:
		spls = i.strip().split("\t")
		tcl = []
		for j in spls:
			try:
				int(j)
			except:
				tcl.append(j)
		clusters[count]=tcl
		count += 1

	species_names = []
	species_files = {}
	trans_seqs = {}
	
	INDIR = sys.argv[2]
	OUTDIR = sys.argv[3]
	for i in os.listdir(INDIR):
		species_name = i
		species_names.append(i)
		species_files[i] = []
		print "working with files from",BOLD,species_name,RESET
		TRANDIR = INDIR+"/"+i+"/translations/"
		for j in os.listdir(TRANDIR):
			print BLUE,j,RESET
			species_files[i].append(j)
			seqhandle = open(TRANDIR+j,"rU")
			if i not in trans_seqs:
				trans_seqs[i] = {}
			for m in SeqIO.parse(seqhandle,"fasta"):
				sid = m.id
				if sid[:3] == "jgi":
					sid = sid.split("|")[2]
				if len(sid) == 8:
					try:
						int(sid[3:])
						sid = sid[3:]
					except:
						sid = sid
				trans_seqs[i][sid]=m
			seqhandle.close()
	count = 0
	error_file = open("cluster_errors","w")
	for i in clusters:
		if len(clusters[i]) >= SMALLESTCLUSTER:
			print i,len(clusters[i])
			cluster_out_seqfile = open(OUTDIR+"/"+str(count)+".fasta","w")
			cluster_out_seqs = []
			for j in clusters[i]:
				sp_name = j.split("_")[0]+"_"+j.split("_")[1]
				#in case the species is denoted with a single name
				cid_index = 2
				if sp_name not in species_names:
					sp_name = j.split("_")[0]
					cid_index = 1
				found = False
				seq = None
				cid = ""
				cid_spls = j.split("_")
				if len(cid_spls) == cid_index-1:
					cid = cid_spls[cid_index]
				else:
					for n in cid_spls[cid_index:]:
						cid += n+"_"
					cid = cid[:-1]
				if cid[:3] == "jgi":
					cid = cid.split("|")[2]
				if len(cid) == 8:
					try:
						int(cid[3:])
						cid = cid[3:]
					except:
						cid = cid
				try:
					seq = trans_seqs[sp_name][cid]
					seq.id = sp_name+"_"+cid
					seq.description = ""
					seq.name = ""
					cluster_out_seqs.append(seq)
				except:
					seq = None
				if seq == None:
					print "ERROR:no break"
					error_file.write(j+"\n")
					print j
					#sys.exit(0)
			SeqIO.write(cluster_out_seqs,cluster_out_seqfile,"fasta")
			cluster_out_seqfile.close()
			count += 1
	error_file.close()
	mcl_file.close()
		
