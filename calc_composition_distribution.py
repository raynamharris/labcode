from Bio import SeqIO
import sys

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python calc_gc.py inseq.fna outfile.r"
		sys.exit(0)
	ACOMP = []
	CCOMP = []
	GCOMP = []
	TCOMP = []
	
	infile = open(sys.argv[1],"r")
	for i in SeqIO.parse(infile,"fasta"):
		tsq = str(i.seq).lower()
		As = tsq.count("a")
		Cs = tsq.count("c")
		Gs = tsq.count("g")
		Ts = tsq.count("t")
		sm = float(As+Cs+Gs+Ts)
		ACOMP.append(str(As/sm))
		CCOMP.append(str(Cs/sm))
		GCOMP.append(str(Gs/sm))
		TCOMP.append(str(Ts/sm))
	
	print len(ACOMP),len(CCOMP),len(GCOMP),len(TCOMP)
	
	outfile = open(sys.argv[2],"w")
	outfile.write("As=c(")
	count = 1
	for i in ACOMP:
		outfile.write(i)
		count += 1
		if (count-1)!=len(ACOMP):
			outfile.write(",")
	outfile.write(")\n")
	outfile.write("Cs=c(")
	count = 1
	for i in CCOMP:
		outfile.write(i)
		count += 1
		if (count-1)!=len(ACOMP):
			outfile.write(",")
	outfile.write(")\n")
	outfile.write("Gs=c(")
	count = 1
	for i in GCOMP:
		outfile.write(i)
		count += 1
		if (count-1)!=len(ACOMP):
			outfile.write(",")
	outfile.write(")\n")
	outfile.write("Ts=c(")
	count = 1
	for i in TCOMP:
		outfile.write(i)
		count += 1
		if (count-1)!=len(ACOMP):
			outfile.write(",")
	outfile.write(")\n")
	
	
	outfile.close()
		
