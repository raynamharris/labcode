import sys,os,sqlite3
from Bio import SeqIO

transl = {"Solemya_velum":"Solemya","Octopus_vulgaris":"Octopus","Gadila_tolmiei":"Scaphopods","Nucula_expansa":"Nucula","Nautilus_pompilius":"Nautilus","Neomenia_megatrapezata":"Antarctic_neomeniomorph"}

"""
basically this walks through the directory that has the 
seqs and matches the id of the nucleotide seq with the
id of the prot seq in the sqlite database.
this then outputs it in the right order so that it can just be loaded 
into the database as is
"""

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print "python convert_sqlite_to_mysql_seqs_nucleotides.py folder infile.db outfile"
        sys.exit(0)
    DIR = sys.argv[1]
    dbname = sys.argv[2]
    species = []
    if os.path.exists(dbname) == False:
        print "the database doesn't exist"
        sys.exit(0)
    con = sqlite3.connect(dbname)
    cur = con.cursor()
    outfile = open(sys.argv[3],"w")
    for i in os.listdir(DIR):
        tnm = i
        if i in transl:
            tnm = transl[i]
        cur.execute("SELECT * FROM species_names where name = '"+tnm+"';")
        b = cur.fetchall()
        if len(b) == 1:
            print b[0][1]
            seqs = {}
            for j in os.listdir(DIR+"/"+i):
                if "all.forncbi.f" in j or "transcripts.fa" in j:
                    tfile = open(DIR+"/"+i+"/"+j,"r")
                    for k in SeqIO.parse(tfile, "fasta"):
                        seqs[k.id.replace("/","__")] = k.seq
            cur.execute("SELECT * FROM translated_seqs where species_names_id = "+str(b[0][0])+";")
            c = cur.fetchall()
            for j in c:
                if j[2] in seqs:
                    outfile.write("UPDATE sequences set nucleotide_seq = '"+str(seqs[j[2]])+"' where id = "+str(j[0])+";\n")
                else:
                    print "ERROR",j[2]
                    exit()
            print len(c)
    outfile.close()
    con.close()
