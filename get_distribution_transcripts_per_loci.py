import sys
from Bio import SeqIO


if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "python get_distribution_transcripts_per_loci.py infile.fa"
		sys.exit(0)
	infile  = open(sys.argv[1],"r")
	locus = {} #key is locus label, value is number of transcripts
	count = 0
	for i in SeqIO.parse(infile,"fasta"):
		spls = i.id.split("_Transcript_")
		if spls[0] not in locus:
			locus[spls[0]] = 0
		locus[spls[0]] += 1
		count += 1
#		print spls[0],locus
	for i in locus:
		print locus[i]
	