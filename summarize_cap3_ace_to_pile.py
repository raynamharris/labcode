import sys
from Bio import SeqIO
from Bio.Sequencing import Ace

def cut_ends(read, start, end):
	"""Replace residues on either end of a sequence with gaps.
	In this case we want to cut out the sections of each read which the assembler has 
  decided are not good enough to include in the contig and replace them with gaps 
  so the other information about positions in the read is maintained"""
  
	return (start-1) * '-' + read[start-1:end] + (len(read)-end) * '-'
 
def pad_read(read, start, conlength):
	""" Pad out either end of a read so it fits into an alignment.
	The start argument is the position of the first base of the reads sequence in
	the contig it is part of. If the start value is negative (or 0 since ACE files 
	count from 1, not 0) we need to take some sequence off the start 
	otherwise each end is padded to the length of the consensus with gaps."""
	if start < 1:
		seq = read[-1*start+1:]
	else:
		seq = (start-1) * '-' + read
	seq = seq + (conlength-len(seq)) * '-'
	return seq


if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python summarize_isotig_ace_to_pile.py cap3.ace outfile"
		sys.exit(0)	
	
	outw = open(sys.argv[2],"w")
	for contig in Ace.parse(open(sys.argv[1])):
		nm = contig.name
		tseqs = []
		gaps = []
		contiglength = len(contig.sequence)
		for i in range(len(contig.sequence)):
			if contig.sequence[i] == "*":
				gaps.append(i)
		gaps = sorted(gaps,reverse=True)
		contiglength -= len(gaps)
		# print isotig.sequence
		for readn in range(len(contig.reads)):
			#print contig.reads[readn].qa.align_clipping_start
			clipst = contig.reads[readn].qa.qual_clipping_start
			clipe = contig.reads[readn].qa.qual_clipping_end
			start = contig.af[readn].padded_start
			seq = cut_ends(contig.reads[readn].rd.sequence, clipst, clipe)
			seq = pad_read(seq, start, len(contig.sequence))
			#take out to leave in gaps
			for j in gaps:
				seq = seq[:j]+seq[j+1:]
			tseqs.append(seq)
		clist = [0] * (int(contiglength))
		for s in range(int(contiglength)):
			cnt = 0
			for k in tseqs:
				try:
					if k[s].upper() == 'A' or k[s].upper() == 'C' or k[s].upper() == 'G' or k[s].upper() == 'T':
						cnt += 1
				except:
					#print "length issue: trying "+str(s)+" on "+str(len(k))
					continue
			clist[s] = cnt
		stri = nm
		for s in range(int(contiglength)):
			stri += " "+str(clist[s])
			#print inm,c
		outw.write(stri+"\n")
	outw.close()
