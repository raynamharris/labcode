import sys
from svg_lib import *

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "usage: python convert_combined_to_svg_contig.py contig infile outfile"
		sys.exit(0)
	contig = sys.argv[1]
	
	infile = open(sys.argv[2],"r")
	data = []
	lc = len(contig)
	MAX_Y = 0
	for i in infile:
		if i[:lc] == contig:
			spls = i.strip().split("\t")
			data.append(spls[1:])
			for j in spls[1:]:
				if int(j) > MAX_Y:
					MAX_Y = int(j)
			
	infile.close()
	
	SCALE = 1
	XOFFSET = 5
	YOFFSET = 5
	
	MAX_Y *= 2
	
	ld = len(data)
	
	scene = Scene(sys.argv[3],(YOFFSET+MAX_Y)*4,(2*XOFFSET)+(len(data)*SCALE))
	#setup the space
	#he red
	scene.add(CLine((0+XOFFSET,(YOFFSET+MAX_Y)*1),(ld*SCALE,(YOFFSET+MAX_Y)*1), "red"))
	#il green
	scene.add(CLine((0+XOFFSET,(YOFFSET+MAX_Y)*2),(ld*SCALE,(YOFFSET+MAX_Y)*2), "green"))
	#so blue
	scene.add(CLine((0+XOFFSET,(YOFFSET+MAX_Y)*3),(ld*SCALE,(YOFFSET+MAX_Y)*3), "blue"))
	count = 0
	for i in data:
		#so
		scene.add(CLine(((count + XOFFSET),((YOFFSET+MAX_Y)*3)), ((count + XOFFSET),((YOFFSET+MAX_Y)*3)-int(i[5])), "lightblue"))
		scene.add(CLine(((count + XOFFSET),((YOFFSET+MAX_Y)*3)), ((count + XOFFSET),int(i[4])+((YOFFSET+MAX_Y)*3)), "darkblue"))
		##il
		scene.add(CLine(((count + XOFFSET),((YOFFSET+MAX_Y)*2)), ((count + XOFFSET),((YOFFSET+MAX_Y)*2)-int(i[3])), "lightgreen"))
		scene.add(CLine(((count + XOFFSET),((YOFFSET+MAX_Y)*2)), ((count + XOFFSET),int(i[2])+((YOFFSET+MAX_Y)*2)), "darkgreen"))
		##he
		scene.add(CLine(((count + XOFFSET),((YOFFSET+MAX_Y)*1)), ((count + XOFFSET),((YOFFSET+MAX_Y)*1)-int(i[1])), "lightred"))
		scene.add(CLine(((count + XOFFSET),((YOFFSET+MAX_Y)*1)), ((count + XOFFSET),int(i[0])+((YOFFSET+MAX_Y)*1)), "darkred"))
		count += 1
		
	scene.write_svg()
	#scene.display()
	
