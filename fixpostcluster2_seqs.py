import sys
from Bio import SeqIO


"""
this should change any sequence name in the fasta file that starts with
>name_1
to
>name

use this after partigene, but before prot4EST
"""

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print "usage: python fixpostcluster2_seqs.py infile outfile"
        sys.exit(0)
    infile = open(sys.argv[1],"rU")
    seqs = list(SeqIO.parse(infile,"fasta"))
    infile.close()
    for i in seqs:
        spls = i.id.split("_")
        i.id = spls[0]
        i.description = ""
        i.name = ""
    outfile = open(sys.argv[2],"w")
    SeqIO.write(seqs,outfile,"fasta")
    outfile.close()
