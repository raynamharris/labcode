import sqlite3,sys
from Bio import SeqIO
from Bio.Blast import NCBIXML


DB = "/home/smitty/Desktop/pln.178.db"

#Trematoda Taxonomy ID: 6178
#Microsporidia Taxonomy ID: 6029
PARASITES = [6178,6029] #taxon_ids for the parasite clades

MAXBLAST = 5 #number of blast hits to look at

"""
by using an sqlite database, get the left right for the parasites and see if the blast hits
are within those left rights
"""

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "usage: python remove_parasites.py fasta blast.xml outfasta"
		sys.exit(0)
	#open infile
	infile = open(sys.argv[1],"rU")
	allseqs_dict = {}
	for i in SeqIO.parse(infile,"fasta"):
		allseqs_dict[i.id] = i
	
	conn = sqlite3.connect(DB)
	
	#each position corresponds to a taxon
	lefts = []
	rights = []
	
	for i in PARASITES:
		c = conn.cursor()
		c.execute("select left_value,right_value from taxonomy where ncbi_id=?", (i,))
		c.close()
		fl = None
		fr = None
		for j in c:
			fl = int(j[0])
			fr = int(j[1])
		lefts.append(fl)
		rights.append(fr)
	
	inblast = open(sys.argv[2],"rU")
	for j in NCBIXML.parse(inblast):
		qutitle = j.query.split(" ")[0]
		bad = False
		if len(j.alignments) > 0:
			count = 0
			for k in j.alignments:
				if count >= MAXBLAST:
					break
				try:
					count += 1
					name = k.hit_def.split("[")[1].split("]")[0] 
					c = conn.cursor()
					c.execute("select left_value,right_value,ncbi_id from taxonomy where name=?", (str(name),))
					c.close()
					fl = None
					fr = None
					fncbi_id = None
					for m in c:
						fl = int(m[0])
						fr = int(m[1])
						fncbi_id = int(m[2])
					for m in range(len(lefts)):
						if fl > lefts[m] and fr < rights[m]:
							bad = True
					if bad == True:
						break
				except:
					#print k.hit_def
					count += 1
					continue
			if bad == True:
				print qutitle,name,fncbi_id,count
				
				
	inblast.close()
